﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.ViewModels
{
    public class BikeHistoryReportVM
    {
        public BikeHistoryReportVM() { }

        public int JOBcode;
        public int specificJobID;
        //public int RepJobCode;
        //public int MtcJobCode;
        public string StudentName;
        public /*DateTime*/string DateCompleted;
    }
}
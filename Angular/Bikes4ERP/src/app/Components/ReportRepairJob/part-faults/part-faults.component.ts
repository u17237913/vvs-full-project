import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { BicycleBluePrintService } from 'src/app/Services/Blueprint/bicycle-blue-print.service';
import { Section } from 'src/app/Classes/Section/section';
import { Part } from 'src/app/Classes/Part/part';
import { FaultTask } from 'src/app/Classes/FaultTask/fault-task';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';

import { id } from 'date-fns/locale';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';
import { Qrscan } from 'src/app/Components/qr-scan/qrscan.service';

@Component({
  selector: 'app-part-faults',
  templateUrl: './part-faults.component.html',
  styleUrls: ['./part-faults.component.css'],
})
export class PartFaultsComponent implements OnInit {
  TableData1: FaultType[];
  ID: any;
  isPictureAvailable: any;
  Base64Picture: string;
  thefile: FileToUpload = new FileToUpload();
  message: string;

  constructor(
    public route: ActivatedRoute,
    public bicyclepartService: BicyclePartServiceService,
    public bicycleBluePrintService: BicycleBluePrintService,
    public uploadService: FileUploadServiceService,
    public router: Router,
    private qrService: Qrscan,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer
  ) {}

  unknownDetails = new FormGroup({
    notes: new FormControl(''),
  });

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.generatepartfaulttable();

    //Play the audio feed
    setTimeout(( ) => { 
      console.log('ItS BeEn 2 SeCoNdS');
      this.playAudio(); }, 2000);
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../../assets/sounds/Fault.mp3";
    audio.load();
    audio.play();
  }

  buildForm(): void {
    this.unknownDetails = this.formBuilder.group({
      notes: ['', [Validators.required]],
    });
  }

  generatepartfaulttable() {
    this.bicycleBluePrintService.getpartfaulttable(this.ID).subscribe((res) => {
      res.forEach(element => {
        this.uploadService.getFaultIMAGE(element.PartFaultID).subscribe((data) => {
          this.thefile = data;
          this.Base64Picture = data.fileAsBase64;
          if (this.Base64Picture) {
            this.isPictureAvailable = true;
          }
          this.message = 'Image has been successfully loaded boiii !';
          element.PartImage = this.Base64Picture;
        });
      });
    
      this.TableData1 = res as FaultType[];
    });
  }

  transformer(img: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      'data:image;base64,' + img
    );
  }

  returnList(id: any) {
    // debugger;
    this.bicycleBluePrintService.getpartfault(this.ID).subscribe((data) => {
      this.ID = data;
      console.log(this.ID);
    });
    
    this.qrService.CurrentJobFault.PartFaultID = id;
    this.qrService.CurrentJobFault.JobFaultStatusID = 3;
    this.qrService.CurrentJobFaultList.push(this.qrService.CurrentJobFault);

    this.router.navigate(['/reportlist', { ID: id }],{ skipLocationChange: true });
    // var myModal = new Bootstrap.Modal(document.getElementById('Success'));
    // myModal.show();
  }

  reportUnknown(unknownDetails){
    this.qrService.CurrentJobFault.PartFaultID = null;
    this.qrService.CurrentJobFault.JobFaultStatusID = 3;
    this.qrService.CurrentJobFault.Notes = unknownDetails.value.notes;
    this.qrService.CurrentJobFaultList.push(this.qrService.CurrentJobFault);

    document.getElementById('disappearpliz').click();

    this.router.navigate(['/reportlist'],{ skipLocationChange: true });
  }

}
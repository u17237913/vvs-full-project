﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedSectionPart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Part", "SectionID", c => c.Int(nullable: false));
            CreateIndex("dbo.Part", "SectionID");
            AddForeignKey("dbo.Part", "SectionID", "dbo.Section", "SectionID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Part", "SectionID", "dbo.Section");
            DropIndex("dbo.Part", new[] { "SectionID" });
            DropColumn("dbo.Part", "SectionID");
        }
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartFaultsComponent } from './part-faults.component';

describe('PartFaultsComponent', () => {
  let component: PartFaultsComponent;
  let fixture: ComponentFixture<PartFaultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartFaultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartFaultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Tag } from 'src/app/Classes/Tag/tag';
import { TagService } from 'src/app/Services/Tag/tag.service';
import { Router } from '@angular/router';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-create-tag',
  templateUrl: './create-tag.component.html',
  styleUrls: ['./create-tag.component.css'],
})
export class CreateTagComponent implements OnInit {
  TagList: Tag[] = [];
  Form: any;
  data = false;
  public values = [];
  ngModel: any;

  modalHeading = '';
  modalBody = '';
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string;
  href: string;

  constructor(public router: Router, public tagService: TagService) {}

  ngOnInit(): void {
    this.tagService.TagForm = {
      TagID: 0,
      TagSerialNumber: '',
    };
  }

  SaveTag() {
    this.tagService.AddTag(this.tagService.TagForm).subscribe(
      () => {
        this.modalHeading = 'Success';
        this.modalBody = 'Tag has been created successfully.';
        this.ShowSuccessModal();
      },
      (error) => {
        if (error.status == 409) {
          this.modalHeading = 'Error';
          this.modalBody =
            'The Tag Serial Number already exists. Please try again.';
          this.ShowModal();
        }
      }
    );

    // this.GoBackToList();
  }

  GoBacktoTag() {
    this.router.navigate(['/tag']);
  }

  GoBacktoCreateTag() {
    this.router.navigate(['/createtag']);
  }
  onFormSubmit(TagForm) {}

  GoBackToList() {
    this.router.navigate(['/tag']);
  }

  ShowSuccessModal() {
    var Modal = new Bootstrap.Modal(document.getElementById('Success'));
    Modal.show();
  }
  ShowModal() {
    // var modal = document.getElementById('UnassignBicycle');
    // modal.style.removeProperty('z-index');
    var myModal = new Bootstrap.Modal(document.getElementById('TagExists'));
    myModal.show();
  }
}

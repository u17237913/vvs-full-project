﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedPeriodInBusinessRuleTableToValueOfTypeString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusinessRule", "Value", c => c.String());
            DropColumn("dbo.BusinessRule", "Period");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusinessRule", "Period", c => c.DateTime());
            DropColumn("dbo.BusinessRule", "Value");
        }
    }
}

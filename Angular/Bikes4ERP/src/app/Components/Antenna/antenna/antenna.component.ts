import { Component, OnInit } from '@angular/core';
import {AntennaService } from 'src/app/Services/Antenna/antenna.service'
import {Antenna} from 'src/app/Classes/Antenna/antenna'
import { Router } from '@angular/router';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-antenna',
  templateUrl: './antenna.component.html',
  styleUrls: ['./antenna.component.css']
})

export class AntennaComponent implements OnInit {

  AntennaList : Antenna []=[];
  public values = [];
  AntennaForm : Antenna;
  LocationList : LocationViewModel [];
  public TempArray: Antenna[] = [];
  public SearchArray: Antenna[] = [];
  isArrayStored: boolean = false;


  constructor( public route : Router, private antennaService : AntennaService) { }

  ngOnInit(): void {

    this.GetAntennas();
  }

  GetAntennas(){
    this.antennaService.GetAntennas().subscribe((data) =>{
      this.AntennaList = data as Antenna [];
      console.log(this.AntennaList)
    })
  }

  CreateAntenna(){
    this.route.navigate(['/createantenna'])
  }

  GetLocations (){
    this.antennaService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    })
  }
  ViewAntenna (ids : number){
    this.route.navigate(['viewantenna', { id : ids }],{ skipLocationChange: true })
   // console.log(ids)
  }
  SearchOptions() {
    var myModal = new Bootstrap.Modal(
      document.getElementById('SearchOptionsModal')
    );
    myModal.show();
  }

  // var valid =
  // (keycode > 47 && keycode < 58)   || // number keys
  // keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
  // (keycode > 64 && keycode < 91)   || // letter keys
  // (keycode > 95 && keycode < 112)  || // numpad keys
  // (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
  // (keycode > 218 && keycode < 223);   // [\]' (in order)

  SearchAntenna(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.AntennaList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.AntennaList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.AntennaList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.AntennaList.forEach((student) => {
        var SearchString: string = event.target.value;
        if (
          student.AntennaAddress.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(student);
        }
      }); // For each
*/

      this.AntennaList = this.SearchArray;
    } else {
      this.AntennaList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class CheckInStatVM
    {
        public CheckInStatVM() { }

        public string CheckInStatus;
        public string JobType;
        public int BicycleID;
    }
}
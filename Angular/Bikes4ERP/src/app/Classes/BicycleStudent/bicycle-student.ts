export class BicycleStudent {

    //The Constructor
    constructor(){}

    //Data Fields
    BicycleStudentID : number;
    BicycleID : number;
    StudentID : number;
    DateAssigned : string;
    DateUnassigned : string;

}//BicycleStudent
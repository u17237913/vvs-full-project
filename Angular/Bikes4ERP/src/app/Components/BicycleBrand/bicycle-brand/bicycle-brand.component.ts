import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BicycleBrandService } from 'src/app/Services/BicycleBrand/bicycle-brand.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';

@Component({
  selector: 'app-bicycle-brand',
  templateUrl: './bicycle-brand.component.html',
  styleUrls: ['./bicycle-brand.component.css']
})
export class BicycleBrandComponent implements OnInit {

  BicycleBrandList : BicycleBrand []=[];
  public values = [];
  BicycleBrandForm : BicycleBrand;
  public TempArray: BicycleBrand[] = [];
  public SearchArray: BicycleBrand[] = [];
  isArrayStored: boolean = false;
  BicycleBrandArray: BicycleBrand[] = [];
  data = false;
  mySubscription: any;

  constructor(
    private router: Router,
    private BicycleBrandService: BicycleBrandService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    this.GetBicycleBrands();
  } //ngOnInit

  GetBicycleBrands(){
    this.BicycleBrandService.getBicycleBrands().subscribe(data => data.forEach(element => { 
      var bicyclebrand = new BicycleBrand();
      bicyclebrand.BicycleBrandID = element.BicycleBrandID;
      bicyclebrand.BicycleBrandName = element.BicycleBrandName;

        this.BicycleBrandArray.push(bicyclebrand);
      })
    );
  }
  EditBicycleBrand(ids: number) {
    this.router.navigate(['/viewbicyclebrand', { id: ids }],{ skipLocationChange: true });
  } //EditBicycleBrand

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  SearchAudits(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.BicycleBrandArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.BicycleBrandArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.BicycleBrandArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

      this.BicycleBrandArray = this.SearchArray;
    } else {
      this.BicycleBrandArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }

} //OnInit
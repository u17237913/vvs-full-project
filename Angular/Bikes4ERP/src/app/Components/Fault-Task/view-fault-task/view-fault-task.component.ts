import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { take } from 'rxjs/operators';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { Part } from 'src/app/Classes/Part/part';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';

@Component({
  selector: 'app-view-fault-task',
  templateUrl: './view-fault-task.component.html',
  styleUrls: ['./view-fault-task.component.css'],
})
export class ViewFaultTaskComponent implements OnInit {
  PartFaultForm: any;
  data = false;
  message: string;
  ID: any;
  FaultType: FaultType[] = [];
  Parts: Part[] = [];
  oldData: any;

  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public route: ActivatedRoute,
    public MaintenanceTaskService: MyServiceService
  ) {}

  ngOnInit(): void {
    this.PartFaultForm = new FormGroup({
      PartFaultID: new FormControl(),
      PartID: new FormControl(),
      FaultTypeID: new FormControl(),
      // FaultImage: new FormControl(),
    });
    this.getFaults();
    this.getParts();

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.MaintenanceTaskService.GetPartFault().subscribe((data) =>
      data.forEach((element) => {
        console.log(element.PartFaultID);
        if (element.PartFaultID == this.ID) {
          this.PartFaultForm = this.formbuilder.group({
            PartFaultID: [element.PartFaultID, [Validators.required]],
            FaultTypeID: [element.FaultTypeID, [Validators.required]],
            PartID: [element.PartID, [Validators.required]],
            FaultImage: [element.FaultImage, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );
  }

  getParts() {
    this.MaintenanceTaskService.GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  getFaults() {
    this.MaintenanceTaskService.GetFaultTypes()
      .pipe(take(1))
      .subscribe((Faults) => {
        this.FaultType = Faults;
        // console.log(this.UserTypes);
      });
  }

  onFormSubmit(PartFaultForm) {
    console.log();
    const MaintenanceTask = PartFaultForm.value;
    this.UpdateMaintenanceTask(this.ID, MaintenanceTask);
  }
  UpdateMaintenanceTask(ID: any, PartFaultForm: PartFault) {
    this.MaintenanceTaskService.UpdatePartFault(
      ID,
      PartFaultForm, this.oldData
    ).subscribe(() => {});
    console.log(ID, PartFaultForm);
  }

  GoBack() {
    this.router.navigate(['faulttask']);
  }

  DeleteMaintenanceTask(MaintenanceTaskID: any) {
    MaintenanceTaskID = this.ID;
    this.MaintenanceTaskService.DeletePartFault(MaintenanceTaskID, this.oldData).subscribe(
      () => {
        this.data = true;
      }
    );
  }
}

export class Antenna {

    //The Constructor
    constructor(){}

    //Data Fields
    AntennaID : number;
    AntennaStatusID : number;
    SuburbID : number;
    CoOrdinates : string;
    DateInstalled : string;
    SuburbName : string;
   // AntennaSerialNumber : string;
    AntennaStatus : string
    AntennaAddress : string;
    Position1: number;
    Position2: number;

}//Antenna
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBicyclePartComponent } from './view-bicycle-part.component';

describe('ViewBicyclePartComponent', () => {
  let component: ViewBicyclePartComponent;
  let fixture: ComponentFixture<ViewBicyclePartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBicyclePartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBicyclePartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router'
import { SchoolService } from 'src/app/Services/School/school.service'
import { School } from 'src/app/Classes/School/school'
import { FormBuilder } from '@angular/forms'
import { Village } from 'src/app/Classes/Village/village';
import { ViewSchoolComponent} from 'src/app/Components/School/view-school/view-school.component'
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {

  SchoolList : School [] = [];
  public values = [];
  SchoolForm : School;
  public TempArray: School[] = [];
  public SearchArray: School[] = [];
  isArrayStored: boolean = false;
 
  constructor(  public route : Router, private schoolService : SchoolService ) { }
 
   ngOnInit(): void {
 
     this.GetSchools();

   }

   GetSchools (){
     this.schoolService.GetSchools().subscribe((data) => {
       this.SchoolList = data as School[];
     })
   }

   CreateSchool(){
     this.route.navigate(['/createschool'])
   }

   ViewSchool (ids : number){
     this.route.navigate(['viewschool', { id : ids }],{ skipLocationChange: true })
     console.log(ids)
   }

   SearchOptions() {
    var myModal = new Bootstrap.Modal(
      document.getElementById('SearchOptionsModal')
    );
    myModal.show();
  }

  SearchSchools(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.SchoolList.slice();
      this.isArrayStored = true;
    }

    if (event.target.value != '') {
      this.SearchArray = [];
      this.SchoolList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.SchoolList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.SchoolList.forEach((school) => {
        var SearchString: string = event.target.value;
        if (
          school.SchoolName.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(school);
        }
      }); // For each
*/

      this.SchoolList = this.SearchArray;
    } else {
      this.SchoolList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
 }
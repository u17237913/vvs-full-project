import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAntennaComponent } from './view-antenna.component';

describe('ViewAntennaComponent', () => {
  let component: ViewAntennaComponent;
  let fixture: ComponentFixture<ViewAntennaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAntennaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAntennaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

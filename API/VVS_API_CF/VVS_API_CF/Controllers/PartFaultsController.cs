﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.Services;
using System.Web;
using System.IO;

namespace VVS_API_CF.Controllers
{
    public class PartFaultsController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/PartFaults
        [System.Web.Http.Route("api/PartFaults/GetPartFaults")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetPartFaults()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartFaultList(db.PartFault.Include(z => z.Part).Include(x => x.FaultType).ToList());
        }

        private List<dynamic> getPartFaultList(List<PartFault> forClient)
        {
            List<dynamic> dynamicUserTypes = new List<dynamic>();
            foreach (PartFault partFault in forClient)
            {
                dynamic dynamicType = new ExpandoObject();
                dynamicType.PartFaultID = partFault.PartFaultID;
                dynamicType.PartID = partFault.PartID;
                dynamicType.PartName = partFault.Part.PartName;
                dynamicType.FaultTypeID = partFault.FaultTypeID;
                dynamicType.FaultType1 = partFault.FaultType.FaultType1;
                

                dynamicUserTypes.Add(dynamicType);
            }
            return dynamicUserTypes;
        }

        //parts
        [System.Web.Http.Route("api/PartFaults/GetParts")]
        [System.Web.Mvc.HttpGet]


        public List<dynamic> GetParts()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getPartsList(db.Part.ToList());
        }
        private List<dynamic> getPartsList(List<Part> forClient)
        {
            List<dynamic> dynamicParts = new List<dynamic>();
            foreach (Part part in forClient)
            {
                dynamic dynamicPart = new ExpandoObject();
                dynamicPart.PartID = part.PartID;
                dynamicPart.PartName = part.PartName;




                dynamicParts.Add(dynamicPart);
            }
            return dynamicParts;
        }

        //faults
        [System.Web.Http.Route("api/PartFaults/GetFaults")]
        [System.Web.Mvc.HttpGet]


        public List<dynamic> GetFaults()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getFaultsList(db.FaultType.ToList());
        }
        private List<dynamic> getFaultsList(List<FaultType> forClient)
        {
            List<dynamic> dynamicParts = new List<dynamic>();
            foreach (FaultType faultType in forClient)
            {
                dynamic dynamicPart = new ExpandoObject();
                dynamicPart.FaultTypeID = faultType.FaultTypeID;
                dynamicPart.FaultType1 = faultType.FaultType1;




                dynamicParts.Add(dynamicPart);
            }
            return dynamicParts;
        }

        // GET: api/PartFaults/5
        [ResponseType(typeof(PartFault))]
        public async Task<IHttpActionResult> GetPartFault(int id)
        {
            PartFault partFault = await db.PartFault.FindAsync(id);
            if (partFault == null)
            {
                return NotFound();
            }

            return Ok(partFault);
        }

        // PUT: api/PartFaults/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartFault(int id, PartFault partFault)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partFault.PartFaultID)
            {
                return BadRequest();
            }

            db.Entry(partFault).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartFaultExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // POST: api/PartFaults
        [ResponseType(typeof(PartFault))]
        public async Task<IHttpActionResult> PostPartFault(PartFault partFault)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartFault.Add(partFault);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = partFault.PartFaultID }, partFault);
        }

        // DELETE: api/PartFaults/5
        [ResponseType(typeof(PartFault))]
        public async Task<IHttpActionResult> DeletePartFault(int id)
        {
            PartFault partFault = await db.PartFault.FindAsync(id);
            if (partFault == null)
            {
                return NotFound();
            }

            db.PartFault.Remove(partFault);
            await db.SaveChangesAsync();

            return Ok(partFault);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartFaultExists(int id)
        {
            return db.PartFault.Count(e => e.PartFaultID == id) > 0;
        }



        // add image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/PartFaults/UploadFile/{ID}")]
        [System.Web.Http.Route("api/PartFaults/UploadFile")]
        [System.Web.Mvc.HttpPost]
        public bool UploadFile(/*int ID, */[FromBody] FileModel file)
        {
 
            //this is the static file path on the server, change it to a relative path on deployment
            //var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Faults/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                //var _section = db.Section.Where(sc => sc.SectionID == ID).FirstOrDefault();
                //Get the last Section that was created
                var _partfault = db.PartFault.OrderByDescending(sc => sc.PartFaultID).FirstOrDefault();

                ////create the partfault object
                //PartFault mypartfault = new PartFault();
                ////looping thru to find partID
                //foreach (Part mypart in db.Part)
                //{
                //    if (mypart.PartName == faultType.PartNeeded)
                //    {
                //        mypartfault.PartID = mypart.PartID;
                //    }

                //}

                _partfault.FaultImage = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file upload tanx ------------------------------------>---------------------------------->---------->



        // read image file uploaded plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/PartFaults/GetFile/{ID}")]
        [System.Web.Http.Route("api/PartFaults/GetFile")]
        [System.Web.Mvc.HttpGet]
        public FileModel GetFile(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                FileModel EventFile = new FileModel();
                //retrieve the file path

                //var StudentNotAtSchoolString = db.PartFault.Find(ID).PartFaultID;

                var _par = db.PartFault.Where(sec => sec.FaultTypeID == ID).Select(u=>u.PartFaultID).FirstOrDefault();
                var _partfault = db.PartFault.Where(sec => sec.PartFaultID == _par).FirstOrDefault();

                //get the file as a byte array
                Byte[] bytes = File.ReadAllBytes(_partfault.FaultImage);

                //allocate variables to dynamic object
                EventFile.fileName = _partfault.FaultImage;

                //convert byte array to base64
                EventFile.fileAsBase64 = Convert.ToBase64String(bytes);
                return EventFile;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
        }
        // finished image file read tanx ------------------------------------>---------------------------------->---------->



        // update image file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/PartFaults/UpdateFile/{ID}")]
        [System.Web.Http.Route("api/PartFaults/UpdateFile")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateFile(int ID, [FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            // var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";

            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Images/Faults/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                //var _par = db.PartFault.Where(sec => sec.FaultTypeID == ID).Select(u => u.PartFaultID).FirstOrDefault();
                //var _partfault = db.PartFault.Where(sec => sec.PartFaultID == _par).FirstOrDefault();
                //CURRENT

                //var _partfault = db.PartFault.Where(sc => sc.PartFaultID == ID).FirstOrDefault();
                //var _section = db.Section.OrderByDescending(sc => sc.SectionID).FirstOrDefault();
                //OLD

                //create the partfault object
                PartFault mypartfault = new PartFault();
                FaultType myfaulttype = new FaultType();

                //looping thru to find FaultTypeID
                foreach (FaultType _myfaulttype in db.FaultType)
                {
                    if (_myfaulttype.FaultTypeID == ID)
                    {
                        myfaulttype = _myfaulttype;
                    }
                }
                    foreach (PartFault _mypartfault in db.PartFault)
                    {
                        if (_mypartfault.FaultTypeID == myfaulttype.FaultTypeID)
                        {
                            mypartfault = _mypartfault;
                        }
                    }

                mypartfault.FaultImage = filePathName;

                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished image file update tanx ------------------------------------>---------------------------------->---------->


        /////////////////////////////////////FOR SOME REASON I DO THIS FOR THE BLUEPRINT SCREEN WHEN REPORTING A FAULT JOB
        ///

        // read image file uploaded plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/PartFaults/GetFile/{ID}")]
        [System.Web.Http.Route("api/PartFaults/GetImage")]
        [System.Web.Mvc.HttpGet]
        public FileModel GetImage(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                FileModel EventFile = new FileModel();
                //retrieve the file path

                var _partfault = db.PartFault.Where(sec => sec.PartFaultID == ID).FirstOrDefault();

                //get the file as a byte array
                Byte[] bytes = File.ReadAllBytes(_partfault.FaultImage);

                //allocate variables to dynamic object
                EventFile.fileName = _partfault.FaultImage;

                //convert byte array to base64
                EventFile.fileAsBase64 = Convert.ToBase64String(bytes);
                return EventFile;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
        }
        // finished image file read tanx ------------------------------------>---------------------------------->---------->

    }
}
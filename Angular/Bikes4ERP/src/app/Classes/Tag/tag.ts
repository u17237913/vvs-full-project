export class Tag {
    constructor (){}

    TagID : number
    TagSerialNumber : string
    TagStatusID : number
    DateInstalled : string 
    BicycleCode : number
    TagStatus : string
}

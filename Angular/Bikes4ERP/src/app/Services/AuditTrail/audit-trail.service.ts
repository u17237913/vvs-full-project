import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuditTrail } from 'src/app/Classes/AuditTrail/audit-trail';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuditTrailService {
  newAudit: AuditTrail;
  currentID: number;
  temp: any;
  url : string;
  header : any;
  option : any;

  constructor(private http: HttpClient) {
    // this.url = 'https://localhost:44348/api/AuditTrails/';
    // const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    // let headers = new HttpHeaders({'Content-Type': 'application/json'});
    // this.option = {headers : headers};
   }

  addAuditTrail(type: number, table: string, dataNew?: any, dataOld?: any){

    this.newAudit = new AuditTrail();

    var descrip = "";
    var tType = "";

    var currentDateTime = new Date();
    var date = currentDateTime.getFullYear()+'/'+(currentDateTime.getMonth()+1)+'/'+currentDateTime.getDate();
    var time = currentDateTime.getHours() + ":" + currentDateTime.getMinutes() + ":" + currentDateTime.getSeconds();
    var dateTime = date+' '+time;

    this.temp = localStorage.getItem('UserID');
    this.currentID = parseInt(this.temp);

    switch (type){
      case 1: /*Create*/
        tType = "Create";
        descrip = "The following data: " + JSON.stringify(dataNew) + " was added to the " + table + " table.";
        break;

      case 2: /*Update*/
        tType = "Update";
        descrip = "The following data: " + JSON.stringify(dataOld) + " was updated in the " + table + " table with the following: " + JSON.stringify(dataNew);
        break;

      case 3: /*Delete*/
        tType = "Delete";
        descrip = "The following data: " + JSON.stringify(dataOld) + " was deleted from the " + table + " table.";
        break;

      // case 4: /*Read*/
      //   tType = "Read";
      //   if (!table) {
      //     descrip = "Data was read from the database.";
      //   }else{
      //     descrip = "Data was read from the " + table + " table.";
      //   }
      //   break;
    }

    this.newAudit.UserID = this.currentID;
    this.newAudit.DateTime = dateTime;
    this.newAudit.TransactionType = tType;
    this.newAudit.TransactionDescription = descrip;

    return this.http.post(environment.apiURL + '/AuditTrails/PostAuditTrail/', this.newAudit);

  }

  GetAudits () : Observable <AuditTrail[]>  {
    
    return this.http.get<AuditTrail[]>(environment.apiURL + '/AuditTrails/GetAuditTrail')
  }

}

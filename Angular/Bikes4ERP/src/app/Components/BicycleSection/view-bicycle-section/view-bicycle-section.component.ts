import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { Section } from 'src/app/Classes/Section/section';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-view-bicycle-section',
  templateUrl: './view-bicycle-section.component.html',
  styleUrls: ['./view-bicycle-section.component.css'],
})
export class ViewBicycleSectionComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private bicyclePartService: BicyclePartServiceService,
    private uploadService: FileUploadServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {}

  //SectionForm : FormGroup;
  ID: number;
  data = false;
  message: string;
  // imageUrl: string;
  fileToUpload: File = null;
  thefile: FileToUpload = new FileToUpload();
  imageUrl: string = '../../../assets/images/img-placeholder.jpg';
  oldData: any;

  //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any;

  //If a picture from the API is available, then set this to true so it can
  //be used in the transformer function
  isPictureAvailable = false;
  Base64Picture: string;

  //variable for error messages - note
  messages: string[] = [];

  SectionForm = new FormGroup({
    SectionID: new FormControl(),
    SectionName: new FormControl(),
    SectionDescription: new FormControl(),
    //SectionImage : new FormControl()
  });

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.ID = id;
    });

    //PICKUP FROM HERE ---> READ image to display (WORKING)
    this.uploadService.getFile(this.ID).subscribe((data) => {
      this.thefile = data;
      this.Base64Picture = data.fileAsBase64;
      if (this.Base64Picture) {
        this.isPictureAvailable = true;
      }
      this.message = 'Image has been successfully loaded boiii !';
    }); //GetImage

    //inject service call to get current object details
    this.bicyclePartService.getSections().subscribe((data) =>
      data.forEach((element) => {
        if (element.SectionID == this.ID) {
          this.SectionForm = this.formBuilder.group({
            SectionID: [element.SectionID, Validators.required],
            SectionName: [element.SectionName, [Validators.required]],
            SectionDescription: [
              element.SectionDescription,
              [Validators.required],
            ],
            SectionImage: [element.SectionImage, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

    //var UploadButton = <HTMLInputElement> document.getElementById("hideME");
    var UploadArea = document.getElementById('UploadArea');
    //var UploadBtn = document.getElementById('UploadBtn');
    var ObjArea = document.getElementById('ObjArea');
    var CreateObj = document.getElementById('CreateObj');

    UploadArea.hidden = false;
    //UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;
  } //ngOnInit

  //this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];

        //Show image preview
        var reader = new FileReader();

        reader.readAsDataURL(this.theFile);
        reader.onload = () => {
          this.imageUrl = reader.result as string;
        };
      } else {
        //Display error message <-!-!-> watchout, this allows for multiple files...
        this.messages.push(
          'File: ' + event.target.files[0].name + 'is too large to upload.'
        );
      }
    }
  } //OnFileChange

  //This function will convert base 64 string to an image and return it
  transformer() {
    if (this.isPictureAvailable) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        'data:image;base64,' + this.Base64Picture
      );
    }
  }

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile, this.ID);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any, id: number) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.updateFile(file, id).subscribe((respondation) => {
        this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);

    //Nav back to the list
    //this.router.navigate(['/bicyclesection']);
  }

  onFormSubmit(SectionForm) {
    const section = SectionForm.value;
    this.EditSection(section);
  } //OnFormSubmit

  EditSection(section: Section) {
    this.bicyclePartService.UpdateSection(section, this.oldData).subscribe(() => {
      this.data = true;
      if (this.theFile != null) {
        this.uploadFile();
      } else {
        // this.router.navigate(['/bicyclesection']);
      }

      this.message = 'Section has been successfully updated boiii !';
      this.SectionForm.reset;
      console.log(this.SectionForm.value + 'we adding');
    }); //UpdateSection
  } //EditSection

  DeleteSection(id: number) {
    id = this.ID;
    this.bicyclePartService.DeleteSection(id, this.oldData).subscribe(() => {
      this.data = true;
      // this.router.navigate(['/bicyclesection']);
    });
  } //DeleteSection

  GoToList() {
    this.router.navigate(['/bicyclesection']);
  } //GoToList
} //OnInit

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { element } from 'protractor';
import { take } from 'rxjs/operators';
import { School } from 'src/app/Classes/School/school';
import { Antenna } from 'src/app/Classes/Antenna/antenna';
import { RouteService } from 'src/app/Services/Route/route.service';
import { Route } from 'src/app/Classes/Route/route';
import { SchoolService } from 'src/app/Services/School/school.service';
import { AntennaService } from 'src/app/Services/Antenna/antenna.service';
@Component({
  selector: 'app-view-route',
  templateUrl: './view-route.component.html',
  styleUrls: ['./view-route.component.css']
})
export class ViewRouteComponent implements OnInit {

  constructor(  
     public formBuilder: FormBuilder,
      public routeService : RouteService, 
      public schoolService : SchoolService, 
      public antennaService : AntennaService, 
      public route: ActivatedRoute, 
      public router: Router
      ) { }

      RouteForm : any;
      RouteList : Route;
      RouteID : number;
      data = false;
      Form : any;
      SchoolList : School[]=[];
      message: string;
      //AntennaID : any;
      AntennaList : Antenna[]=[];
      AntennaList1 : Antenna[]=[];
    ID: number;
      oldData: any;
    

  ngOnInit(): void {
    this.GetAntenna();
    this.GetAntennaPosition2(); 
    this.GetSchools();
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.RouteID = id; })

      this.GetAntenna();
      this.GetAntennaPosition2();
      this.GetSchools();
this.RouteForm = new FormGroup({
RouteID: new FormControl(),
  SchoolID: new FormControl(),
  Position1: new FormControl(),
  Position2: new FormControl(),

});
      this.routeService.GetRoutes().subscribe(data => data.forEach(element => {
        if (element.RouteID == this.RouteID){
       
          this.RouteForm = this.formBuilder.group({
            RouteID : [element.RouteID, [Validators.required]],
            SchoolID : [element.SchoolID, [Validators.required]],
            Position1 : [element.Position1, [Validators.required]],
            Position2 : [element.Position2, [Validators.required]],
          });// FormBuilder
          console.log(data);
          this.oldData = element;
          console.log(this.RouteForm.get.Position1);
        }// If
      })
      );

      // this.bicyclePartService.getParts().subscribe((data) => {
      //   data.forEach((element) => {
      //     if (element.PartID == this.ID) {
      //       this.PartForm = this.formBuilder.group({
      //         PartID: [element.PartID, Validators.required],
      //         PartName: [element.PartName, [Validators.required]],
      //         PartDescription: [element.PartDescription, [Validators.required]],
      //         SectionID: [element.SectionID, [Validators.required]],
      //         PartTypeID: [element.PartTypeID, [Validators.required]],
      //         PartImage: [element.PartImage, [Validators.required]],
      //       });
      //     }
      //   });
      // });

  }


  onFormSubmit(RouteForm){
    console.log();
    const route = RouteForm.value;
    this.UpdateRoute(this.RouteID, route);
  }

  UpdateRoute (RouteID : any, route : Route)
  {
    console.log(route)
     this.routeService.UpdateRoute(RouteID, route, this.oldData).subscribe(() => {
      this.data = true;
      this.message = "Route has been updated";
      this.RouteForm.reset;
     // this.router.navigate(['/route'])
    })
  }
  DeleteRoute (RouteID : any)
  {
    RouteID = this.RouteID;
    this.routeService.DeleteRoute(RouteID, this.oldData).subscribe(() => {
      this.data = true;
     // this.router.navigate(['/route'])
    })
  }
  

  GetSchools (){
    // this.schoolService.GetSchools().subscribe((data) => {
    //   this.SchoolList = data;
    //   console.log(this.SchoolList);
    // })
    this.schoolService
    .GetSchools()
    .pipe(take(1))
    .subscribe((data) => {
      this.SchoolList = data;
    });
  }

  GetAntenna (){
    this.antennaService.GetAntennas().subscribe((data) => {
      this.AntennaList = data;

      console.log(this.AntennaList);
    })
  }

  GetAntennaPosition2 (){
    this.antennaService.GetAntennas().subscribe((data) => {
      this.AntennaList1 = data;
    
      console.log(this.AntennaList1);
    })
  }

  GoBack() {
    this.router.navigate(['/route']);
  }

}
 
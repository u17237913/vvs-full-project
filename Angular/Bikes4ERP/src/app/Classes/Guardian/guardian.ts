export class Guardian {

    //The Constructor
    constructor(){}

    //Data Fields
    GuardianID : number;
    GuardianName : string;
    GuardianEmail : string;
    GuardianPhone : string;

}//Guardian
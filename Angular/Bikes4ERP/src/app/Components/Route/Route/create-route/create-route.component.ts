import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { School } from 'src/app/Classes/School/school';
import { Antenna } from 'src/app/Classes/Antenna/antenna';
import { RouteService } from 'src/app/Services/Route/route.service';
import { Route } from 'src/app/Classes/Route/route';
import { SchoolService } from 'src/app/Services/School/school.service';
import { AntennaService } from 'src/app/Services/Antenna/antenna.service';

@Component({
  selector: 'app-create-route',
  templateUrl: './create-route.component.html',
  styleUrls: ['./create-route.component.css']
})
export class CreateRouteComponent implements OnInit {

 
  RouteList : Route[] = [];
  SchoolList : School [] = [];
  AntennaList : Antenna [] = [];
  AntennaList1 : Antenna [] = [];
  Form : any;
  data = false;
  message: string;
  public values = [];
  ngModel : any;
  Position1 : number;
  Position2 : number;

  constructor(public route : Router, public routeService : RouteService, public schoolService : SchoolService, public antennaService : AntennaService) { }

  ngOnInit(): void {
    this.GetAntenna();
    this.GetAntennaPosition2();
    
    this.GetSchools();
    this.routeService.RouteForm = {
      ID: '',
      SchoolID: '',
      Position1: '',
      Position2:'',

    } 
 
  
  }
  onFormSubmit(RouteForm) {
  
   // this.CreateRoute(RouteForm)
  }

  CreateRoute(route : Route){
    console.log(route)
      this.routeService.AddRoute(route).subscribe(() => {
        this.data = true;
        this.message = 'Saved';
      //  this.onFormSubmit.reset();
      })
    }

  SaveRoute(){
    console.log(this.routeService.RouteForm)
    this.routeService
    .AddRoute(this.routeService.RouteForm)
    .subscribe(res => {});
  }

  GetSchools (){
    this.schoolService.GetSchools().subscribe((data) => {
      this.SchoolList = data;
      console.log(this.SchoolList);
    })
  }

  GetAntenna (){
    this.antennaService.GetAntennas().subscribe((data) => {
      this.AntennaList = data;

      console.log(this.AntennaList);
    })
  }

  GoBack() {
    this.route.navigate(['/route']);
  }

  GetAntennaPosition2 (){
    this.antennaService.GetAntennas().subscribe((data) => {
      this.AntennaList1 = data;
    
      console.log(this.AntennaList1);
    })
  }




      
    }




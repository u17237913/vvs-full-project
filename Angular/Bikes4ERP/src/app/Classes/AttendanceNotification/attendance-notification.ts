export class AttendanceNotification {

    //The Constructor
    constructor(){}

    //Data Fields
    NotificationID : number;
    StudentID : number;
    DateTime : string;

}//AttendanceNotification
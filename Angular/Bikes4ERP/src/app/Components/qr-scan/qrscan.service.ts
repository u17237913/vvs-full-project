import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { CheckIn } from 'src/app/Classes/CheckIn/check-in';
import { RepairJob } from 'src/app/Classes/RepairJob/repair-job';
import { JobFault } from 'src/app/Classes/JobFault/job-fault';

@Injectable({
  providedIn: 'root',
})
export class Qrscan {
  ID: number;
  Name: string;
  Surname: string;
  scannedID: number;

  CurrentJobFault: JobFault;
  CurrentJobFaultList: JobFault[] = [];
  Counter = 0;

  constructor(private http: HttpClient) {}

  getQRcode(): Observable<any> {
    return this.http.get<any>(
      environment.apiURL + '/QRScanner/GetQRcode?BicycleID=' + this.scannedID
    );
  }

  PutCheckIn(): Observable<any> {
    return this.http.get<any>(
      environment.apiURL + '/QRScanner/GetQRcode?ID=' + this.scannedID
    );
  }

  CreateCheckIn(NewCheckIn: CheckIn, BicycleCode: number) {
    return this.http.post<CheckIn>(
      environment.apiURL + '/CheckIns?BicycleID=' + BicycleCode,
      NewCheckIn
    );
  }

  CreateRepairJob(NewRepairJob: RepairJob) {
    return this.http.post<RepairJob>(
      environment.apiURL + '/RepairJobs/PostRepairJob',
      NewRepairJob
    );
  }
}

//https://localhost:44348/api/QRScanner/GetQRcode?BicycleID=1


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFaultTaskComponent } from './create-fault-task.component';

describe('CreateFaultTaskComponent', () => {
  let component: CreateFaultTaskComponent;
  let fixture: ComponentFixture<CreateFaultTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFaultTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFaultTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

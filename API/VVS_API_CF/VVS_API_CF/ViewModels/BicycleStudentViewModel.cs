﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.ViewModels
{
    public class BicycleStudentViewModel
    {
        public BicycleStudentViewModel() { }
        public int BicycleCode { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }
        public string BicycleStatus { get; set; }

        public string DateAssigned { get; set; }

    }
}
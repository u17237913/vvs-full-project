import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ParamMap, Router, ActivatedRoute } from '@angular/router';
import { StudentMarksService } from 'src/app/Services/StudentMarks/student-marks.service';
import { StudentMark } from 'src/app/Classes/StudentMark/student-mark';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';
import { StudentMarksVM } from 'src/app/Classes/StudentMark/student-marks-vm';
import { Chart } from 'chart.js';
// import * as Chart from 'chart.js';
import * as XLSX from 'xlsx';
import { MarkTypeService } from 'src/app/Services/MarkType/mark-type.service';

@Component({
  selector: 'app-capture-mark',
  templateUrl: './capture-mark.component.html',
  styleUrls: ['./capture-mark.component.css']
})
export class CaptureMarkComponent implements OnInit {
  @ViewChild('canvas') canvas/*: ElementRef*/;
  public context: CanvasRenderingContext2D;

  constructor( public router : Router, public studentMarksService : StudentMarksService, public route: ActivatedRoute, public markTypeService : MarkTypeService
    ) { }

    StudentID : any;
    dynamicStudentMarks : Object;
    dynamicStudentMarksList : any[];
    MarksVM : StudentMarksVM[]=[];
    MarkTypeList : MarkType[];
    StudentMarkList : StudentMark [] = [];
    StudentMarkForm : any;
    data = false;
    message: string;
    public values = [];
    ngModel : any;
    chart : Chart;
    ChartData : Object;
    nomarks : boolean = false;

    CurrentStudentName = "";

  ngOnInit(): void {

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.StudentID = id; })

      // this.GetMarkTypes();
        this.GetMarks();
        this.GetMarkTypes();
        this.GetStudentAllMarks();

        if(this.StudentMarkList == null || this.StudentMarkList.length == 0) {
          this.nomarks = true;
        }

    this.studentMarksService.StudentMarkForm = {
      StudentID : this.StudentID,
      MarkTypeID : 1,
      Mark : '', 
      Date : '',
    //  StudentID : ''
    }
  }

  /*name of the excel-file which will be downloaded. */
  excelfileName= 'StudentMark.xlsx';

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('excel-table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.excelfileName);
  }

  GetStudentAllMarks (){
    this.studentMarksService.GetStudentAllMarks().subscribe((data) => {
      this.MarksVM = data;

      var student = data.find( sn => sn.StudentID = this.StudentID );

      this.CurrentStudentName = student.StudentName + " " + student.StudentSurname
    })
  }

  
  GetMarkTypes (){
    this.markTypeService.getMarkTypes().subscribe((data) => {
      this.MarkTypeList = data;
    })
  }

  GetMarks (){
    //We destroy the chart if it exists so we can create a new one without the old one still taking memory and being loaded in the DOM
    if(this.chart) {this.chart.destroy();}

    this.studentMarksService.GetSpecificMarks(this.StudentID).subscribe((data) =>{
    this.StudentMarkList = data;
    // debugger;
    //this.StudentMarkList = data;

    //Get Data for the x-axis and the y-axis -!-!-> DATASET 1
    // debugger;
    // let keys = data['dynamicStudentMarks'].map((z: { Date:any; }) => z.Date);
    // let Vals = data['dynamicStudentMarks'].map((z: { Mark:any; }) => z.Mark);
    console.log(data);
    this.dynamicStudentMarksList = data;

    let keys = this.dynamicStudentMarksList.map((z) => z.Date);
    let Vals = this.dynamicStudentMarksList.map((z) => z.Mark);
    console.log(keys);

    // debugger;
    /*var ctx = document.getElementById("canvas").getContext('2d');
    this.chart = new Chart('canvas', {*/
      //this.chart = new Chart(this.canvas.nativeElement.getContext('2d'), {
      
      this.context = (<HTMLCanvasElement>this.canvas.nativeElement).getContext('2d');
      this.chart = new Chart(this.context, {
      type: 'bar',
      data: {
        labels: keys,
        datasets: [
          {
            label: 'Marks',
            data: Vals,
            fill : false,
            barPercentage: 0.75,
            backgroundColor: ['rgba(0, 24, 68, 0.8)'],
            borderColor: ['rgba(0, 24, 68, 1)'],
            borderWidth: 1,
            order: 1
          }],
        },
      options:
      {
        legend: { display: true },
        title: {
          display: true,
          text: 'My Marks'
              },
        scales: {
          xAxes: [{
            display: true,
            gridLines: { offsetGridLines: true }
            }],
          yAxes: [{
            ticks: { beginAtZero: true, max: 100 }
            }],
        }
      }
    }); 
  });//EndSubscription
}
/*    })
  }*/

  SaveMark(){
    this.studentMarksService.CaptureMark(this.studentMarksService.StudentMarkForm).subscribe(res  => {
      console.log(res);
    })
  }

  onFormSubmit (StudentMarkForm){
    this.CaptureMark(  StudentMarkForm)
  }

  CaptureMark (studentMark : StudentMark ){
    this.studentMarksService.CaptureMark(studentMark).subscribe((res) =>{
    this.data = true;
    this.StudentMarkForm.reset; 
    this.router.navigate(['/studentmarks'])
  
    console.log(res);
    // studentMark.StudentID == this.StudentID;
    })
  }

  GoToList() {
    this.router.navigate(['/studentmarks']);
  } //GoToList
}
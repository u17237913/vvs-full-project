import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add auth header with jwt if user is logged in and request is to the api url
    //const currentUser = this.authenticationService.currentUserValue;
    //const isLoggedIn = currentUser && currentUser.token;
    //const isApiUrl = request.url.startsWith(environment.apiUrl);
    if (
      localStorage.getItem('UserToken') != null ||
      localStorage.getItem('UserToken') != undefined
    ) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('UserToken')}`,
        },
      });
    }

    return next.handle(request);
  }
}

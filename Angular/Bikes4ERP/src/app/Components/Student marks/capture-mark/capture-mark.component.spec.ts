import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptureMarkComponent } from './capture-mark.component';

describe('CaptureMarkComponent', () => {
  let component: CaptureMarkComponent;
  let fixture: ComponentFixture<CaptureMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaptureMarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptureMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

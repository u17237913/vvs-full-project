import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './Services/user.model';
import { MyServiceService } from './Services/my-service.service';
import { Subscription, interval } from 'rxjs';
import { Router } from '@angular/router';
import * as Bootstrap from '../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LogoutTimer } from './Classes/Miscellaneous/logout-timer';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Bikes4ERP';
  //router: any;

  LogInSub: Subscription;
  LoginSub : Subscription;
  TokenSub: Subscription;
  ModalSub: Subscription;
  LoggedIn: boolean = true;

  UserRole: string = localStorage.getItem('UserType');

  constructor(public logInService: MyServiceService, public router: Router) {}

  public ngOnInit() {

    // this.LogInSub = this.logInService.LogInSubject.subscribe((LoggedIn) => {
    //   debugger;
    //   this.LoggedIn = LoggedIn;
    //   this.UserRole = localStorage.getItem('UserType');
    //   console.log(this.UserRole);
    // });
    // this.TokenSub = this.logInService.TokenSubject.subscribe((exp) => {
    //   debugger;
    //   if (exp > 0 && this.LoggedIn == true) {
    //     var seconds = new Date().getTime() / 1000;
    //     const source = interval((exp - seconds) * 1000);
    //     this.ModalSub = source.subscribe((res) => {
    //       var myModal = new Bootstrap.Modal(
    //         document.getElementById('TokenModal')
    //       );
    //       myModal.show();
    //     });
    //   }
    // });


    this.LoginSub = this.logInService.LoginSubject.subscribe( (res) => {
      /*
        We are checking if res._LoggedIn has a value so that if we only 
        want to change only the value of the timer then that can be done
      */
      if (res._LoggedIn != null) {
        this.LoggedIn = res._LoggedIn;
        this.UserRole = localStorage.getItem('UserType');
      }

      /*
        Logout Timer Section
        The purpose of this part of the code is to create a timer based on the expiry of the token,
        when the timer reaches 0 then a modal should be displayed that asks the user if they want to
        remain logged in or not
      */
      if ((res._Exp != null || res._Exp != undefined) ) {
      
        //get time in seconds up to now from Jan 1 1970 00:00
        var seconds = new Date().getTime() / 1000;

        //Get how long the timer should be in seconds
        var TimerLength = res._Exp - seconds;

        //Multiply by 1 thousand to get milliseconds
        const Timer = interval((TimerLength) * 1000);

        this.ModalSub = Timer.pipe(take(1)).subscribe( (result) => {
          if (res._Exp > 0) {
            var myModal = new Bootstrap.Modal(
              document.getElementById('TokenModal')
            );
            myModal.show();
          }
        });
      }
    });
  }

  RefreshToken() {
    this.logInService.RefreshToken().subscribe((newToken) => {
      localStorage.setItem('UserToken', newToken.token);
      const helper = new JwtHelperService();
      const DecodedToken = helper.decodeToken(newToken.token);
      //this.logInService.LogInSubject.next(true);
      var logoutTimer = new LogoutTimer( DecodedToken.exp ,  true);
      this.logInService.LoginSubject.next(logoutTimer);
      //this.logInService.TokenSubject.next(DecodedToken.exp);
    });
  }

  LogOut() {
    localStorage.removeItem('UserToken');
    localStorage.removeItem('UserTypeID');
    localStorage.removeItem('UserType');
    localStorage.removeItem('UserID');
    localStorage.removeItem('UserRole');
    localStorage.removeItem('RefreshToken');
    var logoutTimer = new LogoutTimer( 0 ,  false);
    this.logInService.LoginSubject.next(logoutTimer);
    this.ModalSub.unsubscribe();
    this.router.navigate(['/homepage'])

  }

  GoToHelp()
  {
    window.open("https://victorious-dune-0504a6a10.azurestaticapps.net/", "_blank");
  }


  getHelp(){

    var helpCheck = this.router.url.split(";", 1).toString();
    var HelpURL = "https://victorious-dune-0504a6a10.azurestaticapps.net/pages/"
    switch (helpCheck) {

      case "/login":
        window.open( HelpURL + "login-screen.html", "_blank")
        break;

      case "/forgotpassword":
        window.open (HelpURL + "forgot-password-screen.html", "_blank" )
        break;

      case "/assigntagbicycle":
        window.open (HelpURL + "assign-tag-screen.html", "_blank")
        break;

      case "/assigntag":
        window.open (HelpURL + "tag-assignment-screen.html", "_blank")
        break;

      case "/assignbicyclestudent":
        window.open(HelpURL + "assign-bicycle-screen.html")
        break;

      case "/assignbicycle":
        window.open (HelpURL + "bicycle-assignment-screen.html", "_blank")
        break;

      case "/studentmarks":
        window.open (HelpURL + "student-mark-management-screen.html", "_blank" )
        break;

      case "/capturemark":
        window.open (HelpURL + "capture-student-mark-screen.html", "_blank")
        break;

      case "/document":
        window.open (HelpURL + "document-management-screen.html", "_blank")
        break;

      case "/bicycle":
        window.open (HelpURL + "bicycle-management-screen.html", "_blank")
        break;

      case "/createbicycle":
        window.open (HelpURL + "create-bicycle-screen.html", "_blank")
        break;

      case "/viewbicycle":
        window.open (HelpURL + "view-bicycle-screen.html", "_blank")
        break;

      case "/marktype":
        window.open (HelpURL + "mark-type-management-screen.html", "_blank")
        break;

      case "/createmarktype":
        window.open (HelpURL + "create-mark-type-screen.html", "_blank")
        break;

      case "/createmarktype":
        window.open (HelpURL + "view-mark-type-screen.html", "_blank")
        break;

      case "/student":
        window.open (HelpURL + "school-management-screen.html", "_blank")
        break;

      case "/createstudent":
        window.open (HelpURL + "create-student-screen.html", "_blank")
        break;

      case "/viewstudent":
        window.open (HelpURL + "view-student-screen.html", "_blank")
        break;

      case "/antenna":
        window.open (HelpURL + "antenna-management-screen.html", "_blank")
        break;

      case "/createantenna":
        window.open(HelpURL + "create-antenna-screen.html", "_blank")
        break;

      case "/viewantenna":
        window.open(HelpURL + "view-antenna-screen.html", "_blank")
        break;

      case "/bicyclebrand":
        window.open (HelpURL + "bicycle-brand-management-screen.html", "_blank")
        break;

      case "/createbicyclebrand":
        window.open (HelpURL + "create-bicycle-brand-screen.html", "_blank")
        break;

      case "/viewbicyclebrand":
        window.open (HelpURL + "view-bicycle-brand-screen.html", "_blank")
        break;

      case "/bicyclesection":
        window.open (HelpURL + "bicycle-section-management-screen.html", "_blank")
        break;

      case "/createbicyclesection":
        window.open (HelpURL + "create-bicycle-section-screen.html", "_blank")
        break;

      case "/viewbicyclesection":
        window.open (HelpURL + "view-bicycle-section-screen.html", "_blank")
        break;

      case "/businessrule":
        window.open (HelpURL + "business-rule-management-screen.html", "_blank")
        break;

      case "/viewbusinessrule":
        window.open (HelpURL + "view-business-rule-screen.html", "_blank")
        break;

      case "/faulttask":
        window.open (HelpURL + "fault-task-management-screen.html", "_blank")
        break;

      case "/createfaulttask":
        window.open (HelpURL + "create-fault-task-screen.html", "_blank")
        break;

      case "/viewfaulttask":
        window.open (HelpURL + "view-fault-task-screen.html", "_blank")
        break;

      case "/faulttype":
        window.open (HelpURL + "fault-type-management-screen.html", "_blank")
        break;

      case "/createfaulttype":
        window.open (HelpURL + "create-fault-type-screen.html", "_blank")
        break;

      case "/viewfaulttype":
        window.open (HelpURL + "view-fault-type-screen.html", "_blank")
        break;

      case "/maintenancetask":
        window.open (HelpURL + "maintenance-task-management-screen.html", "_blank")
        break;

      case "/createmaintenancetask":
        window.open (HelpURL + "create-maintenance-task-screen.html", "_blank")
        break;

      case "/viewmaintenancetask":
        window.open (HelpURL + "view-maintenance-task-screen.html", "_blank")
        break;
        
      // case "/parttype":
      //   window.open (HelpURL + "part-type-management-screen.html", "_blank")
      //   break;

      // case "/createparttype":
      //   window.open (HelpURL + "create-part-type-screen.html", "_blank")
      //   break;

      // case "/viewparttype":
      //   window.open (HelpURL + "view-part-type-screen.html", "_blank")
        break;

      case "/route":
        window.open (HelpURL + "route-management-screen.html", "_blank")
        break;

      case "/createroute":
        window.open(HelpURL + "create-route-screen.html", "_blank")
        break;

      case "/viewroute":
        window.open(HelpURL + "view-route-screen.html", "_blank")
        break;

      case "/school":
        window.open(HelpURL + "school-management-screen.html", "_blank")
        break;

      case "/createschool":
        window.open(HelpURL + "create-school-screen.html", "_blank")
        break;

      case "/viewschool":
        window.open(HelpURL + "view-school-screen.html", "_blank")
        break;

      case "/tag":
        window.open(HelpURL + "tag-management-screen.html", "_blank")
        break;

      case "/createtag":
        window.open(HelpURL + "create-tag-screen.html", "_blank")
        break;

      case "/viewtag":
        window.open(HelpURL + "view-tag-screen.html", "_blank")
        break;


      case "/bicyclepart":
          window.open(HelpURL + "bicycle-part-management-screen.html", "_blank")
          break;
  
      case "/createbicyclepart":
          window.open(HelpURL + "create-bicycle-part-screen.html", "_blank")
          break;
  
     case "/viewbicyclepart":
        window.open(HelpURL + "view-bicycle-part-screen.html", "_blank")
          break;
      case "/mechaniccalendar" :
        window.open(HelpURL + "mechanic-calendar-screen.html","_blank")
        break;

      case "/logentry":
        window.open(HelpURL + "log-entry-screen.html", "_blank") 
        break;

      case "/user":
        window.open(HelpURL + "user-management-screen.html", "_blank")
        break;

      case "/createuser":
        window.open(HelpURL + "create-user-screen.html", "_blank")
        break;

      case "/viewuser":
        window.open(HelpURL + "view-user-screen.html", "_blank")
        break;

      case "/usertype":
        window.open(HelpURL + "user-type-management-screen.html", "_blank")
        break;

      case "/createusertype":
        window.open(HelpURL + "create-user-type-screen.html", "_blank")
        break;

      case "/viewusertype":
        window.open(HelpURL + "view-user-type-screen.html", "_blank")
        break;

      case "/bicyclehistoryreport":
        window.open(HelpURL + "bicycle-history-report.html")
        break;

      case "/logreport":
        window.open(HelpURL + "log-report.html", "_blank")
        break;

      case "/mechanicschedulereport":
        window.open (HelpURL + "mechanic-schedule-report.html", "_blank" )
        break;

      case "/mechanicschedulerequirementsreport":
        window.open (HelpURL + "mechanic-schedule-requirements-report.html", "_blank" )
        break;

      case "/partsusedreport":
        window.open(HelpURL + "parts-used-report.html", "_blank" )
        break;

      case "/studentreport":
        window.open(HelpURL + "generate-student-report.html", "_blank")
        break;

      case "/qrscan":
        window.open(HelpURL + "qr-scan-screen.html", "_blank")
          break;

      case "/audittrail":
        window.open(HelpURL + "audit-trail-management-screen.html", "_blank")
        break;

      case "/bicycleblueprint":
        window.open(HelpURL + "bicycle-blueprint-screen.html", "_blank")
        break;

      case "/partfaults":
        window.open(HelpURL + "part-faults-screen.html", "_blank")
        break;

      case "/reportlist":
        window.open(HelpURL + "report-job-screen.html", "_blank")
        break;

      case "/joblist":
        window.open(HelpURL + "job-screen.html", "_blank")
        break;

      case "/sectionblueprint":
        window.open(HelpURL + "section-blueprint-screen.html")
        break;

      case "/passwordreset":
        window.open(HelpURL + "forgot-password-screen.html")
        break;

      default:
        window.open ("https://victorious-dune-0504a6a10.azurestaticapps.net/", "_blank")
        break;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators, FormGroup, FormControl, ReactiveFormsModule} from '@angular/forms'
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import { JobType } from 'src/app/Classes/JobType/job-type';
import { School } from 'src/app/Classes/School/school';
import { take } from 'rxjs/operators';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';

@Component({
  selector: 'app-bicycle-history-report',
  templateUrl: './bicycle-history-report.component.html',
  styleUrls: ['./bicycle-history-report.component.css']
})
export class BicycleHistoryReportComponent implements OnInit {
  title = 'VVS - Bicycle History Report';
  selectedSchool : any;
  selectedJobType : any;
  showErrorMessage : boolean = false;
  TableData : Object;
  totalage : any;
  totalJobs : number;

    JobTypes : JobType[] = [];
    JobTypeID : any;
    Schools : School[] = [];
    SchoolID : any;
    counter : any;

  constructor(private ReportingService : ReportingServiceService) { }

  ngOnInit(): void {
    this.GetJobTypes();
    this.GetSchools();
    this.counter = 0;

  }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'BicycleHistoryReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    if (this.totalJobs > 0) {
    /* save to file */
    XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.totalJobs == 0){ alert('No Bicycle History Report Data for the chosen Job Type in the chosen School to download.\n Please capture new dates and try again.'); }
  }

  // downloadPDF() {
  //   this.ReportingService.getBicycleHistoryReportData(this.selectedSchool, this.selectedJobType).subscribe((res) => {

  //   let doc = new jsPDF('l', 'pt', 'a4');

  //   let finalY = 100;

  //   var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
  //   var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

  //   doc.setFontSize(20)
  //   doc.text("Bicycle History Report", (150/3)-5,15)

  //   doc.setFontSize(14)
  //   for (let index = 0; index < length; index++) {
  //     //@ts-ignore
  //     doc.autoTable({startY : finalY + 15, html:'#table'+index,useCss:true/*, head: [
  //     ['Job Code', 'Bicycle Code', 'Student Name', 'Job Type', 'Student Contact', 'Guardian Contact', 'Date Scheduled']]*/})
  //       //@ts-ignore
  //       finalY = doc.autoTable.previous.finalY;
  //     }
  //     doc.save('BicycleHistoryReport.pdf');
  //   })
  // }

  GenerateReport() {
    //check if an option is selected otherwise Show error message
    if(this.selectedSchool == undefined || this.selectedJobType == undefined) {
      this.showErrorMessage == true;
    } else {
      this.showErrorMessage = false;

      this.ReportingService.getBicycleHistoryReportData(this.selectedSchool, this.selectedJobType).subscribe((res) => {
        console.log(res);
        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;

        //Get the Total for the control break
        let totality = res['TableData'].map((z: { TotalJobs: any; }) => z.TotalJobs);
        const sum = totality.reduce((a: any,b: any) => a + b, 0);
        this.totalage = sum;

        console.log(this.totalage);
      })
    }

    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) {
      var tableWithData = document.getElementById('table');

      tableWithData.hidden = false;
        console.log(this.totalJobs)
      }
      else if (this.totalJobs == 0){ alert('No Bicycle History Report Data for the chosen Job Type in the chosen School.\n Please capture different search criteria and try again.'); }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 2 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 2000);
        this.counter++;
      }
  }

    // EXTRAS NEEDED FOR THE dropdowns --------------------------
    GetJobTypes() {
      this.ReportingService.getJobTypes().pipe(take(1)).subscribe((JobType) => {
        this.JobTypes = JobType;
      })
    }
  
    GetSchools() {
      this.ReportingService.getSchools().pipe(take(1)).subscribe((School) => {
        this.Schools = School;
      })
    }
    // EXTRAS END HERE ------------------------------------------
}
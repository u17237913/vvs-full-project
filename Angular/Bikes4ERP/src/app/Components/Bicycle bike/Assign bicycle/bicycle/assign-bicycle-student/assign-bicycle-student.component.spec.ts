import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignBicycleStudentComponent } from './assign-bicycle-student.component';

describe('AssignBicycleStudentComponent', () => {
  let component: AssignBicycleStudentComponent;
  let fixture: ComponentFixture<AssignBicycleStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignBicycleStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignBicycleStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

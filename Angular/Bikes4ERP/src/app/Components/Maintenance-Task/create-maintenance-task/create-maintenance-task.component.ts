import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { MaintenanceTask } from 'src/app/Classes/MaintenanceTask/maintenance-task';
import { take } from 'rxjs/operators';
import { MaintenanceType } from 'src/app/Classes/MaintenanceType/maintenance-type';
import { Part } from 'src/app/Classes/Part/part';

@Component({
  selector: 'app-create-maintenance-task',
  templateUrl: './create-maintenance-task.component.html',
  styleUrls: ['./create-maintenance-task.component.css'],
})
export class CreateMaintenanceTaskComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public MaintenanceTaskService: MyServiceService
  ) {}

  MaintenanceTypes: MaintenanceType[] = [];
  Parts: Part[] = [];

  ngOnInit(): void {
    this.MaintenanceTaskService.MaintenanceTaskForm = {
      MaintenanceTaskID: 0,
      MaintenanceTaskDescription: '',
      MaintenanceTask1: '',
      MaintenanceTypeID: 0,
      PartID: 0,
    };
    this.getMaintenanceTypes();
    this.getParts();
  }

  SaveMaintenanceTask() {
    console.log(this.MaintenanceTaskService.MaintenanceTaskForm);
    this.MaintenanceTaskService.AddMaintenanceTask(
      this.MaintenanceTaskService.MaintenanceTaskForm
    ).subscribe((res) => {
      console.log(res);
    });
  }

  getMaintenanceTypes() {
    this.MaintenanceTaskService.GetMaintenanceTypes()
      .pipe(take(1))
      .subscribe((MaintenanceTypes) => {
        this.MaintenanceTypes = MaintenanceTypes;
        // console.log(this.UserTypes);
      });
  }
  getParts() {
    this.MaintenanceTaskService.GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  GoBackToList() {
    this.router.navigate(['/maintenancetask']);
  }
}

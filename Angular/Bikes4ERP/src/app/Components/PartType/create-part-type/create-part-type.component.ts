import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { PartTypeService } from 'src/app/Services/PartType/part-type.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { PartType } from 'src/app/Classes/PartType/part-type';

@Component({
  selector: 'app-create-part-type',
  templateUrl: './create-part-type.component.html',
  styleUrls: ['./create-part-type.component.css']
})
export class CreatePartTypeComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private PartTypeService: PartTypeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
//PartTypeForm : any;
  ID: number;
  data = false;
  message: string;

  //Initialize FormGroup ting
  PartTypeForm = new FormGroup({
    PartType1: new FormControl(''),
  });

  ngOnInit(): void {
    //For the Create object
    this.PartTypeForm = this.formBuilder.group({
      PartType1: ['', [Validators.required]],
    });
  } //ngOnInit

  onFormSubmit(PartTypeForm) {
    // debugger;
    const parttype = PartTypeForm.value;
    this.CreatePartType(parttype);
  } //OnFormSubmit

  CreatePartType(parttype: PartType) {
    //inject the services
    // debugger;
    this.PartTypeService.addPartType(parttype).subscribe(() => {
      this.data = true;

      this.message = 'PartType has been added bro! good job...';
      this.PartTypeForm.reset;
      console.log(this.PartTypeForm.value + 'we adding');

      this.router.navigate(['/parttype']);
    }); //addPartType
  } //CreatePartType

  GoToList() {
    this.router.navigate(['/parttype']);
  } //GoToList
} //OnInit
﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BicycleCodeChanged : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bicycle", "BicyleCode", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bicycle", "BicyleCode", c => c.Int(nullable: false));
        }
    }
}

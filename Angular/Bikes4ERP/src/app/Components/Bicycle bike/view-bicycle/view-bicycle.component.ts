import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { take } from 'rxjs/operators';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { BicycleStatus } from 'src/app/Classes/BicycleStatus/bicycle-status';

@Component({
  selector: 'app-view-bicycle',
  templateUrl: './view-bicycle.component.html',
  styleUrls: ['./view-bicycle.component.css'],
})
export class ViewBicycleComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public bicycleService: BicycleService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  BicycleForm: any;
  BicycleList: Bicycle[];
  data = false;
  BicycleID: number;
  message: string;
  BicycleBrandList: BicycleBrand[];
  elementType: 'url' | 'canvas' | 'img' = 'url';
  value: string;
  href: string;
  BicycleStatus : BicycleStatus[];
  oldData: any;

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.BicycleID = id;
    });

    this.GetBicycles();
    this.GetBicycleBrands();
    this.GetBicycleStatus();

    this.BicycleForm = new FormGroup({
      BicyleCode: new FormControl(),
      BicycleBrandName: new FormControl(),
      BicycleID: new FormControl(),
      BicycleBrandID: new FormControl(),
      BicycleStatusID : new FormControl(),
      DateAdded : new FormControl()
    });

    this.bicycleService.GetBicycles().subscribe((data) =>
      data.forEach((element) => {
        if (element.BicycleID == this.BicycleID) {
          this.BicycleForm = this.formBuilder.group({
            BicycleID: [element.BicycleID, [Validators.required]],
            BicyleCode: [element.BicyleCode, [Validators.required]],
            BicycleBrandID: [element.BicycleBrandID, [Validators.required]],
            BicycleBrandName: [element.BicycleBrandName, [Validators.required]],
            BicycleStatusID: [element.BicycleStatusID, [Validators.required]],
            BicycleStatus: [element.BicycleStatus, [Validators.required]],
            DateAdded: [element.DateAdded, [Validators.required]]
          }); // FormBuilder
          this.oldData = element;
        }
      })
    );
    this.value = this.BicycleID.toString();
  }

  onFormSubmit(BicycleForm) {
    const bicycle = BicycleForm.value;
    this.UpdateBicycle(this.BicycleID, bicycle);
  }

  UpdateBicycle(BicycleID: any, bicycle: Bicycle) {
    this.bicycleService.UpdateBicycle(BicycleID, bicycle, this.oldData).subscribe(() => {
      this.data = true;
      // this.message = "School has been updated";
      this.BicycleForm.reset;
      this.router.navigate(['/bicycle']);
    });
  }
  DeleteBicycle(BicycleID: any) {
    BicycleID = this.BicycleID;
    this.bicycleService.DeleteBicycle(BicycleID, this.oldData).subscribe(() => {
      this.data = true;
      this.router.navigate(['/bicycle']);
    });
  }

  GetBicycles() {
    this.bicycleService.GetBicycles().subscribe((data) => {
      this.BicycleList = data;
    });
  }

  GetBicycleStatus() {
    this.bicycleService.GetBicycleStatus().subscribe((data) => {
      this.BicycleStatus = data;
    });
  }


  GetBicycleBrands() {
    this.bicycleService.GetBicycleBrands().subscribe((data) => {
      this.BicycleBrandList = data;
    });
  }

  downloadQRcode() {
    this.href = document.getElementsByTagName('img')[3].src;
    var href = document.getElementsByTagName('img');
    console.log(href);
  }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    [System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]

    public class AuditTrailsController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/AuditTrails/GetAuditTrail")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public List<dynamic> GetAuditTrail()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getGetAuditTrailReturnList(db.AuditTrail.Include(xx => xx.User).ToList());

        }

        private List<dynamic> getGetAuditTrailReturnList(List<AuditTrail> forClient)
        {
            List<dynamic> dynamicAudits = new List<dynamic>();
            foreach (AuditTrail auditTrail in forClient)
            {
                dynamic dynamicauditTrail = new ExpandoObject();
                dynamicauditTrail.AuditTrailID = auditTrail.AuditTrailID;
                dynamicauditTrail.Username = db.User.Where(xx => xx.UserID == auditTrail.UserID).Select(xx => xx.Username).FirstOrDefault();
                dynamicauditTrail.TransactionType = auditTrail.TransactionType;
                dynamicauditTrail.DateTime = auditTrail.DateTime.Value.ToString("yyyy-MM-dd HH:mm");
                dynamicauditTrail.TransactionDescription = auditTrail.TransactionDescription;


                dynamicAudits.Add(dynamicauditTrail);
            }

            return dynamicAudits;
        }

        // POST: api/AuditTrails
        [ResponseType(typeof(AuditTrail))]
        public async Task<IHttpActionResult> PostAuditTrail(AuditTrail auditTrail)
        {

            auditTrail.User = db.User.Where(x => x.UserID == auditTrail.UserID).FirstOrDefault();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AuditTrail.Add(auditTrail);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = auditTrail.AuditTrailID }, auditTrail);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AuditTrailExists(int id)
        {
            return db.AuditTrail.Count(e => e.AuditTrailID == id) > 0;
        }
    }
}
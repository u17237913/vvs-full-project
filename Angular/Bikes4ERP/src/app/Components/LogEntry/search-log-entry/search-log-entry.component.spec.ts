import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchLogEntryComponent } from './search-log-entry.component';

describe('SearchLogEntryComponent', () => {
  let component: SearchLogEntryComponent;
  let fixture: ComponentFixture<SearchLogEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchLogEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLogEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

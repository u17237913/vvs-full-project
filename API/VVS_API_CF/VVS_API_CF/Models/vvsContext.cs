﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.Models
{
    public class vvsContext : DbContext
    {

        public vvsContext() : base("DefaultConnection")
        { }

        public static vvsContext Create()
        {
            return new vvsContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<AdminSchool> AdminSchool { get; set; }
        public virtual DbSet<Antenna> Antenna { get; set; }
        public virtual DbSet<AntennaStatu> AntennaStatus { get; set; }
        public virtual DbSet<Attendance> Attendance { get; set; }
        public virtual DbSet<AttendanceNotification> AttendanceNotification { get; set; }
        public virtual DbSet<AuditTrail> AuditTrail { get; set; }
        public virtual DbSet<Bicycle> Bicycle { get; set; }
        public virtual DbSet<BicycleBrand> BicycleBrand { get; set; }
        public virtual DbSet<BicyclePart> BicyclePart { get; set; }
        public virtual DbSet<BicycleSection> BicycleSection { get; set; }
        public virtual DbSet<BicycleStatu> BicycleStatus { get; set; }
        public virtual DbSet<BicycleStudent> BicycleStudent { get; set; }
        public virtual DbSet<BusinessRule> BusinessRule { get; set; }
        public virtual DbSet<CheckIn> CheckIn { get; set; }
        public virtual DbSet<CheckInStatu> CheckInStatus { get; set; }
        public virtual DbSet<City> City { get; set; }
        public virtual DbSet<Document> Document { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        //public virtual DbSet<FaultTask> FaultTask { get; set; }
        public virtual DbSet<FaultType> FaultType { get; set; }
        public virtual DbSet<Guardian> Guardian { get; set; }
        public virtual DbSet<JobFault> JobFault { get; set; }
        public virtual DbSet<JobFaultStatu> JobFaultStatus { get; set; }
        //public virtual DbSet<JobFaultTask> JobFaultTask { get; set; }
        public virtual DbSet<JobType> JobType { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<MaintenanceJob> MaintenanceJob { get; set; }
        public virtual DbSet<MaintenanceJobStatu> MaintenanceJobStatus { get; set; }
        public virtual DbSet<MaintenanceList> MaintenanceList { get; set; }
        public virtual DbSet<MaintenanceListStatu> MaintenanceListStatus { get; set; }
        public virtual DbSet<MaintenanceNotification> MaintenanceNotification { get; set; }
        public virtual DbSet<MaintenanceTask> MaintenanceTask { get; set; }
        public virtual DbSet<MaintenanceType> MaintenanceType { get; set; }
        public virtual DbSet<MarkType> MarkType { get; set; }
        public virtual DbSet<Mechanic> Mechanic { get; set; }
        public virtual DbSet<MechanicSchool> MechanicSchool { get; set; }
        public virtual DbSet<Part> Part { get; set; }
        public virtual DbSet<PartFault> PartFault { get; set; }
        public virtual DbSet<PartType> PartType { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<RepairJob> RepairJob { get; set; }
        public virtual DbSet<RepairJobStatu> RepairJobStatus { get; set; }
        public virtual DbSet<Route> Route { get; set; }
        public virtual DbSet<RouteAntenna> RouteAntenna { get; set; }
        public virtual DbSet<School> School { get; set; }
        public virtual DbSet<Section> Section { get; set; }
        //public virtual DbSet<SectionPart> SectionPart { get; set; }
        public virtual DbSet<Student> Student { get; set; }
        public virtual DbSet<StudentAttendance> StudentAttendance { get; set; }
        public virtual DbSet<StudentMark> StudentMark { get; set; }
        public virtual DbSet<Suburb> Suburb { get; set; }
        public virtual DbSet<Tag> Tag { get; set; }
        public virtual DbSet<TagStatu> TagStatus { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserType> UserType { get; set; }
        public virtual DbSet<Village> Village { get; set; }
        public virtual DbSet<WorkDay> WorkDay { get; set; }
    }
}
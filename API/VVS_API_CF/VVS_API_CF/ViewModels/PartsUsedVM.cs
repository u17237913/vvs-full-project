﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class PartsUsedVM
    {
        public PartsUsedVM() { }

        public string JobType;
        public int BicycleCode;
        public string StudentName;
        public int JobCode;
        public /*DateTime*/ string DateCompleted;
        public string TimeCompleted;
    }
}
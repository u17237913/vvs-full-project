import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/Services/user.model';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css'],
})
export class PasswordResetComponent implements OnInit {
  constructor(
    public route: ActivatedRoute,
    public router: Router,
    public PasswordService: MyServiceService
  ) {}
  UserForm: any;
  Email: any;
  User: User;
  Password: any;

  ngOnInit(): void {
    this.UserForm = new FormGroup({
      Password: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
      ]),
      ConfirmPassword: new FormControl(),
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      let email = params.get('EMAIL');
      this.Email = email;
      console.log(email);
    });
  }
  onFormSubmit(UserForm) {
    const user = UserForm.value;
    console.log(user.Password);
    if(user.Password != user.ConfirmPassword){
      alert("Please make sure the passwords match")
    }
    else{
      alert("Password changed successfully")
      this.UpdatePassword(this.Email, user);
    }
  
  } //onFormSubmit

  UpdatePassword(email: string, user: User) {
    // user.UserTypeID = this.UserTypeID;
    this.PasswordService.ChangePassword(email, user).subscribe(() => {
      this.UserForm.reset;
    });
    this.router.navigate(['/login']);
  }

  return() {
    this.router.navigate(['/login']);
  }
}

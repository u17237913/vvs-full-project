import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Part } from 'src/app/Classes/Part/part';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-bicycle-part',
  templateUrl: './bicycle-part.component.html',
  styleUrls: ['./bicycle-part.component.css'],
})
export class BicyclePartComponent implements OnInit {
  PartArray: Part[] = [];
  data = false;
  mySubscription: any;

  //search stuff
  public TempArray: Part[] = [];
  public SearchArray: Part[] = [];
  isArrayStored: boolean = false;

  constructor(private router: Router, private BicyclePartServiceService: BicyclePartServiceService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    this.BicyclePartServiceService.getParts().subscribe((data) =>
      data.forEach((element) => {
        var part = new Part();
        part.PartID = element.PartID;
        part.PartName = element.PartName;
        part.PartDescription = element.PartDescription;
        //part.SectionName = element.SectionName;
        part.Section = element.Section;
        part.PartType = element.PartType;
        //part.PartImage = element.PartImage;

        this.PartArray.push(part);
      })
    );
  } //ngOnInit

  EditPart(ids: number) {
    this.router.navigate(['/viewbicyclepart', { id: ids }],{ skipLocationChange: true });
  } //EditPart

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.PartArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.PartArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.PartArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.PartArray.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.PartName.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.PartArray = this.SearchArray;
    } else {
      this.PartArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
} //OnInit
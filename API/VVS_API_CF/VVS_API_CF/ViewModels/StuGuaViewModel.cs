﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.ViewModels
{
    public class StuGuaViewModel
    {

        public Guardian Guardians { get; set; }
        public Student Students { get; set; }

    }
}
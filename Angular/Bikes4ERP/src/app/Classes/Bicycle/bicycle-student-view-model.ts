export class BicycleStudentViewModel {
  constructor() {}

  public BicycleCode: number;
  public StudentName: string;
  public StudentSurname: string;
  public BicycleStatus: string;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMaintenanceTaskComponent } from './create-maintenance-task.component';

describe('CreateMaintenanceTaskComponent', () => {
  let component: CreateMaintenanceTaskComponent;
  let fixture: ComponentFixture<CreateMaintenanceTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMaintenanceTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMaintenanceTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purgedEverythingToDoWithSectionPartANDchangedPartIDtoBeNULLABLEinJobFaultMODEL : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.JobFault", "PartID", "dbo.Part");
            DropIndex("dbo.JobFault", new[] { "PartID" });
            AlterColumn("dbo.JobFault", "PartID", c => c.Int());
            CreateIndex("dbo.JobFault", "PartID");
            AddForeignKey("dbo.JobFault", "PartID", "dbo.Part", "PartID");
            DropColumn("dbo.JobFault", "SectionPartID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.JobFault", "SectionPartID", c => c.Int());
            DropForeignKey("dbo.JobFault", "PartID", "dbo.Part");
            DropIndex("dbo.JobFault", new[] { "PartID" });
            AlterColumn("dbo.JobFault", "PartID", c => c.Int(nullable: false));
            CreateIndex("dbo.JobFault", "PartID");
            AddForeignKey("dbo.JobFault", "PartID", "dbo.Part", "PartID", cascadeDelete: true);
        }
    }
}

﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedBicycleCodeToBicycle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bicycle", "BicyleCode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bicycle", "BicyleCode");
        }
    }
}

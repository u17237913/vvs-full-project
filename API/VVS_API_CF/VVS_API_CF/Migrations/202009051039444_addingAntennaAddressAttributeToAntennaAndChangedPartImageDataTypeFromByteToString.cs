﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingAntennaAddressAttributeToAntennaAndChangedPartImageDataTypeFromByteToString : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Antenna", "AntennaAddress", c => c.String());
            AlterColumn("dbo.Part", "PartImage", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Part", "PartImage", c => c.Binary());
            DropColumn("dbo.Antenna", "AntennaAddress");
        }
    }
}

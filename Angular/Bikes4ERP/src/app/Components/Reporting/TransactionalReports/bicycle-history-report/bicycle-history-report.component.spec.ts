import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicycleHistoryReportComponent } from './bicycle-history-report.component';

describe('BicycleHistoryReportComponent', () => {
  let component: BicycleHistoryReportComponent;
  let fixture: ComponentFixture<BicycleHistoryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicycleHistoryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicycleHistoryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

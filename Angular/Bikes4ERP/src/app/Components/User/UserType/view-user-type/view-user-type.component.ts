import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { UserType } from 'src/app/Classes/UserType/user-type';

@Component({
  selector: 'app-view-user-type',
  templateUrl: './view-user-type.component.html',
  styleUrls: ['./view-user-type.component.css'],
})
export class ViewUserTypeComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public UserTypeService: MyServiceService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  UserTypeForm: any;
  UserTypeList: UserType[];
  ID: Number;
  data = false;
  message: string;
  oldData: any;

  ngOnInit(): void {
    this.UserTypeForm = new FormGroup({
      UserTypeID: new FormControl(),
      UserType1: new FormControl(),
      UserPermissions: new FormControl(),
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.UserTypeService.GetUserTypes().subscribe((data) =>
      data.forEach((element) => {
        console.log(element.UserType);

        if (element.ID == this.ID) {
          this.UserTypeForm = this.formBuilder.group({
            UserTypeID: [element.ID, Validators.required],
            UserType1: [element.UserType, [Validators.required]],
            UserPermissions: [element.UserPermissions, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );
  }
  onFormSubmit(UserTypeForm) {
    console.log();
    const userType = UserTypeForm.value;
    this.UpdateUserType(this.ID, userType);
  } //onFormSubmit

  UpdateUserType(ID: any, userType: UserType) {
    this.UserTypeService.UpdateUserType(ID, userType, this.oldData).subscribe(() => {
      this.data = true;
      this.message = 'User has been successfully updated boiii !';
      this.UserTypeForm.reset;
      // this.router.navigate(['/usertype']);
    });
  } //Update User

  DeleteUserType(UserTypeID: any) {
    UserTypeID = this.ID;
    this.UserTypeService.DeleteUserType(UserTypeID, this.oldData).subscribe(() => {
      this.data = true;
      // this.router.navigate(['/usertype']);
    });
  }

  UserType() {
    this.router.navigate(['/usertype']);
  }
}

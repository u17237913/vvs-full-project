﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.Controllers;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Web;
using System.Net.Mail;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UsersController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/Users
        [System.Web.Http.Route("api/Users/GetUsers")]
        [System.Web.Mvc.HttpGet]
        public IHttpActionResult GetUsers()
        //public Task ActionResult(List<dynamic>) GetUsers()
        {
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value;

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            if (UserTypeID == "1")
            {
                return Ok(getUsersReturnList(db.User.Include(s => s.UserType).ToList()));
            }
            else
            {
                return Unauthorized();
            }

            //return db.Users;


        }
        private List<dynamic> getUsersReturnList(List<User> forClient)
        {
            List<dynamic> dynamicUsers = new List<dynamic>();
            foreach (User user in forClient)
            {
                dynamic dynamicUser = new ExpandoObject();
                dynamicUser.UserID = user.UserID;
                dynamicUser.UserTypeID = user.UserTypeID;
                dynamicUser.Username = user.Username;
                dynamicUser.Password = user.Password;
                dynamicUser.Firstname = user.Firstname;
                dynamicUser.Surname = user.Surname;
                dynamicUser.Email = user.Email;
                dynamicUser.Telephone = user.Telephone;
                dynamicUser.UserType = user.UserType.UserType1;

                dynamicUsers.Add(dynamicUser);
            }
            return dynamicUsers;
        }

        //// GET: api/Users
        //public IQueryable<User> GetUser()
        //{
        //    return db.User;
        //}

        // GET: api/Users/5
        [System.Web.Http.Route("api/Users/GetUser")]
        [System.Web.Mvc.HttpGet]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(int id)
        {
            User user = await db.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        //[System.Web.Http.Route("api/Users/PutUser")]
        //[System.Web.Mvc.HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserID)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [System.Web.Http.Route("api/Users/PostUser/{SchoolID:int?},{WorkDayID:int?}")]
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(int? SchoolID, int? WorkDayID, User user)
        {

            

            
            //if (!ModelState.IsValid)
            //{
            //    return BadRequest(ModelState);
            //}

            //db.User.Add(user);
            //await db.SaveChangesAsync();

            //return CreatedAtRoute("DefaultApi", new { id = user.UserID }, user);

            User usr = new Models.User();
            db.Configuration.ProxyCreationEnabled = false;

            var hash = GenerateHash(user.Password);
            //usr.Role = user.Role;
            //usr.Password = hash;
            //usr.Username = user.Username;
            usr.UserID = user.UserID;
            usr.UserTypeID = user.UserTypeID;
            usr.Username = user.Username;
            usr.Password = hash;
            usr.Firstname = user.Firstname;
            usr.Surname = user.Surname;
            usr.Email = user.Email;
            usr.Telephone = user.Telephone;
            Guid g = Guid.NewGuid();
            //usr.SessionID = g.ToString();

            db.User.Add(usr);
            await db.SaveChangesAsync();

            // UserTypeID == 3 should be School Admin
            if (user.UserTypeID == 3)
            {
                var LastUser = await db.User.OrderByDescending(u => u.UserID).FirstOrDefaultAsync();

                var admin = new Admin();
                admin.UserID = LastUser.UserID;

                db.Admin.Add(admin);
                await db.SaveChangesAsync();

                var LastAdmin = await db.Admin.OrderByDescending(a => a.AdminID).FirstOrDefaultAsync();

                var adminSchool = new AdminSchool();
                adminSchool.SchoolID = SchoolID;
                adminSchool.UserID = LastUser.UserID;
                adminSchool.AdminID = LastAdmin.AdminID;
                adminSchool.Position = "Just a School Admin, nothing more, nothing less";

                db.AdminSchool.Add(adminSchool);
                await db.SaveChangesAsync();

            }

            // UserTypeID == 2 should be Mechanic
            if (user.UserTypeID == 2)
            {
                var LastUser = await db.User.OrderByDescending(u => u.UserID).FirstOrDefaultAsync();

                var mechanic = new Mechanic();
                mechanic.UserID = LastUser.UserID;

                db.Mechanic.Add(mechanic);
                await db.SaveChangesAsync();

                var LastAdmin = await db.Mechanic.OrderByDescending(a => a.MechanicID).FirstOrDefaultAsync();

                var adminSchool = new MechanicSchool();
                adminSchool.SchoolID = SchoolID;
                adminSchool.UserID = LastUser.UserID;
                adminSchool.MechanicID = LastAdmin.MechanicID;
                adminSchool.WorkDayID = WorkDayID;
                //adminSchool. = "Just a School Admin, nothing more, nothing less";

                db.MechanicSchool.Add(adminSchool);
                await db.SaveChangesAsync();

            }

            return Ok(usr);

        }

        // DELETE: api/Users/5
        [HttpDelete]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            User user = await db.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.User.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.User.Count(e => e.UserID == id) > 0;
        }
        private bool UserEmailExists(string email)
        {
            return db.User.Count(e => e.Email == email) > 0;
        }

        //Login

        [System.Web.Http.Route("api/Users/Login")]
        [AllowAnonymous]

        public object Login(User usr)
        {
            //GenerateHash(
            var hash = GenerateHash(usr.Password);
            User user = db.User.Where(zz => zz.Username == usr.Username && zz.Password == hash).FirstOrDefault();

            dynamic toReturn = new ExpandoObject();
            if (user != null)
            {
                Guid g = Guid.NewGuid();
                //user.SessionID = g.ToString();
                db.Entry(user).State = EntityState.Modified;

                db.SaveChanges();
                toReturn.Correct = "Correct";
                toReturn.Token = GetToken(user);
                toReturn.RefreshToken = GetRefreshToken(user);

                return toReturn;
            }
            else
            {
                toReturn.Incorrect = "Incorrect";
                return toReturn;
            }

        }

        [System.Web.Http.Route("api/Users/RefreshToken")]
        [AllowAnonymous]
        [System.Web.Mvc.HttpPost]
        public object RefreshToken()
        {
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            int UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);
            var user = db.User.Include(u => u.UserType).Where(u => u.UserID == UserID).FirstOrDefault();
            dynamic toReturn = new ExpandoObject();

            //if the UserID is not null then get user from the database and generate a new token
            if (user != null)
            {

                toReturn.token = GetToken(user);
                return Ok(toReturn);

            }
            else
            {
                return Unauthorized();
            }


            //return db.Users;

        }

        //[HttpGet("[action]/{email}")]
        [System.Web.Http.Route("api/Users/ForgotPassword")]
        [AllowAnonymous]
        public async Task<string> ForgotPassword(string email)
        {

            var user = await db.User.Include(u => u.UserType).Where(u => u.Email.ToLower() == email.ToLower()).FirstOrDefaultAsync();
            string toReturn = "";
            if (user != null)
            {

                var emails = new List<string>
                {
                    user.Email
                };
                //string subject = "Shanah From Project Manager - Confirm Password Reset";
                //await _emailSender.SendEmailAsync(emails, subject, "", user, "ForgotPassword");

                using (var message = new MailMessage("vvinf370@gmail.com", user.Email))
                {
                    message.Subject = "Test";
                    message.Body = GetForgotPasswordBody(user);
                    message.IsBodyHtml = true;
                    using (SmtpClient client = new SmtpClient
                    {
                        EnableSsl = true,
                        Host = "smtp.gmail.com",
                        Port = 587,
                        Credentials = new NetworkCredential("vvinf370@gmail.com", "Virtu@l370")

                    })
                    {
                        client.Send(message);
                    }
                }// using

                return toReturn = "Found";


            }
            else
            {
                return toReturn = "NotFound";
            }
        }

        public string GetForgotPasswordBody(User user)
        {
            string Domain = "http://localhost:4200/";

            //string url = Domain + "api/User/ConfirmEmail?id=" + user.UserID;
            string token = GetToken(user);
            string url = Domain + "passwordreset;EMAIL=" + user.Email;

            return string.Format(@"<div style='text-align:center;'>
                                    <h1>Hi {1}, We heard through the grapevine that you have forgotten your password,</h1>
                                    <h1> we had to send you a email first to confirm that it's really you. </h1>
                                    <h3>Click on the button below to change your password, it will take you back to our website</h3>
                                      <a href='{0}' style=' display: block;
                                                                    text-align: center;
                                                                    font-weight: bold;
                                                                    background-color: #008CBA;
                                                                    font-size: 16px;
                                                                    border-radius: 10px;
                                                                    color:#ffffff;
                                                                    cursor:pointer;
                                                                    width:100%;
                                                                    padding:10px;'>
                                        Change Password
                                      </a>
                                    </form>
                                </div>", url, user.Firstname);
        }

        //[HttpGet("[action]/{email}")]
        //[AllowAnonymous]
        //public async Task<ActionResult> ForgotPassword(string email)
        //{

        //    var user = await _context.Users.Include(u => u.UserRole).Where(u => u.UserEmail.ToLower() == email.ToLower()).FirstOrDefaultAsync();

        //    if (user != null)
        //    {
        //        var emails = new List<string>
        //        {
        //            user.UserEmail
        //        };
        //        string subject = "Shanah From Project Manager - Confirm Password Reset";
        //        await _emailSender.SendEmailAsync(emails, subject, "", user, "ForgotPassword");
        //    }
        //    else
        //    {
        //        return NotFound();
        //    }

        //    return Ok();

        //}

        //[System.Web.Http.Route("api/Users/ChangePassword")]
        //[AllowAnonymous]
        public async Task<IHttpActionResult> ChangePassword(string email, User user)
        {

            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserEmail from the claims List
            var UserEmail = claims.Where(p => p.Type == "Email").FirstOrDefault()?.Value;


            User usr = db.User.Where(z => z.Email == email).FirstOrDefault();

            var hash = GenerateHash(user.Password);

            usr.UserID = usr.UserID;
            usr.UserTypeID = usr.UserTypeID;
            usr.Username = usr.Username;
            usr.Password = hash;
            usr.Firstname = usr.Firstname;
            usr.Surname = usr.Surname;
            usr.Email = usr.Email;
            usr.Telephone = usr.Telephone;
            Guid g = Guid.NewGuid();



            db.Entry(usr).State = EntityState.Modified;
            await db.SaveChangesAsync();


            //if (UserEmail == "1")
            //{
            //    return Ok(getUsersReturnList(db.User.Include(s => s.UserType).ToList()));
            //}
            //else
            //{
            //    return Unauthorized();
            //}

            //var currentUser = HttpContext.User;
            ////var email = currentUser.Claims.FirstOrDefault(c => c.Type == "email").Value;
            //var email = currentUser.Claims.FirstOrDefault(c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress").Value;
            ////var exp = currentUser.Claims.FirstOrDefault(c => c.Type == "exp").Value;
            ////var CurrentDateTime = FromUnixTime(Convert.ToInt32(exp));
            //var user = await _context.Users.Where(u => u.UserEmail.ToLower() == email.ToLower()).FirstOrDefaultAsync();



            return Ok();

        }

        public string GetToken(User user)
        {

            db.Configuration.ProxyCreationEnabled = false;

            string key = "HansenTheAsianThePasswordThatCanNeverBeCrackedkilyhanybfjrqjkls"; //Secret key which will be used later during validation    
            var issuer = "http://mysite.com";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            //permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("UserID", user.UserID.ToString()));
            permClaims.Add(new Claim("UserTypeID", user.UserTypeID.ToString()));
            permClaims.Add(new Claim("UserType", user.UserType.UserType1.ToString()));
            //permClaims.Add(new Claim("name", "bilal"));

            int TokenExpiry = Convert.ToInt32(db.BusinessRule.Find(5).Value);

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddMinutes(TokenExpiry),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt_token;
        }

        public string GetRefreshToken(User user)
        {
            string key = "HansenTheAsianThePasswordThatCanNeverBeCrackedkilyhanybfjrqjkls"; //Secret key which will be used later during validation    
            var issuer = "http://mysite.com";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            //permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("UserID", user.UserID.ToString()));
            //permClaims.Add(new Claim("name", "bilal"));
            int TokenExpiry = Convert.ToInt32(db.BusinessRule.Find(6).Value);
            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddYears(TokenExpiry),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt_token;
        }

        public static string GenerateHash(string GenInput)
        {
            //string hash = GenInput;
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(GenInput);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        // GET: api/UserEmail
        [System.Web.Http.Route("api/Users/GetUserEmail")]
        [System.Web.Mvc.HttpGet]

        public List<dynamic> GetUserEmail(string Email)
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getUserEmailList(db.User.Where(x => x.Email == Email).ToList());



        }
        private List<dynamic> getUserEmailList(List<User> forClient)
        {
            List<dynamic> dynamicUsers = new List<dynamic>();
            foreach (User user in forClient)
            {
                dynamic dynamicUser = new ExpandoObject();
                dynamicUser.UserID = user.UserID;
                dynamicUser.UserTypeID = user.UserTypeID;
                dynamicUser.Username = user.Username;
                dynamicUser.Password = user.Password;
                dynamicUser.Firstname = user.Firstname;
                dynamicUser.Surname = user.Surname;
                dynamicUser.Email = user.Email;
                dynamicUser.Telephone = user.Telephone;


                dynamicUsers.Add(dynamicUser);
            }
            return dynamicUsers;
        }

        //update user by email
        [System.Web.Http.Route("api/Users/PutUserPassword")]
        [System.Web.Mvc.HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserPassword(string email, User user)
        {
            var newpassword = GenerateHash(user.Password);
            user.Password = newpassword;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (email != user.Email)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserEmailExists(email))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //postmechanicschool things
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostMechanicSchool(MechanicSchool mechanicSchool)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var mechanic = new Mechanic();
            var mechanicID = 0;

            mechanic = db.Mechanic.Where(x => x.UserID == mechanicSchool.UserID).FirstOrDefault();
            mechanicID = mechanic.MechanicID;

            mechanicSchool.MechanicID = mechanicID;



            db.MechanicSchool.Add(mechanicSchool);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { ID = mechanicSchool.MechanicSchoolID }, mechanicSchool);
        }

        // GET: api/Get Work days
        [System.Web.Http.Route("api/Users/GetWorkDays")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetWorkDays()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getWorkDaysList(db.WorkDay.ToList());
        }
        private List<dynamic> getWorkDaysList(List<WorkDay> forClient)
        {
            List<dynamic> dynamicUserTypes = new List<dynamic>();
            foreach (WorkDay userType in forClient)
            {
                dynamic dynamicType = new ExpandoObject();
                dynamicType.WorkDayID = userType.WorkDayID;
                dynamicType.WorkDay1 = userType.WorkDay1;
               

                dynamicUserTypes.Add(dynamicType);
            }
            return dynamicUserTypes;
        }

        [System.Web.Http.Route("api/Users/MechanicDays")]
        [System.Web.Http.AllowAnonymous]
        public object MechanicDays(MechanicSchool Mechanic)
        {
            int maxworkdays = 0;
            int UserID = 0;
            int workdaycount = 0;

            BusinessRule myRule = db.BusinessRule.Where(x => x.BusinessRuleID == 4).FirstOrDefault();

            maxworkdays = Convert.ToInt32(myRule.Value);

            UserID = Convert.ToInt32(Mechanic.UserID);

            List<dynamic> myList = new List<dynamic>();

            foreach (MechanicSchool mechanicSchool in db.MechanicSchool)
            {
                if(mechanicSchool.UserID == UserID)
                {
                    workdaycount++;
                }
                
            }

            dynamic toReturn = new ExpandoObject();

            if(workdaycount >= maxworkdays)
            {
                toReturn.TooManyDays = "TooManyDays";
                return toReturn;
            }

            else
            {
                toReturn.Days = "Days";
                return toReturn;
            }
          

        }

        



    }
    }
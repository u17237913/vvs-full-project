import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceTaskComponent } from './maintenance-task.component';

describe('MaintenanceTaskComponent', () => {
  let component: MaintenanceTaskComponent;
  let fixture: ComponentFixture<MaintenanceTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenanceTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { BusinessRule } from 'src/app/Classes/BusinessRule/business-rule';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessRuleService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/BusinessRules/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//Business Rule ---------------------------------------//

    //GET ---> Business Rule ---------------------------------------------------------------------
    getBusinessRules():Observable<BusinessRule[]> {
      //this.audit.addAuditTrail(4, "BusinessRule", null, null).subscribe(() => console.error);
      return this.http.get<BusinessRule[]>(this.url + 'GetAllBusinessRules/'); 
    }

    //ADD ---> Business Rule ---------------------------------------------------------------------
    addBusinessRule(newBusinessRule : BusinessRule){
      this.audit.addAuditTrail(1, "BusinessRule", newBusinessRule, null).subscribe(() => console.error);
      return this.http.post(this.url + 'AddBusinessRule/' , newBusinessRule); 
    }

    //UPDATE ---> Business Rule ---------------------------------------------------------------------
    UpdateBusinessRule(updatedBusinessRule : BusinessRule, olddata: any){
      this.audit.addAuditTrail(2, "BusinessRule", updatedBusinessRule, olddata).subscribe(() => console.error);
      return this.http.post(this.url + 'UpdateBusinessRule/' , updatedBusinessRule); 
  }

    //DELETE ---> Business Rule ---------------------------------------------------------------------
    DeleteBusinessRule(id : number, olddata: any){ 
      this.audit.addAuditTrail(3, "BusinessRule", null, olddata).subscribe(() => console.error);
      return this.http.delete(this.url + 'DeleteBusinessRule/?id=' + id, this.option); 
  }

}
export class User {
  constructor() {}

  UserID: number;
  UserTypeID: number;
  UserType: string;
  Username: string;
  Password: string;
  Firstname: string;
  Surname: string;
  Email: string;
  Telephone: string;
  ConfirmPassword: string;
}

﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class faultimageChangedtoStringFromByteArray : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PartFault", "FaultImage", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PartFault", "FaultImage", c => c.Binary());
        }
    }
}

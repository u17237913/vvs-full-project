import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Document } from 'src/app/Classes/Document/document';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/Documents/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//Document ---------------------------------------//

    //GET ---> Document ---------------------------------------------------------------------
    getDocuments():Observable<Document[]> {
      //this.audit.addAuditTrail(4, "Document", null, null).subscribe(() => console.error);
      return this.http.get<Document[]>(this.url + 'GetAllDocuments/'); 
    }

    //ADD ---> Document ---------------------------------------------------------------------
    addDocument(newDocument : Document){
      this.audit.addAuditTrail(1, "Document", newDocument, null).subscribe(() => console.error);
      return this.http.post(this.url + 'AddDocument/' , newDocument); 
    }

    //UPDATE ---> Document ---------------------------------------------------------------------
    UpdateDocument(updatedDocument : Document, olddata: any){
      this.audit.addAuditTrail(2, "Document", updatedDocument, olddata).subscribe(() => console.error);
      return this.http.post(this.url + 'UpdateDocument/' , updatedDocument); 
  }

    //DELETE ---> Document ---------------------------------------------------------------------
    DeleteDocument(id : number, olddata: any){ 
      this.audit.addAuditTrail(3, "Document", null, olddata).subscribe(() => console.error);
      return this.http.delete(this.url + 'DeleteDocument/?id=' + id, this.option); 
  }

}
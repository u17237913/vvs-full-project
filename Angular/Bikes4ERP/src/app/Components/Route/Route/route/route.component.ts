import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { RouteService } from 'src/app/Services/Route/route.service';
import { Route } from 'src/app/Classes/Route/route';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css']
})
export class RouteComponent implements OnInit {

  constructor(public route : Router, private routeService : RouteService) { }

  RouteList : Route[] = [];
  public values = [];
  public TempArray: Route[] = [];
  public SearchArray: Route[] = [];
  isArrayStored: boolean = false;

  ngOnInit(): void {
this.GetRoutes();
  }
 
  GetRoutes (){
    this.routeService.GetRoutes().subscribe((data) => {
      this.RouteList = data as Route[];
      console.log(data);
    })
  }

  CreateRoute(){
    this.route.navigate(['/createroute'])
  }

  ViewRoute (ids : number){
    this.route.navigate(['viewroute', { id : ids }],{ skipLocationChange: true })
    console.log(ids)
  }

 SearchRoutes(event: any) {
  if (this.isArrayStored == false) {
    this.TempArray = this.RouteList.slice();
    this.isArrayStored = true;
  }

  if (event.target.value != '') {
    this.SearchArray = [];
    this.RouteList = this.TempArray;
    //this.TempArray = [];
    //var destinationArray = Array.from(sourceArray);

    const filterValue = event.target.value;
    this.SearchArray = this.RouteList.filter(x => Object.keys(x).some(k => x[k] != null && 
      x[k].toString().toLowerCase()
      .includes(filterValue.toLowerCase())));

/*
    this.RouteList.forEach((school) => {
      var SearchString: string = event.target.value;
      if (
        school.SchoolName.toLowerCase().startsWith(
          SearchString.toLowerCase()
        )
      ) {
        this.SearchArray.push(school);
      }
    }); // For each
*/

    this.RouteList = this.SearchArray;
  } else {
    this.RouteList = this.TempArray.slice();
    this.SearchArray = [];
  }
}
}
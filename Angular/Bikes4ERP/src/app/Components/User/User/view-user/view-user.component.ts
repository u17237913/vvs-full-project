import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
} from '@angular/forms';
import { UserType } from 'src/app/Classes/UserType/user-type';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Route } from 'src/app/Classes/Route/route';
import { User } from 'src/app/Services/user.model';
import { element } from 'protractor';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css'],
})
export class ViewUserComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public UserService: MyServiceService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  UserForm: any;
  UserList: User[];
  ID: Number;
  data = false;
  message: string;
  UserTypes: UserType[] = [];
  UserTypeID: any;
  oldData: any;

  // UserType: any;
  // UserTypeArr: [1, 2, 3];
  // AdminID: 1;
  // SchoolAdminID: 2;
  // MechanicID: 3;

  ngOnInit(): void {
    this.UserForm = new FormGroup({
      Firstname: new FormControl('', Validators.required),
      Surname: new FormControl('', Validators.required),
      Email: new FormControl('', [
        Validators.required,
        Validators.pattern('[^ @]*@[^ @]*'),
      ]),
      Username: new FormControl(),
      Telephone: new FormControl(),
      UserID: new FormControl(),
      UserTypeID: new FormControl(),
      UserType: new FormControl(),
    });
    this.GetUserTypes();

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.UserService.getUsers().subscribe((data) =>
      data.forEach((element) => {
        if (element.UserID == this.ID) {
          this.UserForm = this.formBuilder.group({
            UserID: [element.UserID, Validators.required],
            //UserType: [element.UserType, [Validators.required]],
            UserTypeID: [element.UserTypeID, [Validators.required]],
            Firstname: [element.Firstname, [Validators.required]],
            Surname: [element.Surname, [Validators.required]],
            Email: [element.Email, [Validators.required]],
            Username: [element.Username, [Validators.required]],
            Telephone: [element.Telephone, [Validators.required]],
            Password: [element.Password, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );
  }

  // this.UserService.UserForm = {

  //     UserTypeID: this.UserTypeID2,
  //     Username: '',
  //     Firstname: '',
  //     Surname: '',
  //     Email: '',
  //     Telephone: ''
  //   }

  onFormSubmit(UserForm) {
    console.log();
    const user = UserForm.value;
    this.UpdateUser(this.ID, user);
  } //onFormSubmit

  UpdateUser(ID: any, user: User) {
    // user.UserTypeID = this.UserTypeID;
    this.UserService.UpdateUser(ID, user, this.oldData).subscribe(() => {
      this.data = true;
      this.message = 'User has been successfully updated boiii !';
      this.UserForm.reset;
    });
  } //Update User

  // UpdateUserID() {

  // }
  // DropChange(UserTypeID: any) {
  //   this.UserTypeID = UserTypeID;
  //   console.log("hello")
  // }

  DeleteUser(UserID: any) {
    UserID = this.ID;
    this.UserService.DeleteUser(UserID, this.oldData).subscribe(() => {
      this.data = true;
    });
  }

  GetUserTypes() {
    this.UserService.GetUserTypes()
      .pipe(take(1))
      .subscribe((UserTypes) => {
        this.UserTypes = UserTypes;
        // console.log(this.UserTypes);
      });
  }
  Homepage() {
    this.router.navigate(['/homepage']);
  }
  User() {
    this.router.navigate(['/user']);
  }
}

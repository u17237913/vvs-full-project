export class JobFault {
  //The Constructor
  constructor() {}

  //Data Fields
  JobFaultID: number;
  JobFaultStatusID: number;
  SectionID: number;
  //SectionPartID: number;
  PartFaultID: number;
  RepairJobID: number;
  Notes: string;
  JobFaultStatus: string;
  Section: string;
  Part: string;
  PartID: number;
  ID: number;
} //JobFault

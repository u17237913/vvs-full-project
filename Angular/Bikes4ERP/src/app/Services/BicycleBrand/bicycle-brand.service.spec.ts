import { TestBed } from '@angular/core/testing';

import { BicycleBrandService } from './bicycle-brand.service';

describe('BicycleBrandService', () => {
  let service: BicycleBrandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BicycleBrandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

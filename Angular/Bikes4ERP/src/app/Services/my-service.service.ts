import { Injectable } from '@angular/core';
import { User } from './user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';
import { UserType } from '../Classes/UserType/user-type';
import { MaintenanceTask } from '../Classes/MaintenanceTask/maintenance-task';
import { MaintenanceType } from '../Classes/MaintenanceType/maintenance-type';
import { Part } from '../Classes/Part/part';
import { MaintenanceJob } from '../Classes/MaintenanceJob/maintenance-job';
import { MaintenanceList } from '../Classes/MaintenanceList/maintenance-list';
import { RepairJob } from '../Classes/RepairJob/repair-job';
import { JobFault } from '../Classes/JobFault/job-fault';
import { Identifiers } from '@angular/compiler';
import { PartFault } from '../Classes/PartFault/part-fault';
import { FaultType } from '../Classes/FaultType/fault-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';
import { MechanicSchool } from '../Classes/MechanicSchool/mechanic-school';
import { WorkDay } from '../Classes/WorkDay/work-day';
import { School } from '../Classes/School/school';
import { Mechanic } from '../Classes/Mechanic/mechanic';
import { LogoutTimer } from '../Classes/Miscellaneous/logout-timer';
import { env } from 'process';

@Injectable({
  providedIn: 'root',
})
export class MyServiceService {
  UserForm: any;
  UserTypeForm: any;
  MaintenanceTaskForm: any;
  RepairJobForm: any;
  JobFaultForm: any;
  PartFaultForm: any;
  FaultTypeForm: any;

  LoggedIn: boolean = false;

  header: any;
  option: any;

  constructor(private http: HttpClient, public audit: AuditTrailService) {
    const headerSettings: { [name: string]: string | string[] } = {};
    //this.header = new HttpHeaders(headerSettings);
    this.header = {
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    };

    let headers = new HttpHeaders({
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    });
    this.option = { headers: headers };
  }

  // getUserList(): Observable<User[]> {
  //   return this.http.get<User[]>('https://localhost:44376/api/Users/GetUsers');
  // }

  getUserList() {
    const headers = {
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    };
    //this.audit.addAuditTrail(4, 'User', null, null).subscribe(() => console.error);
    return this.http.get(environment.apiURL + '/Users/GetUsers', { headers });
  }

  getUsers(): Observable<User[]> {
    const headers = {
      Authorization: 'bearer ' + localStorage.getItem('UserToken'),
    };
    //this.audit.addAuditTrail(4, 'User', null, null).subscribe(() => console.error);
    return this.http.get<User[]>(environment.apiURL + '/Users/GetUsers', {
      headers,
    });
  }

  AddUser(SchoolID: number, WorkDayID: number, newUser: User) {
    this.audit
      .addAuditTrail(1, 'User', newUser, null)
      .subscribe(() => console.error);

    return this.http.post(
      environment.apiURL + '/Users/PostUser/' + SchoolID + ',' + WorkDayID,
      newUser
    );
  }

  UpdateUser(id: any, user: User, olddata: any) {
    this.audit
      .addAuditTrail(2, 'User', user, olddata)
      .subscribe(() => console.error);
    return this.http.put(environment.apiURL + '/Users/' + id, user);
  }

  DeleteUser(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'User', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(environment.apiURL + '/Users/DeleteUser?ID=' + id);
  }

  //user types

  GetUserTypes() {
    //this.audit.addAuditTrail(4, 'UserType', null, null).subscribe(() => console.error);
    return this.http.get<UserType[]>(
      environment.apiURL + '/UserTypes/GetUserTypes'
    );
  }

  GetUserType(id: number) {
    //this.audit.addAuditTrail(4, 'MarkType', null, null).subscribe(() => console.error);
    return this.http.get<UserType>(
      environment.apiURL + '/UserTypes/GetUserType?id=' + id
    );
  }

  AddNewUserType(newUserType: UserType) {
    this.audit
      .addAuditTrail(1, 'User', newUserType, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/UserTypes/PostUserType',
      newUserType
    );
  }

  UpdateUserType(id: any, userType: UserType, olddata: any) {
    this.audit
      .addAuditTrail(2, 'UserType', userType, olddata)
      .subscribe(() => console.error);
    return this.http.put(environment.apiURL + '/UserTypes/' + id, userType);
  }

  DeleteUserType(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'UserType', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/UserTypes/DeleteUserTypes?ID=' + id
    );
  }

  //Maintenance Task

  GetMaintenanceTask() {
    //this.audit.addAuditTrail(4, 'MaintenanceTask', null, null).subscribe(() => console.error);
    return this.http.get<MaintenanceTask[]>(
      environment.apiURL + '/MaintenanceTasks/GetMaintenanceTasks'
    );
  }

  AddMaintenanceTask(MaintenanceTask: MaintenanceTask) {
    this.audit
      .addAuditTrail(1, 'MaintenanceTask', MaintenanceTask, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/MaintenanceTasks/PostMaintenanceTask',
      MaintenanceTask
    );
  }

  UpdateMaintenanceTask(
    id: any,
    MaintenanceTask: MaintenanceTask,
    olddata: any
  ) {
    this.audit
      .addAuditTrail(2, 'MaintenanceTask', MaintenanceTask, olddata)
      .subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/MaintenanceTasks/' + id,
      MaintenanceTask
    );
  }

  DeleteMaintenanceTask(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'MaintenanceTask', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/MaintenanceTasks/DeleteMaintenanceTask?ID=' + id
    );
  }

  GetMaintenanceTypes() {
    //this.audit.addAuditTrail(4, 'MaintenanceType', null, null).subscribe(() => console.error);
    return this.http.get<MaintenanceType[]>(
      environment.apiURL + '/MaintenanceTasks/GetMaintenanceTypes'
    );
  }

  GetParts() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<Part[]>(
      environment.apiURL + '/MaintenanceTasks/GetParts'
    );
  }

  GetFaultTypes() {
    return this.http.get<FaultType[]>(
      environment.apiURL + '/PartFaults/GetFaults'
    );
  }

  //Maintenance Job Stuff

  GetMaintenanceJobs(id: string, userid: any) {
    //this.audit.addAuditTrail(4, 'MaintenanceJob', null, null).subscribe(() => console.error);
    return this.http.get<MaintenanceJob[]>(
      environment.apiURL + '/MaintenanceJobs/GetMaintenanceLists?id=' + id + '&userID=' + userid
    );
  }

  GetMaintenanceLists(id: number) {
    //this.audit.addAuditTrail(4, 'MaintenanceList', null, null).subscribe(() => console.error);
    return this.http.get<MaintenanceList[]>(
      environment.apiURL + '/MaintenanceLists/GetMaintenanceLists?id=' + id
    );
  }

  UpdateMaintenanceLists(id: number, list: MaintenanceList) {
    this.audit
      .addAuditTrail(2, 'MaintenanceList', list, id)
      .subscribe(() => console.error);
    return this.http.put(environment.apiURL + '/MaintenanceLists/' + id, list);
  }

  UpdateMaintenanceJobStatus(id: number) {
    this.audit
      .addAuditTrail(2, 'MaintenanceJob', null, id)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/MaintenanceLists/UpdateMaintenanceJobStatus',
      id
    );
  }

  // ScheduleMaintenance(id: number) {
  //   this.audit
  //     .addAuditTrail(1, 'Maintenance List', null, null)
  //     .subscribe(() => console.error);
  //   console.log(id);
  //   return this.http
  //     .post(
  //       environment.apiURL + '/MaintenanceLists/ScheduleMaintenance?id=' + id,
  //       id
  //     )
  //     .subscribe(() => console.error);
  // }

  //Repair Job Stuff

  GetRepairJob(id: string, userid: any) {
    //this.audit.addAuditTrail(4, 'RepairJob', null, null).subscribe(() => console.error);
    return this.http.get<RepairJob[]>(
      environment.apiURL + '/RepairJobs/GetRepairJobs?id=' + id +"&userID="+ userid
    );
  }

  //Login
  public LogInSubject = new Subject<boolean>();
  public LoginSubject = new Subject<LogoutTimer>();
  Login(user: User) {
    return this.http.post(environment.apiURL + '/Users/Login', user);
  }

  public TokenSubject = new Subject<number>();
  RefreshToken() {
    const headers = {
      Authorization: 'bearer ' + localStorage.getItem('RefreshToken'),
    };
    return this.http.post<any>(
      environment.apiURL + '/Users/RefreshToken',
      null,
      {
        headers,
      }
    );
  }

  //https://localhost:44348/api/JobFaults/GetJobFaults?id=1
  GetJobFaults(id: number) {
    //this.audit.addAuditTrail(4, 'JobFault', null, null).subscribe(() => console.error);
    return this.http.get<JobFault[]>(
      environment.apiURL + '/JobFaults/GetJobFaults?id=' + id
    );
  }

  UpdateJobFaults(id: number, jobfault: JobFault) {
    this.audit
      .addAuditTrail(2, 'JobFault', jobfault, id)
      .subscribe(() => console.error);
    return this.http.put(environment.apiURL + '/JobFaults/' + id, jobfault);
  }

  AddJobFaults(jobFaults: JobFault[]) {
  //RIP-AuditTrail
    return this.http.post(
      environment.apiURL + '/RepairJobs/AddJobFaults',
      jobFaults
    );
  }

  UpdateJobStatus(id: number) {
    this.audit
      .addAuditTrail(2, 'RepairJob', null, id)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/JobFaults/UpdateJobStatus',
      id
    );
  }

  GetUserEmail(Email: string) {
    //this.audit.addAuditTrail(4, 'User', null, null).subscribe(() => console.error);
    return this.http.get<User[]>(
      environment.apiURL + '/Users/GetUserEmail?Email=' + Email
    );
  }

  ForgotPasword(Email: string) {
    return this.http.post<User[]>(
      environment.apiURL + '/Users/ForgotPassword?email=' + Email,
      null
    );
  }

  ChangePassword(email: string, user: User) {
    // this.header = {
    //   Authorization: 'bearer ' + token,
    // };
    return this.http.post(
      environment.apiURL + '/Users/ChangePassword?email=' + email,
      user
    );
  }

  // UpdateUserEmail(id: any, user: User) {
  //   return this.http.put(environment.apiURL + '/Users/' + id, user);
  // }
  //part fault
  GetPartFault() {
    return this.http.get<PartFault[]>(
      environment.apiURL + '/PartFaults/GetPartFaults'
    );
  }

  AddPartFault(PartFault: PartFault) {
    this.audit
      .addAuditTrail(1, 'PartFault', PartFault, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/PartFaults/PostPartFault',
      PartFault
    );
  }
  UpdatePartFault(id: any, PartFault: PartFault, olddata: any) {
    this.audit
      .addAuditTrail(2, 'PartFault', PartFault, olddata)
      .subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/FaultTypes/PutFaultType?ID=' + id,
      PartFault
      
    );
   
  }

  DeletePartFault(id: number, olddata: any) {
    this.audit
      .addAuditTrail(3, 'PartFault', null, olddata)
      .subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/FaultTypes/DeleteFaultType?ID=' + id
    );
  }

  AddMechanicSchool(mechanicSchool: MechanicSchool) {
    this.audit
      .addAuditTrail(1, 'User', mechanicSchool, null)
      .subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Users/PostMechanicSchool',
      mechanicSchool
    );
  }
  GetWorkDays() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<WorkDay[]>(environment.apiURL + '/Users/GetWorkDays');
  }

  GetSchools() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<School[]>(environment.apiURL + '/Schools/GetSchools');
  }

  //this one is not used
  GetMechanicSchools() {
    //this.audit.addAuditTrail(4, 'Part', null, null).subscribe(() => console.error);
    return this.http.get<School[]>(
      environment.apiURL + '/Users/GetMechanicSchools'
    );
  }

  CheckWorkdays(mechanic: User) {
    return this.http.post(environment.apiURL + '/Users/MechanicDays', mechanic);
  }

  IsCheckedIn(jobID: number){
    return this.http.get(environment.apiURL + '/RepairJobs/IsCheckedIn?jobID='+jobID)
  }

  IsCheckedInMaintenance(jobID: number){
    return this.http.get(environment.apiURL + '/RepairJobs/IsCheckedInMaintenance?jobID='+jobID)
  }
}

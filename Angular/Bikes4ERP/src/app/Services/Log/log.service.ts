import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Log } from 'src/app/Classes/Log/log';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class LogService {
  LogForm : any; 

  constructor(private http: HttpClient, public audit : AuditTrailService) { }

  GeLogEntries(): Observable<Log[]> {
    //this.audit.addAuditTrail(4, "Log", null, null).subscribe(() => console.error);
    return this.http.get<Log[]>(environment.apiURL + '/Logs/GetLogEntries');
  }
}

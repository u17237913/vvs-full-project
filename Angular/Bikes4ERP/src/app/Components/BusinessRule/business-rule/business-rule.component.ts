import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BusinessRuleService } from 'src/app/Services/BusinessRule/business-rule.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { BusinessRule } from 'src/app/Classes/BusinessRule/business-rule';

@Component({
  selector: 'app-business-rule',
  templateUrl: './business-rule.component.html',
  styleUrls: ['./business-rule.component.css']
})
export class BusinessRuleComponent implements OnInit {
  BusinessRuleArray: BusinessRule[] = [];
  data = false;
  mySubscription: any;

  //search stuff
  public TempArray: BusinessRule[] = [];
  public SearchArray: BusinessRule[] = [];
  isArrayStored: boolean = false;

  constructor(
    private router: Router,
    private BusinessRuleService: BusinessRuleService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
// debugger
    this.BusinessRuleService.getBusinessRules().subscribe(data => data.forEach(element => { 
      var businessrule = new BusinessRule();
      businessrule.BusinessRuleID = element.BusinessRuleID;
      businessrule.Value = element.Value;
      businessrule.BusinessRule1 = element.BusinessRule1;

        this.BusinessRuleArray.push(businessrule);
      })
    );
  } //ngOnInit

  EditBusinessRule(ids: number) {
    this.router.navigate(['/viewbusinessrule', { id: ids }],{ skipLocationChange: true });
  } //EditBusinessRule

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  
  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.BusinessRuleArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.BusinessRuleArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.BusinessRuleArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.BusinessRuleArray.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.BusinessRule1.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.BusinessRuleArray = this.SearchArray;
    } else {
      this.BusinessRuleArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }

} //OnInit
import {Village} from '../Village/village'
import {Suburb} from '../Suburb/suburb'
import { Country} from '../Country/country'
import {City} from '../City/city'
import {Province} from '../Province/province'
import {School} from '../School/school'

export class LocationViewModel {

    constructor(){}
    Villages : Village[];
    Suburbs : Suburb[];
    Countries : Country[];
    Cities : City [];
    Provinces : Province [];
    Schools : School [];
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VVS_API_CF.Models;

namespace VVS_API_CF.ViewModels
{
    public class LocationViewModel
    {
        public LocationViewModel() { }

        public List<Village> Villages;
        public List<Country> Countries;
        public List<Suburb> Suburbs;
        public List<City> Cities;
        public List<Province> Provinces;
        public List<School> Schools;

    }
}
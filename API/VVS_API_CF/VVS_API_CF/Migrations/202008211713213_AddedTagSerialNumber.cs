﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedTagSerialNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tag", "TagSerialNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tag", "TagSerialNumber");
        }
    }
}

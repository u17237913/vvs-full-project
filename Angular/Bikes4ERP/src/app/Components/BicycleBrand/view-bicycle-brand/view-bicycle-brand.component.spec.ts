import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBicycleBrandComponent } from './view-bicycle-brand.component';

describe('ViewBicycleBrandComponent', () => {
  let component: ViewBicycleBrandComponent;
  let fixture: ComponentFixture<ViewBicycleBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBicycleBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBicycleBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

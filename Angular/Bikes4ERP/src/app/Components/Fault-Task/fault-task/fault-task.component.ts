import { Component, OnInit } from '@angular/core';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router } from '@angular/router';
import { MaintenanceTask } from 'src/app/Classes/MaintenanceTask/maintenance-task';

@Component({
  selector: 'app-fault-task',
  templateUrl: './fault-task.component.html',
  styleUrls: ['./fault-task.component.css'],
})
export class FaultTaskComponent implements OnInit {
  MaintenanceTaskList: PartFault[];
  public values = [];
  MaintenanceTaskForm: PartFault;

  //search stuff
  public TempArray: PartFault[] = [];
  public SearchArray: PartFault[] = [];
  isArrayStored: boolean = false;
  constructor(
    private MaintenanceTaskService: MyServiceService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.MaintenanceTaskService.GetPartFault().subscribe((data) => {
      this.MaintenanceTaskList = data as PartFault[];
    });
  }

  ViewMaintenanceTask(id: number) {
    this.router.navigate(['/viewfaulttask', { ID: id }],{ skipLocationChange: true });
  }
  CreateMaintenanceTask() {
    this.router.navigate(['/createfaulttask']);
  }

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.MaintenanceTaskList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.MaintenanceTaskList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.MaintenanceTaskList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.MaintenanceTaskList.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.PartName.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.MaintenanceTaskList = this.SearchArray;
    } else {
      this.MaintenanceTaskList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}

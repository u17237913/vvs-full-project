import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl,} from '@angular/forms';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Part } from 'src/app/Classes/Part/part';
import { Section } from 'src/app/Classes/Section/section';
import { PartType } from 'src/app/Classes/PartType/part-type';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-create-bicycle-part',
  templateUrl: './create-bicycle-part.component.html',
  styleUrls: ['./create-bicycle-part.component.css'],
})
export class CreateBicyclePartComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private bicyclePartService: BicyclePartServiceService,
    private uploadService: FileUploadServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  PartTypes: PartType[] = [];
  PartTypeID: any;
  Sections: Section[] = [];
  SectionID: any;
  ID: number;
  data = false;
  message: string;
  imageUrl: string =
    '../../../assets/images/Norco-section-sl-ult-black-all-road-bicycle.jpg';

  //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any = null;

  //variable for error messages - note
  messages: string[] = [];

  //Initialize FormGroup ting
  PartForm = new FormGroup({
    PartName: new FormControl(''),
    PartDescription: new FormControl(''), //,
    SectionID: new FormControl(),
    PartTypeID: new FormControl(),
    //PartImage : new FormControl()
  });
  ngOnInit(): void {
    this.GetPartTypes();
    this.GetSections();

    this.PartForm = this.formBuilder.group({
      PartName: ['', [Validators.required]],
      PartDescription: ['', [Validators.required]],
      SectionID: ['', [Validators.required]],
      PartTypeID: ['', [Validators.required]],
      //PartImage : ['', [Validators.required]],
    });

    // var UploadButton = <HTMLInputElement> document.getElementById("hideME");
    var UploadArea = document.getElementById('UploadArea');
    var UploadBtn = document.getElementById('UploadBtn');
    var ObjArea = document.getElementById('ObjArea');
    var CreateObj = document.getElementById('CreateObj');

    UploadArea.hidden = false;
    UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;
  } //ngOnInit

  //this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];

        //Show image preview
        var reader = new FileReader();

        reader.readAsDataURL(this.theFile);
        reader.onload = () => {
          this.imageUrl = reader.result as string;
        };
      } else {
        //Display error message <-!-!-> watchout, this allows for multiple files...
        this.messages.push(
          'File: ' + event.target.files[0].name + 'is too large to upload.'
        );
      }
    }
  } //OnFileChange

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.uploadPartImage(file).subscribe((respondation) => {
        this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);

    //Nav back to the list
    // this.router.navigate(['/bicyclepart']);
  }

  onFormSubmit(PartForm) {
    const part = PartForm.value;
    this.CreatePart(part);

    var UploadArea = document.getElementById('UploadArea');
    var UploadBtn = document.getElementById('UploadBtn');
    var CreateObj = document.getElementById('CreateObj');
    var ObjArea = document.getElementById('ObjArea');

    UploadArea.hidden = false;
    UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;
  } //OnFormSubmit

  CreatePart(part: Part) {
    //inject the services
    this.bicyclePartService.addPart(part).subscribe(() => {
      this.data = true;
      this.uploadFile();

      this.message = 'Part has been added bro! good job...';
      this.PartForm.reset;
      console.log(this.PartForm.value + 'we adding');
      // this.router.navigate(['/bicyclepart'])
    }); //addPart
  } //CreatePart

  // EXTRAS NEEDED FOR THE DROPDOWNS --------------------------
  GetPartTypes() {
    this.bicyclePartService
      .getPartTypes()
      .pipe(take(1))
      .subscribe((PartType) => {
        this.PartTypes = PartType;
      });
  }

  GetSections() {
    this.bicyclePartService
      .getSections()
      .pipe(take(1))
      .subscribe((Section) => {
        this.Sections = Section;
      });
  }
  // EXTRAS END HERE -------------------------------------

  GoToAddFaults() {
    this.router.navigate(['/createfaulttype']); //route it to fault type plis
  }

  GoToList() {
    this.router.navigate(['/bicyclepart']);
  } //GoToList

} //OnInit
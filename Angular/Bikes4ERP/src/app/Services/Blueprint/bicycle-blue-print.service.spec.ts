import { TestBed } from '@angular/core/testing';

import { BicycleBluePrintService } from './bicycle-blue-print.service';

describe('BicycleBluePrintService', () => {
  let service: BicycleBluePrintService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BicycleBluePrintService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

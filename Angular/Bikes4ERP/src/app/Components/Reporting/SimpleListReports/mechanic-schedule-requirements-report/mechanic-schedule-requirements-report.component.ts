import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ReportingServiceService } from 'src/app/Services/Reporting/reporting-service.service';
import * as XLSX from 'xlsx';

import { jsPDF } from "jspdf";
import 'jspdf-autotable';

import * as autoTable from 'jspdf-autotable'

import "jspdf/dist/polyfills.es.js";
import { User } from 'src/app/Services/user.model';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-mechanic-schedule-requirements-report',
  templateUrl: './mechanic-schedule-requirements-report.component.html',
  styleUrls: ['./mechanic-schedule-requirements-report.component.css']
})
export class MechanicScheduleRequirementsReportComponent implements OnInit {
  title = 'VVS - Mechanic Schedule Requirements Report';
  start : Date;
  end : Date;
  selectedUser : any;
  showErrorMessage : boolean = false;
  TableData : Object;
  LilTableData : Object;
  totalJobs : number;
  Users : User[] = [];
  liltotalJobs : number;
  counter: any;

  constructor(private ReportingService : ReportingServiceService) { }

  ngOnInit(): void { 
    var tableWithData = document.getElementById('table');
    var liltable2WithData = document.getElementById('table2');

    tableWithData.hidden = true;
    liltable2WithData.hidden = true;
    this.GetMechanics();
    this.counter = 0;
   }

   GetMechanics() {
    this.ReportingService.getMechanics().pipe(take(1)).subscribe((Mechanic) => {
      this.Users = Mechanic;
      console.log(this.Users);
    })
  }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'MechanicScheduleRequirementsReport.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    let element2 = document.getElementById('table2');
    const ws2: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element2);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    //Testing if there are values for the report tables or not
    if (this.totalJobs > 0) { XLSX.utils.book_append_sheet(wb, ws, 'Main Table');
      if (this.liltotalJobs > 0) { XLSX.utils.book_append_sheet(wb, ws2, 'Sub table'); }

      /* save to file */
      XLSX.writeFile(wb, this.excelfileName);
    } 
    else if (this.totalJobs == 0) { 
      alert('No Mechanic Schedule Requirements Report Data within the chosen dates.\n Please capture new dates and try again.'); 
    }
  }

  //extract HTML element stuff data
  @ViewChild('htmlData') htmlData:ElementRef;

  downloadPDF() {
  }

  GenerateReport() {
    //check if an option is selected otherwise Show error message
    if(this.start == undefined || this.end == undefined) {
      this.showErrorMessage == true;
    } else {
      this.showErrorMessage = false;

      this.ReportingService.getMechanicRequirementsReportData(this.start, this.end, this.selectedUser).subscribe((res) => {
        console.log(res);
        this.TableData = res['TableData'];
        this.totalJobs = res['TableData'].length;
      })

      this.ReportingService.getMechRepLilTable(this.start, this.end).subscribe((res) => {
        console.log(res);
        this.LilTableData = res['LilTableData'];
        this.liltotalJobs = res['LilTableData'].length;
      })
    }

    //Testing if there are values for the report tables or not
      if (this.totalJobs > 0 && this.liltotalJobs > 0) {
      var tableWithData = document.getElementById('table');
      var liltable2WithData = document.getElementById('table2');

      tableWithData.hidden = false;
      liltable2WithData.hidden = false;
      }
      if (this.totalJobs > 0 && this.liltotalJobs == 0) {
        var tableWithData = document.getElementById('table');
        var liltable2WithData = document.getElementById('table2');

        tableWithData.hidden = false;
        liltable2WithData.hidden = true;
        console.log(this.liltotalJobs, this.totalJobs)
        }
      else if (this.totalJobs == 0){ alert('No Mechanic Schedule Requirements Report Data within the chosen dates.\n Please capture new dates and try again.'); }

      if(this.counter==0) {
        setTimeout(( ) => { 
          console.log('ItS BeEn 3 SeCoNdS');
          document.getElementById('generaterTing').click(); }, 3000);
        this.counter++;
      }
  }
}
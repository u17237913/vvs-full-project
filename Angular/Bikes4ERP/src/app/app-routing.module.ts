//ESSENTIALS
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//MAJOR ADMIN PAGES
import { LoginComponent } from './Components/login/login.component';
import { HomePageComponent } from './Components/home-page/home-page.component';

//Bicycle Brand
import { BicycleBrandComponent } from './Components/BicycleBrand/bicycle-brand/bicycle-brand.component';
import { CreateBicycleBrandComponent } from './Components/BicycleBrand/create-bicycle-brand/create-bicycle-brand.component';
import { ViewBicycleBrandComponent } from './Components/BicycleBrand/view-bicycle-brand/view-bicycle-brand.component';

//Business Rules
import { BusinessRuleComponent } from './Components/BusinessRule/business-rule/business-rule.component';
import { CreateBusinessRuleComponent } from './Components/BusinessRule/create-business-rule/create-business-rule.component';
import { ViewBusinessRuleComponent } from './Components/BusinessRule/view-business-rule/view-business-rule.component';

//Document
import { DocumentComponent } from './Components/Document/document/document.component';
import { CreateDocumentComponent } from './Components/Document/create-document/create-document.component';
import { ViewDocumentComponent } from './Components/Document/view-document/view-document.component';

//Part Type
import { PartTypeComponent } from './Components/PartType/part-type/part-type.component';
import { ViewPartTypeComponent } from './Components/PartType/view-part-type/view-part-type.component';
import { CreatePartTypeComponent } from './Components/PartType/create-part-type/create-part-type.component';

import { AntennaComponent } from './Components/Antenna/antenna/antenna.component';
import { ViewAntennaComponent } from './Components/Antenna/view-antenna/view-antenna.component';
import { CreateAntennaComponent } from './Components/Antenna/create-antenna/create-antenna.component';
import { BicycleComponent } from './Components/Bicycle bike/bicycle/bicycle.component';
import { TagComponent } from './Components/Tag/tag/tag.component';
import { AssignBicycleComponent } from './Components/Bicycle bike/Assign bicycle/bicycle/assign-bicycle.component';
import { AssignTagComponent } from './Components/Tag/assign-tag/assign-tag.component';
import { CreateTagComponent } from './Components/Tag/create-tag/create-tag.component';
import { CreateBicycleComponent } from './Components/Bicycle bike/create-bicycle/create-bicycle.component';
import { ViewBicycleComponent } from './Components/Bicycle bike/view-bicycle/view-bicycle.component';
import { AssignBicycleStudentComponent } from './Components/Bicycle bike/Assign bicycle/bicycle/assign-bicycle-student/assign-bicycle-student.component';
import { AssignTagBicycleComponent } from './Components/Tag/assign-tag-bicycle/assign-tag-bicycle.component';
import { StudentComponent } from './Components/Student/student/student.component';
import { CreateStudentComponent } from './Components/Student/create-student/create-student.component';
import { ViewStudentComponent } from './Components/Student/view-student/view-student.component';
import { AuditTrailComponent } from './Components/AuditTrail/audit-trail/audit-trail.component';
import { ViewTagComponent } from './Components/Tag/view-tag/view-tag.component';
import { StudentMarkComponent } from './Components/Student marks/student mark/student-mark.component';
import { CaptureMarkComponent } from './Components/Student marks/capture-mark/capture-mark.component';
import { MarkTypeComponent } from './Components/Mark Type/mark-type/mark-type.component';
import { CreateMarkTypeComponent } from './Components/Mark Type/create-mark-type/create-mark-type.component';
import { ViewMarkTypeComponent } from './Components/Mark Type/view-mark-type/view-mark-type.component';

import { FaultTaskComponent } from './Components/Fault-Task/fault-task/fault-task.component';
import { CreateFaultTaskComponent } from './Components/Fault-Task/create-fault-task/create-fault-task.component';
import { ViewFaultTaskComponent } from './Components/Fault-Task/view-fault-task/view-fault-task.component';
import { ViewMaintenanceTaskComponent } from './Components/Maintenance-Task/view-maintenance-task/view-maintenance-task.component';
import { MaintenanceTaskComponent } from './Components/Maintenance-Task/maintenance-task/maintenance-task.component';
import { CreateMaintenanceTaskComponent } from './Components/Maintenance-Task/create-maintenance-task/create-maintenance-task.component';

//Sub 2 and 10
import { MechanicCalendarComponent } from './Components/MechanicHomepage/mechanic-calendar/mechanic-calendar.component';
import { JobListComponent } from './Components/MechanicHomepage/job-list/job-list.component';
import { FaultListComponent } from './Components/MechanicHomepage/fault-list/fault-list.component';
import { BicycleBlueprintComponent } from './Components/ReportRepairJob/bicycle-blueprint/bicycle-blueprint.component';
import { SectionBlueprintComponent } from './Components/ReportRepairJob/section-blueprint/section-blueprint.component';
import { ReportListComponent } from './Components/ReportRepairJob/report-list/report-list.component';
import { PartFaultsComponent } from './Components/ReportRepairJob/part-faults/part-faults.component';
import { QrScanComponent } from './Components/qr-scan/qr-scan.component';

//Section
import { ViewBicycleSectionComponent } from './Components/BicycleSection/view-bicycle-section/view-bicycle-section.component';
import { CreateBicycleSectionComponent } from './Components/BicycleSection/create-bicycle-section/create-bicycle-section.component';
import { BicycleSectionComponent } from './Components/BicycleSection/bicycle-section/bicycle-section.component';

//SCHOOL
import { ViewSchoolComponent } from './Components/School/view-school/view-school.component';
import { CreateSchoolComponent } from './Components/School/create-school/create-school.component';
import { SchoolComponent } from './Components/School/school/school.component';

//REPORTING
import { MechanicScheduleReportComponent } from './Components/Reporting/SimpleListReports/mechanic-schedule-report/mechanic-schedule-report.component';
import { MechanicScheduleRequirementsReportComponent } from './Components/Reporting/SimpleListReports/mechanic-schedule-requirements-report/mechanic-schedule-requirements-report.component';
import { StudentReportComponent } from './Components/Reporting/ManagementReports/student-report/student-report.component';
import { BicycleHistoryReportComponent } from './Components/Reporting/TransactionalReports/bicycle-history-report/bicycle-history-report.component';
import { LogReportComponent } from './Components/Reporting/TransactionalReports/log-report/log-report.component';
import { PartsUsedReportComponent } from './Components/Reporting/SimpleListReports/parts-used-report/parts-used-report.component';

import { ViewBicyclePartComponent } from './Components/BicyclePart/view-bicycle-part/view-bicycle-part.component';
import { CreateBicyclePartComponent } from './Components/BicyclePart/create-bicycle-part/create-bicycle-part.component';
import { BicyclePartComponent } from './Components/BicyclePart/bicycle-part/bicycle-part.component';

import { UserComponent } from './Components/User/User/user/user.component';
import { CreateUserComponent } from './Components/User/User/create-user/create-user.component';
import { ViewUserComponent } from './Components/User/User/view-user/view-user.component';
import { UserTypeComponent } from './Components/User/UserType/user-type/user-type.component';
import { ViewUserTypeComponent } from './Components/User/UserType/view-user-type/view-user-type.component';
import { CreateUserTypeComponent } from './Components/User/UserType/create-user-type/create-user-type.component';
import { ForgotPasswordComponent } from './Components/forgot-password/forgot-password.component';
import { PasswordResetComponent } from './Components/forgot-password/password-reset/password-reset.component';
import { RouteComponent } from './Components/Route/Route/route/route.component';
import { ViewRouteComponent } from './Components/Route/Route/view-route/view-route.component';
import { CreateRouteComponent } from './Components/Route/Route/create-route/create-route.component';
import { UserRoleComponent } from './Components/User/User/user-role/user-role.component';

import { MaintenanceList } from './Classes/MaintenanceList/maintenance-list';

import { MaintenanceListComponent } from './Components/MechanicHomepage/maintenance-list/maintenance-list.component';
import { AuthGuard } from './Auth/auth.guard';
import { SearchLogEntryComponent } from './Components/LogEntry/search-log-entry/search-log-entry.component';
import { FaultTypeComponent } from './Components/FaultType/fault-type/fault-type.component';
import { CreateFaultTypeComponent } from './Components/FaultType/create-fault-type/create-fault-type.component';
import { ViewFaultTypeComponent } from './Components/FaultType/view-fault-type/view-fault-type.component';
import { ViewMechanicComponent } from './Components/User/User/view-mechanic/view-mechanic.component';
import { AddSchoolComponent } from './Components/User/User/add-school/add-school.component';
import { StaticComponent } from './Components/static/static.component';

const routes: Routes = [
  //default to homepage
  { path: '', redirectTo: '/homepage', pathMatch: 'full' },
  { path: 'homepage', component: HomePageComponent},
  //end
  { path: 'login', component: LoginComponent },
  { path: 'viewbicyclepart', component: ViewBicyclePartComponent },
  { path: 'createbicyclepart', component: CreateBicyclePartComponent },
  { path: 'bicyclepart', component: BicyclePartComponent },
  { path: 'antenna', component: AntennaComponent },
  { path: 'viewantenna', component: ViewAntennaComponent },
  { path: 'createantenna', component: CreateAntennaComponent },

  //Fault Type
  {
    path: 'faulttype',
    component: FaultTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createfaulttype',
    component: CreateFaultTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewfaulttype',
    component: ViewFaultTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  //School admin stuff
  {
    path: 'bicycle',
    component: BicycleComponent,
    canActivate: [AuthGuard],
    data: { roles: ['School Administrator'] },
  },
  //end
  { path: 'tag', component: TagComponent },
  { path: 'viewtag', component: ViewTagComponent },
  { path: 'assignbicycle', component: AssignBicycleComponent },
  { path: 'assigntag', component: AssignTagComponent },
  { path: 'createtag', component: CreateTagComponent },
  { path: 'createbicycle', component: CreateBicycleComponent },
  { path: 'viewbicycle', component: ViewBicycleComponent },
  { path: 'assignbicyclestudent', component: AssignBicycleStudentComponent },
  { path: 'assigntagbicycle', component: AssignTagBicycleComponent },
  //School Admin stuff
  {
    path: 'student',
    component: StudentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['School Administrator'] },
  },
  //end
  { path: 'createstudent', component: CreateStudentComponent },
  { path: 'viewstudent', component: ViewStudentComponent },
  { path: 'audittrail', component: AuditTrailComponent },
  { path: 'studentmarks', component: StudentMarkComponent },

  //School Admin stuff

  //document
  /*{
    path: 'viewdocument',
    component: ViewDocumentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createdocument',
    component: CreateDocumentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['School Administrator', 'Administrator'] },
  },
  {
    path: 'document',
    component: DocumentComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
//    data: { roles: ['School Administrator', 'Administrator'] },
  },*/
  
  {
    path: 'capturemark',
    component: CaptureMarkComponent,
    canActivate: [AuthGuard],
    data: { roles: ['School Administrator'] },
  },
  {
    path: 'marktype',
    component: MarkTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['School Administrator'] },
  },
  //end
  { path: 'createmarktype', component: CreateMarkTypeComponent },
  { path: 'viewmarktype', component: ViewMarkTypeComponent },

  //What mechanic is authorised to see
  {
    path: 'mechaniccalendar',
    component: MechanicCalendarComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Mechanic'] },
  },
  {
    path: 'joblist',
    component: JobListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Mechanic'] },
  },
  {
    path: 'faultlist',
    component: FaultListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Mechanic'] },
  },
  {
    path: 'maintenancelist',
    component: MaintenanceListComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Mechanic'] },
  },
  //no conditions, i think?
  { path: 'bicycleblueprint', component: BicycleBlueprintComponent },
  { path: 'sectionblueprint', component: SectionBlueprintComponent },
  { path: 'reportlist', component: ReportListComponent },
  { path: 'partfaults', component: PartFaultsComponent },
  { path: 'qrscan', component: QrScanComponent },
  { path: 'document', component: DocumentComponent },
  { path: 'viewdocument', component: ViewDocumentComponent },
  { path: 'createdocument', component: CreateDocumentComponent },

  //Super Admin only
  {
    path: 'faulttask',
    component: FaultTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewmechanic',
    component: ViewMechanicComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'addmechschool',
    component: AddSchoolComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createfaulttask',
    component: CreateFaultTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewfaulttask',
    component: ViewFaultTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'maintenancetask',
    component: MaintenanceTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createmaintenancetask',
    component: CreateMaintenanceTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewmaintenancetask',
    component: ViewMaintenanceTaskComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
//businessrule
  {
    path: 'viewbusinessrule',
    component: ViewBusinessRuleComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createbusinessrule',
    component: CreateBusinessRuleComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'businessrule',
    component: BusinessRuleComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
//bicyclebrand
  {
    path: 'viewbicyclebrand',
    component: ViewBicycleBrandComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createbicyclebrand',
    component: CreateBicycleBrandComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'bicyclebrand',
    component: BicycleBrandComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
//bicyclesection
  {
    path: 'viewbicyclesection',
    component: ViewBicycleSectionComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createbicyclesection',
    component: CreateBicycleSectionComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'bicyclesection',
    component: BicycleSectionComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },

  //parttype
  {
    path: 'viewparttype',
    component: ViewPartTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createparttype',
    component: CreatePartTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'parttype',
    component: PartTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },

//school
  {
    path: 'viewschool',
    component: ViewSchoolComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createschool',
    component: CreateSchoolComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'school',
    component: SchoolComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },

  //end of admin?

  {
    path: 'mechanicschedulereport',
    component: MechanicScheduleReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'mechanicschedulerequirementsreport',
    component: MechanicScheduleRequirementsReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'studentreport',
    component: StudentReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'bicyclehistoryreport',
    component: BicycleHistoryReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'logreport',
    component: LogReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'partsusedreport',
    component: PartsUsedReportComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'user',
    component: UserComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  //Regiester User
  { path: 'createuser', component: CreateUserComponent },
  //end

  {
    path: 'viewuser',
    component: ViewUserComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'usertype',
    component: UserTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewusertype',
    component: ViewUserTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createusertype',
    component: CreateUserTypeComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  //password things
  { path: 'forgotpassword', component: ForgotPasswordComponent },
  { path: 'passwordreset', component: PasswordResetComponent },
  //end password
  {
    path: 'route',
    component: RouteComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'viewroute',
    component: ViewRouteComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'createroute',
    component: CreateRouteComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },
  {
    path: 'userrole',
    component: UserRoleComponent,
    canActivate: [AuthGuard],
    data: { roles: ['Administrator'] },
  },

  //Log entry
  { path: 'logentry', component: SearchLogEntryComponent },
  {
    path: 'static',
    component: StaticComponent,
   
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

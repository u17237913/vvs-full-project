import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Services/user.model';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
// import * as Bootstrap from '../node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  UserList: User[];
  public values = [];
  UserForm: User;

  public TempArray: User[] = [];
  public SearchArray: User[] = [];
  isArrayStored: boolean = false;

  constructor(private UserService: MyServiceService, public router: Router) {}

  ngOnInit(): void {
    this.getUsers();
    // this.UserService.getUserList.subscribe((x) =>
    //   x.forEach((y) => {
    //     var user = new User();
    //     user.Firstname = y.Firstname;
    //     user.UserTypeID = y.UserTypeID;
    //     this.UserList.push(user);
    //     console.log('test');
    //   })
    // );
    // this.UserService.getUserList().then((res) => (this.UserList = res as User[]));
    // this.UserForm = {
    //   ID: 0,
    //   UserTypeID: 0,
    //   Username: null,
    //   Password: null,
    //   Firstname: null,
    //   Surname: null,
    //   Email: null,
    //   Telephone: null,
    // };

    // this.UserService.getUserList().subscribe(data => data.forEach(element => {
    //   var user = new User();
    //   user.UserTypeID = element.UserTypeID;
    //   user.Username = element.Username;
    //   this.UserList.push(user);
    //   console.log(this.UserList, "list");
    //   console.log(data, "data")
    // }))

    //this.UserList = this.UserService.getValues()
  }

  getUsers() {
    console.log('hello');
    this.UserService.getUserList().subscribe((data) => {
      this.UserList = data as User[];
      console.log(this.UserList, 'list');
      console.log(data, 'data');
    });
  }

  ViewUser(id: number, usertypeID: number) {
    console.log(usertypeID);
    if (usertypeID != 2) {
      this.router.navigate(['/viewuser', { ID: id }],  { skipLocationChange: true });
    }
    //
    else {
      this.router.navigate(['/viewmechanic', { ID: id }],  { skipLocationChange: true });
    }
  }

  CreateUser() {
    this.router.navigate(['/createuser'], { skipLocationChange: true });
  }

  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.UserList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.UserList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.UserList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.UserList.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.Username.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.UserList = this.SearchArray;
    } else {
      this.UserList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}
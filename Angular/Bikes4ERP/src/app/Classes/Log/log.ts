export class Log {

    //The Constructor
    constructor(){}

    //Data Fields
    LogID : number;
    TagID : number;
    AntennaID : number;
    DateTime : string;
    AntennaSerialNumber : string;
    StudentName : string;
    TagSerialNumber : string;

}//Log
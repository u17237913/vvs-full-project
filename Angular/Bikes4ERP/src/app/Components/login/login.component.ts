import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { User } from 'src/app/Services/user.model';
import { HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Session } from 'protractor';
import { AppComponent } from '../../app.component';
import { LogoutTimer } from 'src/app/Classes/Miscellaneous/logout-timer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  header: any;
  option: any;
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public LoginService: MyServiceService
  ) {}
  user: User = new User();
  showError = false;
  errorMessage: string;
  message: any;
  data: any;
  UserForm: any;

  ngOnInit(): void {
    this.LoginService.UserForm = {
      UserTypeID: 0,
      Username: '',
      Password: '',
      Firstname: '',
      Surname: '',
      Email: '',
      Telephone: '',
    };
  }
  onFormSubmit(UserForm) {
    const user = this.LoginService.UserForm;
    console.log(this.LoginService.UserForm);
    this.LoginUser(user);
  }

  LoginUser(user: User) {
    this.LoginService.Login(user).subscribe((res: any) => {
      console.log(res);
      this.data = true;
      this.message = 'Saved';
      if (res.Correct) {
        this.LoginService.LoggedIn = true;
        //this.router.navigate(['/user'])
        localStorage.setItem('UserToken', res.Token);

        const helper = new JwtHelperService();
        const DecodedToken = helper.decodeToken(res.Token);
        localStorage.setItem('UserTypeID', DecodedToken.UserTypeID);
        localStorage.setItem('UserID', DecodedToken.UserID);
        localStorage.setItem('UserType', DecodedToken.UserType);
        localStorage.setItem('RefreshToken', res.RefreshToken);

        var logoutTimer = new LogoutTimer( DecodedToken.exp ,  true)
        this.LoginService.LoginSubject.next(logoutTimer);

        // this.LoginService.TokenSubject.next(DecodedToken.exp);
        // this.LoginService.LogInSubject.next(true);
        // var app = new AppComponent(this.LoginService, this.router);
        // app.ngOnInit();
        this.router.navigate(['/homepage']);
      } else {
        this.router.navigate(['/login']);
        alert('Incorrect Username or Password. Please try again.');
      }
    });
  }
  forgotPassword() {
    this.router.navigate(['/forgotpassword']);
  }
}

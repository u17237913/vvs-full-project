const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const ByteLength = require('@serialport/parser-byte-length')
const axios = require('axios')

const port = new SerialPort('COM3', {baudRate: 9600})
const port2 = new SerialPort('COM6', {baudRate: 9600})

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

function hex_to_ascii(str1) {
	var hex = str1.toString('hex');

	var str = '';
	var final;
	for (var n = 2; n < hex.length-8; n += 2) {
		str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
	}
	final = str;
	str = '';
	return final;
}

port.on('error', function(err) {
    console.log('Error: ', err.message)
})

const parser = port.pipe(new ByteLength({length: 16}))

parser.on('data', function(id){
	var tagtime = new Date().toLocaleString();
	var IdforTag = hex_to_ascii(id);

	axios({
		method: 'post',
		url: 'https://bikes4erpwebapi.azurewebsites.net/api/log/addLogEntry',
		// httpsAgent: new https.Agent({ rejectUnauthorized: false }),
		data: {
			TagSerial: IdforTag,
			TimeTagged: tagtime,
			AntennaID: 1
		}
	})
	.catch((error) => {
		console.error(error)
	});

})

port2.on('error', function(err) {
    console.log('Error: ', err.message)
})

const parser2 = port2.pipe(new ByteLength({length: 16}))

parser2.on('data', function(id){
	var tagtime = new Date().toLocaleString();
	var IdforTag = hex_to_ascii(id);

	axios({
		method: 'post',
		url: 'https://bikes4erpwebapi.azurewebsites.net/api/log/addLogEntry',
		// httpsAgent: new https.Agent({ rejectUnauthorized: false }),
		data: {
			TagSerial: IdforTag,
			TimeTagged: tagtime,
			AntennaID: 2
		}
	})
	.catch((error) => {
		console.error(error)
	});

})



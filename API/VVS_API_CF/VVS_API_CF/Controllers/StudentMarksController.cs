﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]


    public class StudentMarksController : ApiController
    {
        private vvsContext db = new vvsContext();


        [System.Web.Http.Route("api/StudentMarks/GetStudentMarks")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetStudentMarks()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            var SchoolID = db.AdminSchool.Where(AS => AS.UserID == UserID).FirstOrDefault().SchoolID;

            return getStudentMarkfromIDList(
                db.StudentMark
                .Include( sm => sm.Student )
                .Where( sm => sm.Student.SchoolID == SchoolID )
                .ToList()
                );
        }
        private List<dynamic> getStudentMarkfromIDList(List<StudentMark> forClient)
        {
            List<dynamic> dynamicStudentMarks = new List<dynamic>();
            var StudentMarkList = forClient.GroupBy(fc => fc.StudentID);
            var LastStudentMarkList = new List<StudentMark>();

            dynamic dynamicStudentMarkOut = new ExpandoObject();

            foreach (var student in StudentMarkList)
            {
                LastStudentMarkList.Add(student.OrderByDescending(s => s.Date).FirstOrDefault());
            }

            foreach (StudentMark studentmark in LastStudentMarkList)
            {
                dynamic dynamicStudentMark = new ExpandoObject();

                dynamicStudentMark.StudentID = studentmark.StudentID;
                dynamicStudentMark.StudentName = db.Student.Where(xx => xx.StudentID == studentmark.StudentID).Select(ss => ss.StudentName + " " + ss.StudentSurname).FirstOrDefault();
                dynamicStudentMark.MarkTypeID = studentmark.MarkTypeID;
                dynamicStudentMark.Date = studentmark.Date.Value.ToString("yyyy-MM-dd"); ;
                dynamicStudentMark.MarkType = db.StudentMark.Where(xx => xx.MarkType.MarkTypeID == studentmark.MarkTypeID).Select(cc => cc.MarkType.MarkType1).FirstOrDefault();
                dynamicStudentMark.Mark = studentmark.Mark;



                dynamicStudentMarks.Add(dynamicStudentMark);
            }

            return dynamicStudentMarks;
        }

        [System.Web.Http.Route("api/StudentMarks/GetStudentMarksForID")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetStudentMarksForID(int id)
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getStudentMarkforIDList(db.StudentMark.Where(xx => xx.StudentID == id).ToList());
        }
        private List<dynamic> getStudentMarkforIDList(List<StudentMark> forClient)
        {
            List<dynamic> dynamicStudentMarks = new List<dynamic>();




            foreach (StudentMark studentmark in forClient)
            {
                dynamic dynamicStudentMark = new ExpandoObject();


                dynamicStudentMark.MarkTypeID = studentmark.MarkTypeID;
                dynamicStudentMark.Date = studentmark.Date.Value.ToString("yyyy-MM-dd");
                dynamicStudentMark.MarkType = db.StudentMark.Where(xx => xx.MarkType.MarkTypeID == studentmark.MarkTypeID).Select(cc => cc.MarkType.MarkType1).FirstOrDefault();
                dynamicStudentMark.Mark = studentmark.Mark;
                //dynamicStudentMark.StudentName = studentmark.Student.StudentName + ' ' + studentmark.Student.StudentSurname;



                dynamicStudentMarks.Add(dynamicStudentMark);
            }

            return dynamicStudentMarks;
        }

        [System.Web.Http.Route("api/StudentMarks/GetALLStudentMarks")]
        [System.Web.Mvc.HttpGet]

        public List<StudentMarksVM> GetAllStudentMarks()
        {

            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            var SchoolID = db.AdminSchool.Where(AS => AS.UserID == UserID).FirstOrDefault().SchoolID;

            List<StudentMarksVM> MarksViewModel = new List<StudentMarksVM>();

            var StudentList = db.Student.Where( s => s.SchoolID == SchoolID ).ToList();

            var StudentMarksList = db.StudentMark
                .Include(cc => cc.MarkType)
                .Include( sm => sm.Student )
                .Where( sm => sm.Student.SchoolID == SchoolID )
                .ToList();

            for (int i=0; i <StudentList.Count(); i++)
            {
                var CurrentStudentID = StudentList[i].StudentID;

                var CurrentStudent = StudentList.Find(ss => ss.StudentID == CurrentStudentID);

                var CurrentMark = StudentMarksList.Where(ss => ss.StudentID == CurrentStudentID).FirstOrDefault();

                if (CurrentMark != null)
                {
                    StudentMarksVM vM = new StudentMarksVM();
                    vM.StudentID = (int)CurrentMark.Student.StudentID;
                    vM.StudentName = CurrentMark.Student.StudentName;
                    vM.StudentSurname = CurrentMark.Student.StudentSurname;
                    vM.Mark = CurrentMark.Mark.ToString();
                    vM.MarkType1 = CurrentMark.MarkType.MarkType1;
                    vM.MarkTypeID = CurrentMark.MarkType.MarkTypeID;
                    vM.Date = CurrentMark.Date.Value.ToString("yyyy-MM-dd");

                    MarksViewModel.Add(vM);
                }

                else
                {
                    StudentMarksVM vM = new StudentMarksVM();
                    vM.StudentID = (int)CurrentStudent.StudentID;
                    vM.StudentName = CurrentStudent.StudentName;
                    vM.StudentSurname = CurrentStudent.StudentSurname;

                    MarksViewModel.Add(vM);
                    
                }

            }
            return MarksViewModel;
        }

        [System.Web.Http.Route("api/Logs/GetLogEntries")]
        [System.Web.Mvc.HttpGet]

        public List<dynamic> GetLogEntries()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getGetLogEntriesList(db.Log.Include(xx => xx.Tag).ToList());
        }
        private List<dynamic> getGetLogEntriesList(List<Log> forClient)
        {
            List<dynamic> dynamicLogEntries = new List<dynamic>();
            foreach (Log log in forClient)
            {
                dynamic dynamicLogEntry = new ExpandoObject();

                dynamicLogEntry.LogID = log.LogID;
                dynamicLogEntry.TagID = log.TagID;
                dynamicLogEntry.TagSerialNumber = db.Tag.Where(xx => xx.TagID == log.TagID).Select(xx => xx.TagSerialNumber).FirstOrDefault();
                dynamicLogEntry.DateTime = log.DateTime.Value.ToString("yyyy-MM-dd HH:mm");
                //dynamicLogEntry.StudentName = db.Bicycle.Where(xx => xx.TagID == log.TagID).Include(xx => xx.BicycleStudents).Where(xx => xx.BicycleID == db.)
                dynamicLogEntry.StudentName = db.BicycleStudent.Include(a => a.Student).Include(b => b.Bicycle).Where(aa => aa.Bicycle.TagID == log.TagID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();



                dynamicLogEntries.Add(dynamicLogEntry);
            }

            return dynamicLogEntries;
        }

        //[System.Web.Http.Route("api/StudentMarks/GetMarkTypes")]
        //[System.Web.Mvc.HttpGet]
        //public List<dynamic> GetMarkTypes()
        //{
        //    //return db.Users;
        //    vvsContext db = new vvsContext();
        //    db.Configuration.ProxyCreationEnabled = false;
        //    return getMarkTypeList(db.MarkType.ToList());
        //}
        //private List<dynamic> getMarkTypeList(List<MarkType> forClient)
        //{
        //    List<dynamic> dynamicMarkTypes = new List<dynamic>();
        //    foreach (MarkType marktype in forClient)
        //    {
        //        dynamic dynamicMarkType = new ExpandoObject();

        //        dynamicMarkType.MarkTypeID = marktype.MarkTypeID;
        //        dynamicMarkType.MarkType = marktype.MarkType1;


        //        dynamicMarkTypes.Add(dynamicMarkType);
        //    }

        //    return dynamicMarkTypes;
        //}


        //// GET: api/StudentMarks
        //public IQueryable<StudentMark> GetStudentMark()
        //{
        //    return db.StudentMark;
        //}

        //// GET: api/StudentMarks/5
        //[ResponseType(typeof(StudentMark))]
        //public async Task<IHttpActionResult> GetStudentMark(int id)
        //{
        //    StudentMark studentMark = await db.StudentMark.FindAsync(id);
        //    if (studentMark == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(studentMark);
        //}


        [System.Web.Http.Route("api/StudentMarks/PostStudentMark")]
        [System.Web.Mvc.HttpPost]
        // POST: api/StudentMarks
        [ResponseType(typeof(StudentMark))]
        public async Task<IHttpActionResult> PostStudentMark(StudentMark studentMark)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.StudentMark.Add(studentMark);
            await db.SaveChangesAsync();

            var StudentMarks = await db.StudentMark.Where(sm => sm.StudentID == studentMark.StudentID).ToListAsync();

            return Ok(StudentMarks);
        }

        // DELETE: api/StudentMarks/5


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StudentMarkExists(int id)
        {
            return db.StudentMark.Count(e => e.StudentMarkID == id) > 0;
        }
    }
}

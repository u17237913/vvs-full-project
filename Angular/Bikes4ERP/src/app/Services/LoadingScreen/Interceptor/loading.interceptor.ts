import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { LoadingService } from '../Service/loading.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(private loadingService: LoadingService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.loadingService.requestStarted();

    if (
      localStorage.getItem('UserToken') != null ||
      localStorage.getItem('UserToken') != undefined
    ) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('UserToken')}`,
        },
      });
    }

    return this.handler(next, request);
  }
  handler(next, request) {
    return next.handle(request).pipe(
      tap(
        (event) => {
          //this.spinnerService.requestEnded();
          if (event instanceof HttpResponse) {
            //debugger;
            this.loadingService.requestEnded();
          }
        },
        (error) => {
          //debugger;
          this.loadingService.resetSpinner();
        }
      )
    );
  }
}

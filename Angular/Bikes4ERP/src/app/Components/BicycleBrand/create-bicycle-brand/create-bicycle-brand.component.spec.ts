import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBicycleBrandComponent } from './create-bicycle-brand.component';

describe('CreateBicycleBrandComponent', () => {
  let component: CreateBicycleBrandComponent;
  let fixture: ComponentFixture<CreateBicycleBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBicycleBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBicycleBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { element } from 'protractor';
import { take } from 'rxjs/operators';
import { SchoolService } from 'src/app/Services/School/school.service';
import { School } from 'src/app/Classes/School/school';
import { Village } from 'src/app/Classes/Village/village';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';

@Component({
  selector: 'app-view-school',
  templateUrl: './view-school.component.html',
  styleUrls: ['./view-school.component.css'],
})
export class ViewSchoolComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public schoolService: SchoolService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  SchoolForm: any;
  SchooList: School;
  data = false;
  SchoolID: number;
  message: string;
  Location: LocationViewModel[] = [];
  VillageID: any;
  LocationList: LocationViewModel[] = [];
  oldData: any;

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.SchoolID = id;
    });

    this.GetLocations();

    this.schoolService.GetSchools().subscribe((data) =>
      data.forEach((element) => {
        if (element.SchoolID == this.SchoolID) {
          this.SchoolForm = this.formBuilder.group({
            SchoolID: [element.SchoolID, [Validators.required]],
            SchoolName: [element.SchoolName, [Validators.required]],
            SchoolEmail: [element.SchoolEmail, [Validators.required]],
            SchoolTelephone: [element.SchoolTelephone, [Validators.required]],
            VillageID: [element.VillageID, [Validators.required]],
          }); // FormBuilder
          this.oldData = element;
        } // If
      })
    );
  }

  onFormSubmit(SchoolForm) {
    const school = SchoolForm.value;
    this.UpdateSchool(this.SchoolID, school);
  }

  UpdateSchool(SchoolID: any, school: School) {
    this.schoolService.UpdateSchool(SchoolID, school, this.oldData).subscribe(() => {
      this.data = true;
      this.message = 'School has been updated';
      this.SchoolForm.reset;
    });
  }
  DeleteSchool(SchoolID: any) {
    SchoolID = this.SchoolID;
    this.schoolService.DeleteSchool(SchoolID, this.oldData).subscribe(() => {
      this.data = true;
      // this.router.navigate(['/school']);
    });
  }
  GetLocations() {
    this.schoolService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  GoBack() {
    this.router.navigate(['/school']);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicCalendarComponent } from './mechanic-calendar.component';

describe('MechanicCalendarComponent', () => {
  let component: MechanicCalendarComponent;
  let fixture: ComponentFixture<MechanicCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

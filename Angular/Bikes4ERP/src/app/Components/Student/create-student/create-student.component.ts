import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/Services/Student/student.service';
import { Student } from 'src/app/Classes/Student/student';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { School } from 'src/app/Classes/School/school';
import { Guardian } from 'src/app/Classes/Guardian/guardian';
import { StuGuaViewModel } from 'src/app/Classes/StuGuaViewModel/stu-gua-view-model';
import * as Bootstrap from '../../../../../node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css'],
})
export class CreateStudentComponent implements OnInit {
  // LocationList : LocationViewModel [] = [];
  StudentList: Student[] = [];
  SchoolList: School[] = [];
  LocationList: LocationViewModel[] = [];
  StudentForm: any;
  data = false;
  message: string;
  public values = [];

  constructor(
    formbuilder: FormBuilder,
    private router: Router,
    public studentService: StudentService
  ) {}

  ngOnInit(): void {
    // this.studentService.AddStudent(this.studentService.StudentForm).subscribe(res => {

    // })

    this.GetSchools();
    this.GetLocations();

    this.studentService.StudentForm = {
      ID: 0,
      // School : 1,
      //Village : 1,
      StudentName: '',
      //StudentPhoneNumber : '',
      StudentDoB: '',
    };

    this.studentService.GuardianForm = {
      ID: 0,
      GuardianName: '',
      GuardianPhone: '',
      GuardianEmail: '',
    };
  } //ngOnit

  GetLocations() {
    this.studentService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  BacktoCreate() {
    this.router.navigate(['/createstudent']);
  }
  GoBack() {
    this.router.navigate(['/student']);
  }
  GetSchools() {
    this.studentService.GetSchools().subscribe((data) => {
      this.SchoolList = data;
    });
  }

  CreateStudent() {
    var guardian: Guardian = this.studentService.GuardianForm;
    var student: Student = this.studentService.StudentForm;
    var stuGuaViewModel: StuGuaViewModel = new StuGuaViewModel();
    stuGuaViewModel.Guardians = guardian;
    stuGuaViewModel.Students = student;

    this.studentService.AddStudent(stuGuaViewModel).subscribe(() => {
      this.data = true;
      var myModal = new Bootstrap.Modal(
        document.getElementById('StudentAddedModal')
      );
      myModal.show();
      this.GoBack();
      //  this.onFormSubmit.reset();
    });
  }
} //export

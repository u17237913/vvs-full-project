import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingService } from '../Service/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnDestroy {

  showSpinner = false;

  constructor(private loadingService: LoadingService, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.init();
  }

  init() {

    this.loadingService.getSpinnerObserver().subscribe((status) => {
      this.showSpinner = (status === 'start');
      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy(){}

}

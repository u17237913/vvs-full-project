﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{
    [System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoutesController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/Routes/GetRoutes")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetRoutes()
        {
            //return db.Users;
            vvsContext db = new vvsContext();
            db.Configuration.ProxyCreationEnabled = false;
            return getRoutesReturnList(db.Route.Include(rr => rr.School).ToList());
        }
        private List<dynamic> getRoutesReturnList(List<Route> forClient)
        {
            List<dynamic> dynamicRoutes = new List<dynamic>();
            foreach (Route route in forClient)
            {
                dynamic dynamicRoute = new ExpandoObject();

                dynamicRoute.RouteID = route.RouteID;
                dynamicRoute.SchoolName = db.School.Where(xx => xx.SchoolID == route.SchoolID).Select(xx => xx.SchoolName).FirstOrDefault();
                dynamicRoute.Postion1 = route.Position1;
                dynamicRoute.Postion2 = route.Position2;
                //dynamicRoute.Postion1 = db.Antenna.Where(zz => zz.AntennaID == route.Position1).Select(zz => zz.AntennaID).FirstOrDefault();
                
   

                dynamicRoutes.Add(dynamicRoute);
            }

            return dynamicRoutes;
        }

        // GET: api/Routes
        public IQueryable<Route> GetRoute()
        {
            return db.Route;
        }

        // GET: api/Routes/5
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route(Name = "GetRoute")]
        [ResponseType(typeof(Route))]
        public async Task<IHttpActionResult> GetRoute(int id)
        {
            Route route = await db.Route.FindAsync(id);
            if (route == null)
            {
                return NotFound();
            }

            return Ok(route);
        }


        // PUT: api/Routes/5
        [System.Web.Http.Route("api/Routes/PutRoute")]
        [System.Web.Mvc.HttpPut]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRoute(int id, Route route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != route.RouteID)
            {
                return BadRequest();
            }

            db.Entry(route).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RouteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Routes
        [System.Web.Http.Route("api/Routes/PostRoute")]
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(Route))]
        public async Task<IHttpActionResult> PostRoute(Route route)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Route.Add(route);
            await db.SaveChangesAsync();

            return CreatedAtRoute("GetRoute", new { id = route.RouteID }, route);
        }

        // DELETE: api/Routes/5
        [System.Web.Http.Route("api/Routes/DeleteRoute")]
        [System.Web.Mvc.HttpDelete]
        [ResponseType(typeof(Route))]
        public async Task<IHttpActionResult> DeleteRoute(int id)
        {
            Route route = await db.Route.FindAsync(id);
            if (route == null)
            {
                return NotFound();
            }

            db.Route.Remove(route);
            await db.SaveChangesAsync();

            return Ok(route);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RouteExists(int id)
        {
            return db.Route.Count(e => e.RouteID == id) > 0;
        }
    }
}
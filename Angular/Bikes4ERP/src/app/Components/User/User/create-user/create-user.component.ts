import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  NgForm,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { User } from 'src/app/Services/user.model';
import { UserType } from 'src/app/Classes/UserType/user-type';

import { take } from 'rxjs/operators';
import { SchoolService } from 'src/app/Services/School/school.service';
import { School } from 'src/app/Classes/School/school';
import { WorkDay } from 'src/app/Classes/WorkDay/work-day';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
})
export class CreateUserComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public UserService: MyServiceService,
    public schoolService: SchoolService
  ) {}

  userForm: any;
  data = false;
  message: string;
  ID: any;
  UserTypes: UserType[] = [];
  ShowSchoolList = false;
  ShowWorkList = false;
  WorkDayID: number;
  WorkDayList: WorkDay[] = [];
  SchoolList: School[] = [];
  SchoolID: number;
  passmatch: boolean = false;

  // ConfirmPassword: string;
  ngOnInit(): void {
    this.WorkDayID = 0;
    this.SchoolID = 0;
    this.UserService.UserForm = {
      UserTypeID: 0,
      Username: '',
      Password: '',
      Firstname: '',
      Surname: '',
      Email: '',
      Telephone: '',
    };
    this.GetUserTypes();
  }
  SaveUser() {
    console.log(this.UserService.UserForm);

    if(this.UserService.UserForm.Password != this.UserService.UserForm.ConfirmPassword){
      alert("Please make sure the passwords match")
      this.passmatch = false;
    }
    else{
      this.passmatch = true;
    this.UserService.AddUser(
      this.SchoolID,
      this.WorkDayID,
      this.UserService.UserForm
    ).subscribe((res) => {
      console.log(res);
      // this.router.navigate(['/homepage']);
    });

  }
  }

  UserTypeSelection(event: any) {
    var UserTypeID = event.target.value;
    if (UserTypeID == '3') {
      console.log('School admin');
      this.ShowSchoolList = true;
      this.ShowWorkList = false;
      this.schoolService.GetSchools().subscribe((schools) => {
        this.SchoolList = schools;
      });
    } else if (UserTypeID == '2') {
      this.ShowWorkList = true;
      this.ShowSchoolList = true;
      this.schoolService.GetWorkDays().subscribe((workdays) => {
        this.WorkDayList = workdays;
      });
      this.schoolService.GetSchools().subscribe((schools) => {
        this.SchoolList = schools;
      });
    } else {
      this.ShowWorkList = false;
      this.ShowSchoolList = false;
    }
  }

  GoBackToList() {
    this.router.navigate(['/user']);
  }

  GetUserTypes() {
    this.UserService.GetUserTypes()
      .pipe(take(1))
      .subscribe((UserTypes) => {
        this.UserTypes = UserTypes;
        // console.log(this.UserTypes);
      });
  }

  NavigateHome() {
    this.router.navigate(['/user']);
  }
}

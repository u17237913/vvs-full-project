﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MaintenanceJobsController : ApiController
    {
        
        
        private vvsContext db = new vvsContext();

        // GET: api/MaintenanceJobs
        // GET: api/MaintenanceLists ie: the stuff inside MaintenanceJobs
        [System.Web.Http.Route("api/MaintenanceJobs/GetMaintenanceLists")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetMaintenanceLists(string id, int userID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int year = Convert.ToInt32(id.Substring(0, 4));
            int month = Convert.ToInt32(id.Substring(5, 2));
            int day = Convert.ToInt32(id.Substring(8, 2));

            DateTime dateTime = new DateTime(year, month, day);
            return getMaintenanceJobList(db.MaintenanceJob.Include(s => s.MaintenanceJobStatu)
                .Include(y => y.CheckIn.MechanicSchool)
                .Include(z => z.Bicycle)
                .Where(y => y.CheckIn.MechanicSchool.UserID == userID)
                .Where(x => x.DateScheduled == dateTime).ToList());
            
            
        }
        private List<dynamic> getMaintenanceJobList(List<MaintenanceJob> forClient)
        {

            List<dynamic> dynamicMTs = new List<dynamic>();
            foreach (MaintenanceJob mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.MaintenanceJobID = mt.MaintenanceJobID;
                dynamicMT.BicycleID = mt.BicycleID;
                dynamicMT.BicycleCode = mt.Bicycle.BicyleCode;
                dynamicMT.MaintenanceJobStatusID = mt.MaintenanceJobStatusID;
                dynamicMT.MaintenanceJobStatus = mt.MaintenanceJobStatu.MaintenanceJobStatus;
                dynamicMT.CheckInID = mt.CheckInID;
                dynamicMT.DateScheduled = mt.DateScheduled.ToString().Substring(0,10);
                dynamicMT.Assigned = mt.Assigned;


                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }
        //npm install -fixerGenie

        // GET: api/MaintenanceJobs/5
        [ResponseType(typeof(MaintenanceJob))]
        public async Task<IHttpActionResult> GetMaintenanceJob(int id)
        {
            MaintenanceJob maintenanceJob = await db.MaintenanceJob.FindAsync(id);
            if (maintenanceJob == null)
            {
                return NotFound();
            }

            return Ok(maintenanceJob);
        }

        // PUT: api/MaintenanceJobs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMaintenanceJob(int id, MaintenanceJob maintenanceJob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != maintenanceJob.MaintenanceJobID)
            {
                return BadRequest();
            }

            db.Entry(maintenanceJob).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaintenanceJobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MaintenanceJobs
        [ResponseType(typeof(MaintenanceJob))]
        public async Task<IHttpActionResult> PostMaintenanceJob(MaintenanceJob maintenanceJob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MaintenanceJob.Add(maintenanceJob);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = maintenanceJob.MaintenanceJobID }, maintenanceJob);
        }

        // DELETE: api/MaintenanceJobs/5
        [ResponseType(typeof(MaintenanceJob))]
        public async Task<IHttpActionResult> DeleteMaintenanceJob(int id)
        {
            MaintenanceJob maintenanceJob = await db.MaintenanceJob.FindAsync(id);
            if (maintenanceJob == null)
            {
                return NotFound();
            }

            db.MaintenanceJob.Remove(maintenanceJob);
            await db.SaveChangesAsync();

            return Ok(maintenanceJob);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MaintenanceJobExists(int id)
        {
            return db.MaintenanceJob.Count(e => e.MaintenanceJobID == id) > 0;
        }
    }
}
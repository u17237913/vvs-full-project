﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.Services;

namespace VVS_API_CF.Controllers
{
    public class BusinessRulesController : ApiController
    {
        private vvsContext db = new vvsContext();

        //----------------------------------------ALL Business Rules API END-POINTS BELOW----------------------------------------------

        //GET ---> Business Rules
        [System.Web.Http.Route("api/BusinessRules/GetAllBusinessRules")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllBusinessRules()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getBusinessRulesReturnList(db.BusinessRule.ToList());
        }

        private List<dynamic> getBusinessRulesReturnList(List<BusinessRule> forMark)
        {
            List<dynamic> dynamicBusinessRules = new List<dynamic>();
            foreach (BusinessRule businessrule in forMark)
            {
                dynamic dynamicBusinessRule = new ExpandoObject();

                dynamicBusinessRule.BusinessRuleID = businessrule.BusinessRuleID;
                dynamicBusinessRule.Value = businessrule.Value;
                dynamicBusinessRule.BusinessRule1 = businessrule.BusinessRule1;

                dynamicBusinessRules.Add(dynamicBusinessRule);
            }
            return dynamicBusinessRules;
        }

        //ADD ---> Business Rule
        [System.Web.Http.Route("api/BusinessRules/AddBusinessRule")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddBusinessRule([FromBody] BusinessRule businessrule)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.BusinessRule.Add(businessrule);
            db.SaveChanges();

            return getBusinessRulesReturnList(db.BusinessRule.ToList());
        }

        //UPDATE ---> Business Rule
        //Come back and generate a new token if the RefreshToken or Token Values are chanaged in the business rules!
        [System.Web.Http.Route("api/BusinessRules/UpdateBusinessRule")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateBusinessRule([FromBody] BusinessRule _businessrule)
        {

            // Gte the identity
            var identity = User.Identity as ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = Convert.ToInt32(claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value);

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            using (vvsContext vvsSys = new vvsContext())
            {
                BusinessRule updatedBusinessRule = (from mt in vvsSys.BusinessRule
                                            where mt.BusinessRuleID == _businessrule.BusinessRuleID
                                            select mt)
                                            .FirstOrDefault();

                updatedBusinessRule.BusinessRuleID = _businessrule.BusinessRuleID;
                updatedBusinessRule.Value = _businessrule.Value;
                updatedBusinessRule.BusinessRule1 = _businessrule.BusinessRule1;

                vvsSys.SaveChanges();
            }

            var StudentNotAtSchoolString = db.BusinessRule.Find(1).Value;
            var SNASTH = Convert.ToInt32(StudentNotAtSchoolString.Substring(0, 2));
            var SNASTM = Convert.ToInt32(StudentNotAtSchoolString.Substring(3, 2));

            var BikeForTwoYearsString = db.BusinessRule.Find(7).Value;
            var BFTYH = Convert.ToInt32(BikeForTwoYearsString.Substring(0, 2));
            var BFTYM = Convert.ToInt32(BikeForTwoYearsString.Substring(3, 2));

            EmailJobs email = new EmailJobs();

            RecurringJob.AddOrUpdate(() => email.StudentNotAtSchool(), Cron.Daily(SNASTH, SNASTM));
            RecurringJob.AddOrUpdate(() => email.BikeForTwoYears(), Cron.Daily(BFTYH, BFTYM));

            return true;
        }

        //DELETE ---> BicyclePart
        [System.Web.Http.Route("api/BusinessRules/DeleteBusinessRule")]
        [System.Web.Mvc.HttpDelete]
        public void DeleteBusinessRule(int id)
        {
            BusinessRule BusinessRuleToDelete = (db.BusinessRule
                                        .Where(o => o.BusinessRuleID == id))
                                        .FirstOrDefault();

            db.BusinessRule.Remove(BusinessRuleToDelete);
            db.SaveChanges();
        }



        // --- SCAFFOLDED APPROACH BELOW IF NEEDED...



/*        // GET: api/BusinessRules
        public IQueryable<BusinessRule> GetBusinessRule()
        {
            return db.BusinessRule;
        }

        // GET: api/BusinessRules/5
        [ResponseType(typeof(BusinessRule))]
        public async Task<IHttpActionResult> GetBusinessRule(int id)
        {
            BusinessRule businessRule = await db.BusinessRule.FindAsync(id);
            if (businessRule == null)
            {
                return NotFound();
            }

            return Ok(businessRule);
        }

        // PUT: api/BusinessRules/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBusinessRule(int id, BusinessRule businessRule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != businessRule.BusinessRuleID)
            {
                return BadRequest();
            }

            db.Entry(businessRule).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BusinessRuleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BusinessRules
        [ResponseType(typeof(BusinessRule))]
        public async Task<IHttpActionResult> PostBusinessRule(BusinessRule businessRule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BusinessRule.Add(businessRule);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = businessRule.BusinessRuleID }, businessRule);
        }

        // DELETE: api/BusinessRules/5
        [ResponseType(typeof(BusinessRule))]
        public async Task<IHttpActionResult> DeleteBusinessRule(int id)
        {
            BusinessRule businessRule = await db.BusinessRule.FindAsync(id);
            if (businessRule == null)
            {
                return NotFound();
            }

            db.BusinessRule.Remove(businessRule);
            await db.SaveChangesAsync();

            return Ok(businessRule);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BusinessRuleExists(int id)
        {
            return db.BusinessRule.Count(e => e.BusinessRuleID == id) > 0;
        }
*/
    }
}
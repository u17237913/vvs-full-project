﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;

namespace VVS_API_CF.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TagsController : ApiController
    {
        private vvsContext db = new vvsContext();

        [System.Web.Http.Route("api/Tags/GetTags")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetTags()
        {
            //return db.Users;
            db.Configuration.ProxyCreationEnabled = false;
            return getTagsReturnList(db.Tag.ToList());
        }
        private List<dynamic> getTagsReturnList(List<Tag> forClient)
        {
            List<dynamic> dynamicTags = new List<dynamic>();
            foreach (Tag tag in forClient)
            {
                dynamic dynamicTag = new ExpandoObject();
                dynamicTag.TagID = tag.TagID;
                dynamicTag.TagStatusID = tag.TagStatusID;
                //dynamicTag.DateInstalled = tag.DateInstalled.Value.ToString("yyyy-MM-dd");
                dynamicTag.TagSerialNumber = tag.TagSerialNumber;
                dynamicTag.TagStatus = db.TagStatus.Where(xx => xx.TagStatusID == tag.TagStatusID).Select(y => y.TagStatus).FirstOrDefault();
                dynamicTag.BicycleCode = db.Bicycle.Where(xx => xx.TagID == tag.TagID).Select(yy => yy.BicyleCode).FirstOrDefault();

                dynamicTags.Add(dynamicTag);
            }
            return dynamicTags;
        }

        [System.Web.Http.Route("api/Tags/GetTagAssignmentList")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public List<TagAssignmentVM> GetTagAssignmentList()
        {

            db.Configuration.ProxyCreationEnabled = false;

            var TagAssignmentList = new List<TagAssignmentVM>();

            var BicycleList = db.Bicycle.ToList();
            var TagList = db.Tag.ToList();

            foreach (var tag in TagList)
            {
                var CurrentBicycle = BicycleList.Where(bl => bl.TagID == tag.TagID).FirstOrDefault();

                if (CurrentBicycle != null)
                {
                    var TagAssignment = new TagAssignmentVM();
                    TagAssignment.BicycleCode = (int)CurrentBicycle.BicyleCode;
                    TagAssignment.TagSerialNumber = tag.TagSerialNumber;
                    TagAssignment.TagStatus = "Assigned";
                    TagAssignmentList.Add(TagAssignment);
                }
                else
                {
                    var TagAssignment = new TagAssignmentVM();
                    //TagAssignment.BicycleCode = 0;
                    TagAssignment.TagSerialNumber = tag.TagSerialNumber;
                    TagAssignment.TagStatus = "Available";
                    TagAssignmentList.Add(TagAssignment);
                }
            }

            return TagAssignmentList;

        }

        [System.Web.Http.Route("api/Tags/GetBicyclesWithoutTags")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public IHttpActionResult GetBicyclesWithoutTags()
        {

            db.Configuration.ProxyCreationEnabled = false;

            var BicycleList = db.Bicycle.Where(b => b.TagID == null).ToList();

            return Ok(BicycleList);

        }

        [System.Web.Http.Route("api/Tags/AssignBicycleTag")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public IHttpActionResult AssignBicycleTag(string TagSerialNumber, int BicycleCode)
        {

            db.Configuration.ProxyCreationEnabled = false;

            var CurrentBicycle = db.Bicycle.Where(b => b.BicyleCode == BicycleCode).FirstOrDefault();
            var Tag = db.Tag.Where(t => t.TagSerialNumber == TagSerialNumber).FirstOrDefault();

            if (CurrentBicycle != null)

            {
                
                CurrentBicycle.TagID = Tag.TagID;
                Tag.TagStatusID = 2;
                Tag.DateInstalled = DateTime.Now;
                db.Entry(Tag).State = EntityState.Modified;
                db.Entry(CurrentBicycle).State = EntityState.Modified;
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return NotFound();
            }

        }

        [System.Web.Http.Route("api/Tags/UassignTag")]
        [System.Web.Mvc.HttpGet]
        // GET: api/Students
        public IHttpActionResult UassignTag(int BicycleCode)
        {

            db.Configuration.ProxyCreationEnabled = false;

            var CurrentBicycle = db.Bicycle.Where(b => b.BicyleCode == BicycleCode).FirstOrDefault();
            var Tag = db.Tag.Where(t => t.TagID == CurrentBicycle.TagID).FirstOrDefault();
            

            if (CurrentBicycle != null)
            {
                CurrentBicycle.TagID = null;
                db.Entry(CurrentBicycle).State = EntityState.Modified;
                db.SaveChanges();

                if (Tag != null)
                {
                    Tag.TagStatusID = 1;
                    Tag.DateInstalled = null;
                    db.Entry(Tag).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Ok();
            }
            else
            {
                return NotFound();
            }

        }

        [System.Web.Http.Route("api/Tags/GetTagStatus")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetTagStatus()
        {
            //return db.Users;
            db.Configuration.ProxyCreationEnabled = false;
            return getTagStatusReturnList(db.TagStatus.ToList());
        }
        private List<dynamic> getTagStatusReturnList(List<TagStatu> forClient)
        {
            List<dynamic> dynamicTagStatuss = new List<dynamic>();
            foreach (TagStatu tagStatu in forClient)
            {
                dynamic dynamicTagStatu = new ExpandoObject();
                dynamicTagStatu.TagStatusID = tagStatu.TagStatusID;
                dynamicTagStatu.TagStatus = tagStatu.TagStatus;


                dynamicTagStatuss.Add(dynamicTagStatu);
            }
            return dynamicTagStatuss;
        }
        // GET: api/Tags
        public IQueryable<Tag> GetTag()
        {
            return db.Tag;
        }

        // GET: api/Tags/5

        [ResponseType(typeof(Tag))]
        [System.Web.Mvc.HttpGet]
        [System.Web.Http.Route(Name = "GetTag")]
        public async Task<IHttpActionResult> GetTag(int id)
        {
            Tag tag = await db.Tag.FindAsync(id);
            if (tag == null)
            {
                return NotFound();
            }

            return Ok(tag);
        }

        // PUT: api/Tags/5 [System.Web.Http.Route("api/Tags/GetTagStatus")]
        [System.Web.Mvc.HttpPut]
        [System.Web.Http.Route("api/Tags/PutTag")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTag(int id, Tag tag)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tag.TagID)
            {
                return BadRequest();
            }

            db.Entry(tag).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TagExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tags
        [System.Web.Http.Route("api/Tags/PostTag")]
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(Tag))]
        public async Task<IHttpActionResult> PostTag(Tag tag)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var CheckTag = await db.Tag.Where(tt => tt.TagSerialNumber == tag.TagSerialNumber).FirstOrDefaultAsync();


            if (CheckTag == null)
            {
                tag.TagStatusID = 1;
                db.Tag.Add(tag);
                await db.SaveChangesAsync();

                return CreatedAtRoute("GetTag", new { id = tag.TagID }, tag);
            }
            else
            {
                return Conflict();
            }
    
        }

        // DELETE: api/Tags/5
        [ResponseType(typeof(Tag))]
        public async Task<IHttpActionResult> DeleteTag(int id)
        {
            Tag tag = await db.Tag.FindAsync(id);
            if (tag == null)
            {
                return NotFound();
            }

            db.Tag.Remove(tag);
            await db.SaveChangesAsync();

            return Ok(tag);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TagExists(int id)
        {
            return db.Tag.Count(e => e.TagID == id) > 0;
        }
    }
}
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import { Log } from 'src/app/Classes/Log/log.js';
import { LogService } from 'src/app/Services/Log/log.service.js';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-search-log-entry',
  templateUrl: './search-log-entry.component.html',
  styleUrls: ['./search-log-entry.component.css']
})
export class SearchLogEntryComponent implements OnInit {
  LogList : Log[] = [];
  public values = [];
  LogForm : Log;
  public TempArray : Log[] = [];
  public SearchArray: Log[] =[];
  isArrayStored : boolean = false;

  constructor(public route: Router, private logService: LogService) { }

  ngOnInit(): void {

    this.GetLogs()
  }

  GetLogs(){
    this.logService.GeLogEntries().subscribe((data) => {
      this.LogList = data as Log[];
    })
  }

  /*name of the excel-file which will be downloaded. */ 
  excelfileName= 'LogSheet.xlsx';  

  exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('table'); 
    const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    if (this.LogList.length > 0) {
    /* save to file */
    XLSX.writeFile(wb, this.excelfileName);
    }
    else if (this.LogList.length == 0){ alert('No Log entries to export to Excel.\n Please make a new search and try again.'); }
  }

  SearchLogs(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.LogList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.LogList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.LogList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));
      this.LogList = this.SearchArray;
    } else {
      this.LogList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
}
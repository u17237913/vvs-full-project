﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class PartUsedCalcVM
    {
        public PartUsedCalcVM() { }

        public string PartsUtilized;
        public bool UsedPart;
        public int PartsUsedCount;
    }
}
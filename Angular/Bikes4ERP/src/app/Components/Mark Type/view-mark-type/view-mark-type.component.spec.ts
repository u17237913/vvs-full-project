import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMarkTypeComponent } from './view-mark-type.component';

describe('ViewStudentComponent', () => {
  let component: ViewMarkTypeComponent;
  let fixture: ComponentFixture<ViewMarkTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMarkTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMarkTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

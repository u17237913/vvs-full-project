import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor {

  // didDelete : boolean = true;

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      retry(1),
      catchError((error: HttpErrorResponse) => {
        let errorMessage = '';

        switch (error.status.toString()){

          case "500": /*Internal Server Error*/
            errorMessage = `Error Code: ${error.status}\nMessage: Our server is currently experiencing some difficulties.\n` + `Please try again later.`;
            break;

          case "409": /*Internal Server Error*/
          errorMessage = `Error Code: ${error.status}\nMessage: The entered details already exist in the database.\n` + `Please try again with different details.`;
          break;

          case "404": /*Not Found*/
            errorMessage = `Error Code: ${error.status}\nMessage: No matching resources were found for the request.\n` + `Page Not Found.`;
            break;

          case "403": /*Forbidden*/
            errorMessage = `Error Code: ${error.status}\nMessage: The thing that you seek is not within reach.`;
            break;

          case "401": /*Unauthorized*/
            errorMessage = `Error Code: ${error.status}\nMessage: You are currently not authorized to access this resource.`;
            break;

          case "400": /*Bad Request*/
            errorMessage = `Error Code: ${error.status}\nMessage: The details of your request were incorrect.\n Please validate the details entered and try again.`;
            break;

          case "200": /*ESKOM Service Provider outages, E.g API dying or going ghost*/
            errorMessage = `Error Code: ${error.status}\nMessage: Server under maintenance \n` + `Try again later.`;
            break;

          case "0": /*Sumting Wong*/
            errorMessage = `Error Code: ${error.status}\nMessage: Server under maintenance \n` + `Try again later.`;
            break;

          default:
            if (error.error instanceof ErrorEvent) {
              // client-side error '${error.error.message}'
              errorMessage = `Error: You have done some odd stuff that our system cannot comprehend at this present juncture.`;
            } else {
              // server-side error
              errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            }
        }
        window.alert(errorMessage);
        // this.didDelete = false;
        return throwError(errorMessage);
      })
    );
  }
}
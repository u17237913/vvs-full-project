export class TagAssignmentVM {
  constructor() {}

  TagSerialNumber: string;
  BicycleCode: number;
  TagStatus: string;
}

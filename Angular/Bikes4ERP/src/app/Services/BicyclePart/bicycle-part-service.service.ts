import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { from, Observable } from 'rxjs';
import { BicyclePart } from 'src/app/Classes/BicyclePart/bicycle-part';
import { BicycleSection } from 'src/app/Classes/BicycleSection/bicycle-section';
import { Part } from 'src/app/Classes/Part/part';
import { Section } from 'src/app/Classes/Section/section';
import { PartType } from 'src/app/Classes/PartType/part-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class BicyclePartServiceService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/BicyclePart/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//BICYCLE PART ---------------------------------------//

  //GET ---> Part ---------------------------------------------------------------------
  getParts():Observable<Part[]> { 
    //this.audit.addAuditTrail(4, "Part", null, null).subscribe(() => console.error);
    return this.http.get<Part[]>(this.url + 'GetAllParts/'); 
  }

  //ADD ---> Part ---------------------------------------------------------------------
  addPart(newPart : Part){ 
    this.audit.addAuditTrail(1, "Part", newPart, null).subscribe(() => console.error);
    return this.http.post(this.url + 'AddPart/' , newPart); 
  }

  //UPDATE ---> Part ---------------------------------------------------------------------
  UpdatePart(updatedPart : Part, olddata: any){ 
    this.audit.addAuditTrail(2, "Part", updatedPart, olddata).subscribe(() => console.error);
    return this.http.post(this.url + 'UpdatePart/' , updatedPart); 
  }

  //DELETE ---> Part ---------------------------------------------------------------------
  DeletePart(id : number, olddata: any){ 
    this.audit.addAuditTrail(3, "Part", null, olddata).subscribe(() => console.error);
    return this.http.delete(this.url + 'DeletePart/?id=' + id, this.option); 
  }
    

//Additional API Endpoints needed for editing a bicycle Part.

  //GET ---> PartType ---------------------------------------------------------------------
  getPartTypes():Observable<PartType[]> { 
    //this.audit.addAuditTrail(4, "PartType", null, null).subscribe(() => console.error);
    return this.http.get<PartType[]>(this.url + 'GetAllPartTypes/'); 
  }


//BICYCLE SECTION ---------------------------------------//

  //GET ---> Section ---------------------------------------------------------------------
  getSections():Observable<Section[]> { 
    //this.audit.addAuditTrail(4, "Section", null, null).subscribe(() => console.error);
    return this.http.get<Section[]>(this.url + 'GetAllSections/'); 
  }

  //ADD ---> Section --------------------------------------------------------------------- // pane zvikuitika jahman!
  addSection(newSection : Section) { 
    this.audit.addAuditTrail(1, "Section", newSection, null).subscribe(() => console.error);
    const endpoint = this.url + 'AddSection/';
    return this.http.post(endpoint, newSection); 
  }

  //UPDATE ---> Section ---------------------------------------------------------------------
  UpdateSection(updatedSection : Section, olddata: any){ 
    this.audit.addAuditTrail(2, "Section", updatedSection, olddata).subscribe(() => console.error);
    return this.http.post(this.url + 'UpdateSection/' , updatedSection); 
  }

  // UpdateSection(formData : FormData)
  // { 
  //   const endpoint = this.url + 'UpdateSection/';

  //   return this.http.post(endpoint , formData); }

  //DELETE ---> Section ---------------------------------------------------------------------
  DeleteSection(id : number, olddata: any){ 
    this.audit.addAuditTrail(3, "Section", null, olddata).subscribe(() => console.error);
    return this.http.delete(this.url + 'DeleteSection/?id=' + id, this.option);
  }
}
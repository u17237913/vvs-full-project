﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;
using System.Globalization;
using System.Web.Http.Cors;
using System.Dynamic;
using System.Data.Entity;
using VVS_API_CF.Controllers;

namespace VVS_API_CF.Controllers
{
    public class QRscannerController : ApiController
    {

        vvsContext db = new vvsContext();

        [Route("api/QRScanner/GetQRcode")]
        [HttpGet]
        public dynamic GetQRcode(int BicycleID)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;

                var Bicycle = db.Bicycle.Find(BicycleID);

                //Allows us to find the relavant CheckInID for this Bike
                var ChekinID = db.MaintenanceJob.Include(cs => cs.CheckIn).Include(cc => cc.Bicycle).Where(ms => ms.Bicycle.BicycleID == Bicycle.BicycleID).Select(ty => ty.CheckInID).FirstOrDefault();

                //Allows us to find the relavant JobtypeID for this Bike
                var maintenancejobtypeid = db.MaintenanceJob.Where(lk => lk.BicycleID == Bicycle.BicycleID).Select(rr => rr.CheckIn.JobTypeID).FirstOrDefault();

                var repairjobtypeid = db.RepairJob.Where(rk => rk.BicycleID == Bicycle.BicycleID).Select(kr => kr.CheckIn.JobTypeID).FirstOrDefault();

                var CurrentDate = DateTime.Now.Date;

                var maintenance = db.MaintenanceJob.Where(mj => mj.BicycleID == BicycleID && CurrentDate == DbFunctions.TruncateTime(mj.DateScheduled)).FirstOrDefault();
                var repair = db.RepairJob.Where(rj => rj.BicycleID == BicycleID && CurrentDate == DbFunctions.TruncateTime(rj.DateScheduled)).FirstOrDefault();

                CheckInStatVM checkInStat = new CheckInStatVM();

                if (maintenance != null)
                {
                    //Instantiate a new checkin VM
                    checkInStat = new CheckInStatVM();

                    //Might not need this, but lets see, BikeID in the VM...
                    checkInStat.BicycleID = Bicycle.BicycleID; //---NEVERMIND, SIKE, lol

                    //Helps us find the relevant checkInStatus
                    checkInStat.CheckInStatus = db.CheckIn
                        .Include(rr => rr.CheckInStatu)
                        .Where(ra => ra.CheckInStatusID == ChekinID)
                        .Select(dd => dd.CheckInStatu.CheckInStatus).FirstOrDefault(); //---NEVERMIND LOL, sike

                    //Helps us find the relevant JobType
                    checkInStat.JobType = db.CheckIn
                        .Include(rr => rr.JobType)
                        .Where(ra => ra.JobTypeID == maintenancejobtypeid)
                        .Select(dd => dd.JobType.JobType1).FirstOrDefault();

                    //Some madness by a very over excited uncle who thought he could update a status :D lol
                    using (vvsContext vvsSys = new vvsContext())
                    {
                        CheckIn status = (from stat in vvsSys.CheckIn
                                          where stat.CheckInID == ChekinID
                                          select stat).FirstOrDefault();
                        status.CheckInStatusID = 2;
                        status.JobTypeID = 2;
                        status.CheckInDate = CurrentDate;//new

                        //Now we actually save the stuff we have updated in the DB
                        vvsSys.SaveChanges();
                    }

                    return checkInStat;

                    //Don't need these guys anymore ... I think
                    //dynamic dynamicBicycle = new ExpandoObject();
                    //dynamicBicycle.BicycleID = Bicycle.BicycleID;
                    //dynamicBicycle.JobType = "Maintenance";
                    //dynamicBicycle.CheckinStatus = db.CheckIn.Include(cs => cs.CheckInStatu).Include(cc => cc.MaintenanceJobs).Where(ms => ms.Maintena).FirstOrDefault();
                    //return dynamicBicycle;
                }
                else
                {
                    var jobFault = db.RepairJob.Where(rj => rj.BicycleID == BicycleID && CurrentDate == DbFunctions.TruncateTime(rj.DateScheduled)).FirstOrDefault();
                    if (jobFault != null)
                    {
                        //dynamic dynamicBicycle = new ExpandoObject();

                        //dynamicBicycle.BicycleID = Bicycle.BicycleID;
                        //dynamicBicycle.JobType = "Repair";

                        checkInStat = new CheckInStatVM();


                        checkInStat.BicycleID = Bicycle.BicycleID;

                        //Helps us find the relevant checkInStatus

                        //Helps us find the relevant JobType
                        checkInStat.JobType = db.CheckIn
                            .Include(rr => rr.JobType)
                            .Where(ra => ra.JobTypeID == repairjobtypeid)
                            .Select(dd => dd.JobType.JobType1).FirstOrDefault();

                        using (vvsContext vvsSys = new vvsContext())
                        {
                            CheckIn status = (from stat in vvsSys.CheckIn
                                              where stat.CheckInID == jobFault.CheckInID
                                              select stat).FirstOrDefault();
                            status.CheckInStatusID = 2;
                            status.JobTypeID = 1;
                            status.CheckInDate = CurrentDate; //new

                            //db.Entry(status).State = EntityState.Modified;

                            //Now we actually save the stuff we have updated in the DB
                            vvsSys.SaveChanges();
                        }

                        checkInStat.CheckInStatus = db.CheckIn
                            .Include(rr => rr.CheckInStatu)
                            .Where(ra => ra.CheckInID == jobFault.CheckInID)
                            .Select(dd => dd.CheckInStatu.CheckInStatus).FirstOrDefault(); //---NEVERMIND LOL, sike

                        return checkInStat;
                    }
                    else
                    {
                        dynamic dynamicBicycle = new ExpandoObject();
                        dynamicBicycle.BicycleID = Bicycle.BicycleID;
                        dynamicBicycle.Error = "No Job";
                        
                        return dynamicBicycle;
                    }
                }
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
            

            //switch (BicycleID)
            //{
            //    case 1:
            //        foreach (var bike in Bikes)
            //        {
            //            var repjob = db.RepairJob.Include(c => c.CheckIn).Where(jt => jt.CheckIn.JobTypeID == selectedJobType)/*.Where(bid => bid.BicycleID == bike.BicycleID)*/.FirstOrDefault();
            //            RepJobs.Add(repjob);
            //        }
            //        break;
            //    case 2:
            //        foreach (var bike in Bikes)
            //        {
            //            var mtcJob = db.MaintenanceJob.Include(c => c.CheckIn).Where(jt => jt.CheckIn.JobTypeID == selectedJobType)/*.Where(bid => bid.BicycleID == bike.BicycleID)*/.FirstOrDefault();
            //            MtcJobs.Add(mtcJob);
            //        }
            //        break;
            //    default:
            //        break;
            //}

        }

    }

        
    }


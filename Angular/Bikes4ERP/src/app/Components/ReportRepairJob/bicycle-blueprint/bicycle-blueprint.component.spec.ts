import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicycleBlueprintComponent } from './bicycle-blueprint.component';

describe('BicycleBlueprintComponent', () => {
  let component: BicycleBlueprintComponent;
  let fixture: ComponentFixture<BicycleBlueprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicycleBlueprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicycleBlueprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

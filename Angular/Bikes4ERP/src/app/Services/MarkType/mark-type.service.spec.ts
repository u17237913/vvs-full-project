import { TestBed } from '@angular/core/testing';

import { MarkTypeService } from './mark-type.service';

describe('MarkTypeService', () => {
  let service: MarkTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarkTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

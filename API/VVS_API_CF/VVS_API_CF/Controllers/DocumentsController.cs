﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.IO;
using System.Web.Http.Description;
using VVS_API_CF.Models;
using VVS_API_CF.Services;
using System.Web;

namespace VVS_API_CF.Controllers
{
    public class DocumentsController : ApiController
    {
        private vvsContext db = new vvsContext();

        //----------------------------------------ALL Documents API END-POINTS BELOW----------------------------------------------

        //GET ---> Documents
        [System.Web.Http.Route("api/Documents/GetAllDocuments")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllDocuments()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getDocumentsReturnList(db.Document.ToList());
        }

        private List<dynamic> getDocumentsReturnList(List<Document> forMark)
        {
            List<dynamic> dynamicDocuments = new List<dynamic>();
            foreach (Document document in forMark)
            {
                dynamic dynamicDocument = new ExpandoObject();

                dynamicDocument.DocumentID = document.DocumentID;
                dynamicDocument.DocumentName = document.DocumentName;
                dynamicDocument.DocumentPath = document.DocumentPath;

                dynamicDocuments.Add(dynamicDocument);
            }
            return dynamicDocuments;
        }

        //ADD ---> Document
        [System.Web.Http.Route("api/Documents/AddDocument")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddDocument([FromBody] Document document)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.Document.Add(document);
            db.SaveChanges();

            return getDocumentsReturnList(db.Document.ToList());
        }
        
        
        
        //add document file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/Documents/UploadFile/{ID}")]
        [System.Web.Http.Route("api/Documents/UploadDocument")]
        [System.Web.Mvc.HttpPost]
        public IHttpActionResult UploadDocument(/*int ID, */[FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            //var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Images/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Documents/");

            //string directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            //string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //string exeDir = System.IO.Path.GetDirectoryName(exePath);
            //DirectoryInfo binDir = System.IO.Directory.GetParent(exeDir);

            //var filepath = Path.GetFullPath("~/Resources/Images/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                //var _section = db.Section.Where(sc => sc.SectionID == ID).FirstOrDefault();
                //Get the last Section that was created
                var _document = db.Document.OrderByDescending(sc => sc.DocumentID).FirstOrDefault();

                _document.DocumentPath = filePathName;
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
        // finished document file upload tanx ------------------------------------>---------------------------------->---------->



        // read document file uploaded plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/Documents/GetDocument/{ID}")]
        [System.Web.Http.Route("api/Documents/GetDocument")]
        [System.Web.Mvc.HttpGet]
        public FileModel GetDocument(int ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                FileModel EventFile = new FileModel();
                //retrieve the file path
                var _document = db.Document.Where(par => par.DocumentID == ID).FirstOrDefault();

                //get the file as a byte array
                Byte[] bytes = File.ReadAllBytes(_document.DocumentPath);

                //allocate variables to dynamic object
                EventFile.fileName = _document.DocumentPath;

                //convert byte array to base64
                EventFile.fileAsBase64 = Convert.ToBase64String(bytes);
                return EventFile;
            }
            catch (Exception)
            {
                return null;
                //throw;
            }
        }
        // finished document file read tanx ------------------------------------>---------------------------------->---------->



        //UPDATE ---> Document
        [System.Web.Http.Route("api/Documents/UpdateDocument")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateDocument([FromBody] Document _businessrule)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                Document updatedDocument = (from mt in vvsSys.Document
                                                    where mt.DocumentID == _businessrule.DocumentID
                                                    select mt)
                                            .FirstOrDefault();

                updatedDocument.DocumentID = _businessrule.DocumentID;
                updatedDocument.DocumentName = _businessrule.DocumentName;
                updatedDocument.DocumentPath = _businessrule.DocumentPath;

                vvsSys.SaveChanges();
            }
            return true;
        }



        // update document file upload plis ------------------------------------>---------------------------------->---------->
        //[System.Web.Http.Route("api/Documents/UpdateDocument/{ID}")]
        [System.Web.Http.Route("api/Documents/UpdateDocument")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateDocument(int ID, [FromBody] FileModel file)
        {
            //this is the static file path on the server, change it to a relative path on deployment
            // var filepath = "C:/Users/Ndoro/Desktop/INF370_Project_VVS/vvs-full-project/API/VVS_API_CF/VVS_API_CF/Resources/Documents/";
            var filepath = HttpContext.Current.Server.MapPath("~/Resources/Documents/");

            //allocate a unique name to the file using the exact date and time plus the file name
            var filePathName = filepath
                                    + Path.GetFileNameWithoutExtension(file.fileName)
                                    + "-"
                                    + DateTime.Now.ToString()
                                    .Replace("/", "")
                                    .Replace(":", "")
                                    .Replace(" ", "")
                                    + Path.GetExtension(file.fileName);
            //check if the base 64 contains a comma
            if (file.fileAsBase64.Contains(","))
            {
                //if it does, then remove it now ka nhai jahman
                file.fileAsBase64 = file.fileAsBase64.Substring(file.fileAsBase64.IndexOf(",") + 1);
            }

            //Convert to a byte array
            file.fileasByteArray = Convert.FromBase64String(file.fileAsBase64);

            //Upload the file to the server
            using (var fileStr = new FileStream(filePathName, FileMode.CreateNew))
            {
                fileStr.Write(file.fileasByteArray, 0, file.fileasByteArray.Length);
            }

            //check that the file was created,
            if (File.Exists(filePathName))
            {
                var _document = db.Document.Where(pr => pr.DocumentID == ID).FirstOrDefault();

                _document.DocumentPath = filePathName;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        // finished document file update tanx ------------------------------------>---------------------------------->---------->
        
        
        
        //DELETE ---> BicycleDocument
        [System.Web.Http.Route("api/Documents/DeleteDocument")]
        [System.Web.Mvc.HttpDelete]
        public void DeleteDocument(int id)
        {
            Document DocumentToDelete = (db.Document
                                        .Where(o => o.DocumentID == id))
                                        .FirstOrDefault();

            db.Document.Remove(DocumentToDelete);
            db.SaveChanges();
        }



        // --- SCAFFOLDED APPROACH BELOW IF NEEDED...



/*        // GET: api/Documents
        public IQueryable<Document> GetDocument()
        {
            return db.Document;
        }

        // GET: api/Documents/5
        [ResponseType(typeof(Document))]
        public async Task<IHttpActionResult> GetDocument(int id)
        {
            Document document = await db.Document.FindAsync(id);
            if (document == null)
            {
                return NotFound();
            }

            return Ok(document);
        }

        // PUT: api/Documents/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDocument(int id, Document document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != document.DocumentID)
            {
                return BadRequest();
            }

            db.Entry(document).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DocumentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Documents
        [ResponseType(typeof(Document))]
        public async Task<IHttpActionResult> PostDocument(Document document)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Document.Add(document);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = document.DocumentID }, document);
        }

        // DELETE: api/Documents/5
        [ResponseType(typeof(Document))]
        public async Task<IHttpActionResult> DeleteDocument(int id)
        {
            Document document = await db.Document.FindAsync(id);
            if (document == null)
            {
                return NotFound();
            }

            db.Document.Remove(document);
            await db.SaveChangesAsync();

            return Ok(document);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DocumentExists(int id)
        {
            return db.Document.Count(e => e.DocumentID == id) > 0;
        }
*/
    }
}
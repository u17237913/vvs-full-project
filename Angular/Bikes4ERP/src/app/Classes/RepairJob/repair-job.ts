export class RepairJob {

    constructor () {}

    Assigned : boolean
    DateScheduled : string
    DateReported : string
    BicycleID : number
    CheckInID : number
    RepairJobID : number
    RepairJobStatusID : number
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Http;
using VVS_API_CF.Models;
using VVS_API_CF.ViewModels;
using System.Globalization;
using System.Web.Http.Cors;
using System.Dynamic;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace VVS_API_CF.Controllers
{
    public class ReportingController : ApiController
    {
        vvsContext db = new vvsContext();

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- MECHANIC SCHEDULE --- --- --- --- ---
        [Route("api/Reporting/GetMechanicSchedule")]
        [HttpGet]
        public /*dynamic*/IHttpActionResult GetMechanicSchedule(DateTime StartDate, DateTime EndDate, int SelectedMechanic)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<RepairJob> RepairjobsDue = new List<RepairJob>();
                List<MaintenanceJob> MaintenjobsDue = new List<MaintenanceJob>();

                RepairjobsDue = db.RepairJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(ci => ci.CheckInID != null)
                    .Where(nn => nn.CheckIn.CheckInDate == null)
                    .ToList();


                MaintenjobsDue = db.MaintenanceJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(nn => nn.CheckIn.CheckInDate == null)
                    .ToList();

                return Ok(getMechSched(RepairjobsDue, MaintenjobsDue));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        // GET: api/Users
        [System.Web.Http.Route("api/Reporting/GetUsers")]
        [System.Web.Mvc.HttpGet]
        public IHttpActionResult GetUsers()
        {
            db.Configuration.ProxyCreationEnabled = false;

            // Gte the identity
            var identity = User.Identity as System.Security.Claims.ClaimsIdentity;

            // Store all the claims in a list of claims
            IEnumerable<Claim> claims = identity.Claims;

            // Get the UserID from the claims List
            var UserID = claims.Where(p => p.Type == "UserID").FirstOrDefault()?.Value;

            // Get the UserTypeID from the claims List
            var UserTypeID = claims.Where(p => p.Type == "UserTypeID").FirstOrDefault()?.Value;

            if (UserTypeID == "1")
            {
                return Ok(getUsersReturnList(db.User.Include(s => s.UserType).Where(ss=>ss.UserTypeID==2).ToList()));
            }
            else
            {
                return Unauthorized();
            }
        }

        private List<dynamic> getUsersReturnList(List<User> forClient)
        {
            List<dynamic> dynamicUsers = new List<dynamic>();
            foreach (User user in forClient)
            {
                dynamic dynamicUser = new ExpandoObject();
                dynamicUser.UserID = user.UserID;
                dynamicUser.UserTypeID = user.UserTypeID;
                dynamicUser.Username = user.Username;
                dynamicUser.Password = user.Password;
                dynamicUser.Firstname = user.Firstname;
                dynamicUser.Surname = user.Surname;
                dynamicUser.Email = user.Email;
                dynamicUser.Telephone = user.Telephone;
                dynamicUser.UserType = user.UserType.UserType1;

                dynamicUsers.Add(dynamicUser);
            }
            return dynamicUsers;
        }
        public dynamic getMechSched(List<RepairJob> reps, List<MaintenanceJob> mnts)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.TableData = null;

                //TableData
                var rJs = reps;
                List<dynamic> ALLJOBS = new List<dynamic>();

                foreach (var item in rJs)
                {
                    MechanicScheduleVM RepairJobsSrc = new MechanicScheduleVM();

                    RepairJobsSrc.JobCode = item.CheckIn.CheckInID;
                    RepairJobsSrc.BicycleCode = item.Bicycle.BicycleID;
                    RepairJobsSrc.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    RepairJobsSrc.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    RepairJobsSrc.StudentContact = db.BicycleStudent.Include(std => std.Student).Where(stdc => stdc.BicycleID == item.BicycleID).Select(cont => cont.Student.PhoneNumber).FirstOrDefault();
                    RepairJobsSrc.GuardianContact = db.BicycleStudent.Include(std => std.Student).Where(stdc => stdc.BicycleID == item.BicycleID).Select(cont => cont.Student.Guardian.GuardianPhone).FirstOrDefault();
                    RepairJobsSrc.DateScheduled = /*(DateTime)*/item.DateScheduled/*db.RepairJob.Where(DS => DS.CheckInID == item.CheckIn.CheckInID).Select(sch => sch.DateScheduled).FirstOrDefault()*/.Value.ToString("dd-MM-yyyy");
                    //RepairJobsSrc.DateScheduled = item.DateScheduled.Value.ToString("HH:mm:ss");
                    ALLJOBS.Add(RepairJobsSrc);
                }

                var mJs = mnts;

                foreach (var item in mJs)
                {
                    MechanicScheduleVM MaintenanceJobsSrc = new MechanicScheduleVM();

                    MaintenanceJobsSrc.JobCode = item.CheckIn.CheckInID;
                    MaintenanceJobsSrc.BicycleCode = item.Bicycle.BicycleID;
                    MaintenanceJobsSrc.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    MaintenanceJobsSrc.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    MaintenanceJobsSrc.StudentContact = db.BicycleStudent.Include(std => std.Student).Where(stdc => stdc.BicycleID == item.BicycleID).Select(cont => cont.Student.PhoneNumber).FirstOrDefault();
                    MaintenanceJobsSrc.GuardianContact = db.BicycleStudent.Include(std => std.Student).Where(stdc => stdc.BicycleID == item.BicycleID).Select(cont => cont.Student.Guardian.GuardianPhone).FirstOrDefault();
                    MaintenanceJobsSrc.DateScheduled = /*(DateTime)*/item.DateScheduled/*db.MaintenanceJob.Where(DS => DS.CheckInID == item.CheckIn.CheckInID).Select(sch => sch.DateScheduled).FirstOrDefault()*/.Value.ToString("dd-MM-yyyy");
                    //MaintenanceJobsSrc.DateScheduled = item.DateScheduled.Value.ToString("HH:mm:ss");

                    ALLJOBS.Add(MaintenanceJobsSrc);
                }
                toReturn.TableData = ALLJOBS;

                return toReturn;
            }
            catch (Exception err)
            {
                return NotFound();
                //throw;
            }
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- MECHANIC REQUIREMENTS SCHEDULE --- --- --- --- ---
        [Route("api/Reporting/GetMechanicRequirementsSchedule")]
        [HttpGet]
        public /*dynamic*/IHttpActionResult GetMechanicRequirementsSchedule(DateTime StartDate, DateTime EndDate, int SelectedMechanic)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<RepairJob> RepairjobsDue = new List<RepairJob>();
                List<MaintenanceJob> MaintenjobsDue = new List<MaintenanceJob>();

                RepairjobsDue = db.RepairJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(nn => nn.CheckIn.CheckInDate == null)
                    .ToList();

                MaintenjobsDue = db.MaintenanceJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(nn => nn.CheckIn.CheckInDate == null)
                    .ToList();

                return Ok(getMechSchedObjs(RepairjobsDue, MaintenjobsDue));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        public dynamic /*IHttpActionResult*/ getMechSchedObjs(List<RepairJob> reps, List<MaintenanceJob> mnts)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.TableData = null;

                //TableData
                var rJs = reps;
                List<dynamic> ALLJOBS = new List<dynamic>();

                foreach (var item in rJs)
                {
                    MechanicScheduleRequirementsVM RepairJobsSrc = new MechanicScheduleRequirementsVM();

                    RepairJobsSrc.JobCode = item.CheckIn.CheckInID;
                    RepairJobsSrc.BicycleCode = item.Bicycle.BicycleID;
                    RepairJobsSrc.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    RepairJobsSrc.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    RepairJobsSrc.DateScheduled = /*(DateTime)*/item.DateScheduled/*db.RepairJob.Where( DS => DS.CheckInID == item.CheckIn.CheckInID ).Select( sch => sch.DateScheduled ).FirstOrDefault()*/.Value.ToString("dd-MM-yyyy");

                    ALLJOBS.Add(RepairJobsSrc);
                }

                var mJs = mnts;

                foreach (var item in mJs)
                {
                    MechanicScheduleRequirementsVM MaintenanceJobsSrc = new MechanicScheduleRequirementsVM();

                    MaintenanceJobsSrc.JobCode = item.CheckIn.CheckInID;
                    MaintenanceJobsSrc.BicycleCode = item.Bicycle.BicycleID;
                    MaintenanceJobsSrc.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    MaintenanceJobsSrc.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    MaintenanceJobsSrc.DateScheduled = /*(DateTime)*/item.DateScheduled.Value.ToString("dd-MM-yyyy");

                    ALLJOBS.Add(MaintenanceJobsSrc);
                }
                toReturn.TableData = ALLJOBS;

                return toReturn;
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        //THAT LITTLE SMALL TABLE AT THE BOTTOM
        [Route("api/Reporting/GetRequirementsLittleTable")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetRequirementsLittleTable(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<JobFault> jobfaultpartsneeded = new List<JobFault>();

                jobfaultpartsneeded = db.JobFault.Include(jf => jf.RepairJob).Where(pf => pf.RepairJob.DateScheduled >= StartDate && pf.RepairJob.DateScheduled <= EndDate).ToList();

                return Ok(getMechSchedREQUIREDPARTS(jobfaultpartsneeded));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        public dynamic /*IHttpActionResult*/ getMechSchedREQUIREDPARTS(List<JobFault> needed)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.LilTableData = null;

                //TableData
                List<dynamic> partsNeeded = new List<dynamic>();
                PartsNeededVM partNeededsrc = new PartsNeededVM();

                List<dynamic> uniq = new List<dynamic>();

                foreach (var guy in needed)
                {
                    var doGroupParts = db.JobFault
                                                    .Include(aa => aa.PartFault)
                                                    .Where(ab => ab.PartFaultID == guy.PartFaultID)
                                                    .GroupBy(zz => zz.PartFault.FaultType.PartNeeded);

                    foreach (var item in doGroupParts)
                    {
                        partNeededsrc = new PartsNeededVM();

                        //Checking if this item is in the list already so as to group a lot easier... we know... we know
                        if (!uniq.Contains(item.Key))
                        {
                            uniq.Add(item.Key);

                            partNeededsrc.partsNEEEDED = item.Key;
                            partNeededsrc.partCOUNT = item.Count();

                            partsNeeded.Add(partNeededsrc);
                        }
                    }
                }
                toReturn.LilTableData = partsNeeded;

                return toReturn;
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }  
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- STUDENT REPORT --- --- --- --- ---
        [Route("api/Reporting/GetStudentReport")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetStudentReport(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<Student> students = new List<Student>();

                /*students*/
                var studentTags = db.Log.Include(ChkI => ChkI.Tag).Where(a => a.DateTime >= StartDate && a.DateTime <= EndDate).ToList();

                    foreach (var tag in studentTags)
                    {
                        var bike = db.Bicycle.Where(b => b.TagID == tag.TagID).FirstOrDefault();
                        var student = db.BicycleStudent.Include(bs => bs.Student).Where(bs => bs.BicycleID == bike.BicycleID).FirstOrDefault().Student;
                        students.Add(student);
                    }
                    return Ok(getStudentObjs(students, StartDate, EndDate));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        public dynamic /*IHttpActionResult*/ getStudentObjs(List<Student> studentsInSchool, DateTime start, DateTime end)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.ChartData = null;
                toReturn.ChartData2 = null;
                toReturn.TableData = null;

                //var NumDays = (end - start).TotalDays;
                var NumDays = BusinessDaysUntil(start, end);
                //Chart Data 
                //Group all the student details by school for the graph so in this case the School Name will be the Key.
                //This is achieved by having the includes in the function GetReportData
                var schoolList = studentsInSchool.GroupBy(a => a.SchoolID);

                List<dynamic> schools = new List<dynamic>();

                    foreach (var item in schoolList)
                {
                    dynamic school = new ExpandoObject();

                    school.Name = db.School.Include(stu => stu.Students).Where(scID => scID.SchoolID == item.Key).Select(sn => sn.SchoolName).FirstOrDefault();

                    //var marks = db.StudentMark.Include("Student").Where(a=>a.StudentID==fore)
                    //var ave = Convert.ToDouble(item.Average(a => a.Grade), CultureInfo.InvariantCulture); //Average the pass rate

                    //    foreach(var st in school)
                    //{
                    //    ave = (double)(db.StudentMark
                    //                                    .Where(a => a.Date >= start && a.Date <= end)
                    //                                    .Where(a => a.StudentID == st.)
                    //                                    .Average(aa => aa.Mark));
                    //}
                    //school.Average = Math.Round(ave, 2);

                    schools.Add(school);
                }

                List<dynamic> ALL_Students = new List<dynamic>();
                StudentReportVM studentEntry = new StudentReportVM();

                //Table Data
                //Group by and sort products by BicycleID of Job so the Key in this case will be the BicycleCode.

                if (studentsInSchool.Count != 0)
                {
                    double bigAverage = 0;
                    //double bigAttendance = 0;

                    var entry = studentsInSchool.GroupBy(z => z.SchoolID);
                    List<dynamic> the_studentList = new List<dynamic>();



                    foreach (var school in entry)
                    {
                        int totAtt = new int();
                        double totMark = new double();
                        dynamic SpecificSchool = new ExpandoObject();

                        SpecificSchool.SchoolName = db.School.Where(sn => sn.SchoolID == school.Key).Select(sn => sn.SchoolName);

                        List<dynamic> SpecificStudentsInSchool = new List<dynamic>();
                        List<dynamic> SpecificStudentsInSchoolUnique = new List<dynamic>();

                        foreach (var studs in school)
                        {
                            //
                            studentEntry = new StudentReportVM();

                            int? studTag = db.BicycleStudent
                                .Include(b => b.Bicycle)
                                .Where(i => i.StudentID == studs.StudentID)
                                .FirstOrDefault().Bicycle.TagID;

                            studentEntry.StudID = db.Student
                                .Where(i => i.StudentID == studs.StudentID)
                                .Select(i => i.StudentID)
                                .FirstOrDefault();

                            studentEntry.StudentName = db.BicycleStudent
                                .Include(a => a.Student)
                                .Include(b => b.Bicycle)
                                .Where(aa => aa.Bicycle.TagID == studTag)
                                .Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname)
                                .FirstOrDefault();

                            studentEntry.BicycleCode = (int)db.Bicycle
                                .Include(tg => tg.Tag)
                                .Where(aa => aa.TagID == studTag)
                                .Select(bc => bc.BicyleCode)
                                .FirstOrDefault();

                            var CheckDuplicate = SpecificStudentsInSchool.Where(spis => spis.BicycleCode == studentEntry.BicycleCode).FirstOrDefault();

                            //Test to see if student is in the list
                            if (CheckDuplicate == null)
                            {
                                studentEntry.Age = Convert.ToInt32(((DateTime.Now - (DateTime)studs.StudentDOB).TotalDays) / 365);

                                //studentEntry.Attendance = ((db.Log ----------------------WRONG-------------------------
                                //                                    .Include(tg => tg.Tag)
                                //                                    .Where(d => d.TagID == studTag)
                                //                                    .Count()) / NumDays * 100);

                                studentEntry.Attendance = (int)(Math.Round(Convert.ToDouble((db.StudentAttendance
                                                                    .Include(tg => tg.Student)
                                                                    .Include(at => at.Attendance)
                                                                    .Where(a => a.Attendance.Date >= start && a.Attendance.Date <= end)
                                                                    .Where(d => d.StudentID == studs.StudentID)
                                                                    .Count()) / NumDays * 100), 2));

                                //bigAttendance = (double)(db.StudentAttendance.Include(aa => aa.Attendance)
                                //                                    .Where(o => o.StudentID == studs.StudentID)
                                //                                    //.Where(a => a.Attendance.Date >= start && a.Attendance.Date <= end)
                                //                                    .Where(a => a.Student.SchoolID == studs.SchoolID)
                                //                                    .Count());

                                studentEntry.AverageMark = Math.Round((double)(db.StudentMark
                                                                                    .Where(a => a.Date >= start && a.Date <= end)
                                                                                    .Where(a => a.StudentID == studs.StudentID)
                                                                                    .Average(aa => aa.Mark)),2);

                                bigAverage = (double)(db.StudentMark
                                                                    .Where(a => a.Date >= start && a.Date <= end)
                                                                    .Where(a => a.Student.SchoolID == studs.SchoolID)
                                                                    .Average(aa => aa.Mark));

                                // SpecificSchool.SumAttendance += studentEntry.Attendance;

                                SpecificStudentsInSchoolUnique.Add(studentEntry);
                            }
                            SpecificStudentsInSchool.Add(studentEntry);
                        }

                        foreach (var calc in SpecificStudentsInSchool)
                        {
                            totAtt = totAtt + calc.Attendance;
                            totMark = totMark + calc.AverageMark;
                        }
                        SpecificSchool.TotalStudents = SpecificStudentsInSchoolUnique.Count();

                        //SpecificSchool.AverageSchoolAttendance = Math.Round(totAtt/(SpecificSchool.TotalStudents), 2);
                        SpecificSchool.OverallSchoolAverage = Math.Round(bigAverage,2);

                        SpecificSchool.AverageSchoolAttendance = (totAtt / SpecificStudentsInSchool.Count());
                        //SpecificSchool.OverallSchoolAverage    = (totMark / SpecificStudentsInSchool.Count());

                        //SpecificSchool.Students = SpecificStudentsInSchool.GroupBy(a=>a.StudentName);
                        //SpecificSchool.Students = SpecificStudentsInSchool/*.GroupBy( a => a.StudID )*/;
                        SpecificSchool.Students = SpecificStudentsInSchoolUnique;

                        //the_studentList.Add(SpecificSchool);
                        ALL_Students.Add(SpecificSchool);
                    }
                }

                //Chart Data 2
                //Group all the student details by school for the graph so in this case the School Name will be the Key.
                //This is achieved by having the includes in the function GetReportData
                /*var schoolList2 = studentsInSchool.GroupBy(a => a.SchoolID);

                List<dynamic> schools2 = new List<dynamic>();
                foreach (var item in schoolList2)
                {
                    dynamic school2 = new ExpandoObject();

                    school2.Name = db.School.Include(stu => stu.Students).Where(scID => scID.SchoolID == item.Key).Select(sn => sn.SchoolName).FirstOrDefault();

                    var ave = Convert.ToDouble(item.Average(a => a.StudentMarks.Count), CultureInfo.InvariantCulture); //Average the pass rate

                    school2.Average = Math.Round(ave, 2);

                    schools2.Add(school2);
                }

    //            toReturn.ChartData2 = schools2;*/

                //toReturn.ChartData = schools; COME BACK AND GET THIS!
                toReturn.TableData = ALL_Students;

                return /*Ok*/(toReturn);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Calculates number of business days, taking into account:
        ///  - weekends (Saturdays and Sundays)
        ///  - bank/school holidays in the middle of the week
        /// </summary>
        /// <param name="firstDay">First day in the time interval</param>
        /// <param name="lastDay">Last day in the time interval</param>
        /// <param name="bankHolidays">List of bank holidays excluding weekends</param>
        /// <returns>Number of business days during the 'span'</returns>
        
        public /*static*/ int BusinessDaysUntil(/*this*/ DateTime firstDay, DateTime lastDay/*, params DateTime[] bankHolidays*/)
        {
            try
            {
                firstDay = firstDay.Date;
                lastDay = lastDay.Date;
                if (firstDay > lastDay)
                    throw new ArgumentException("Incorrect last day " + lastDay);

                TimeSpan span = lastDay - firstDay;
                int businessDays = span.Days + 1;
                int fullWeekCount = businessDays / 7;
                // find out if there are weekends during the time exceedng the full weeks
                if (businessDays > fullWeekCount * 7)
                {
                    // we are here to find out if there is a 1-day or 2-days weekend
                    // in the time interval remaining after subtracting the complete weeks
                    int firstDayOfWeek = (int)firstDay.DayOfWeek;
                    int lastDayOfWeek = (int)lastDay.DayOfWeek;
                    if (lastDayOfWeek < firstDayOfWeek)
                        lastDayOfWeek += 7;
                    if (firstDayOfWeek <= 6)
                    {
                        if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                            businessDays -= 2;
                        else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                            businessDays -= 1;
                    }
                    else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                        businessDays -= 1;
                }

                // subtract the weekends during the full weeks in the interval
                businessDays -= fullWeekCount + fullWeekCount;

                //USE THIS IF YOU WANNA INTEGRATE SCHOOL HOLIDAYS
                // subtract the number of bank holidays during the time interval
                /*foreach (DateTime bankHoliday in bankHolidays)
                {
                    DateTime bh = bankHoliday.Date;
                    if (firstDay <= bh && bh <= lastDay)
                        --businessDays;
                }*/

                return businessDays;
            }
            catch (Exception)
            {
                return 1;
                //throw;
            }
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- BICYCLE HISTORY REPORT --- --- --- --- ---
        [Route("api/Reporting/GetBicycleHistory")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetBicycleHistory(int SelectedSchool, int selectedJobType)
        {
            //try
            //{
                db.Configuration.ProxyCreationEnabled = false;
                List<BicycleStudent> bikeHist = new List<BicycleStudent>();
                List<School> Sch = new List<School>();

                //BikeHist has all the BicycleStudent objects from the school specified in the parameters
                // This was done specifically so that we only get the bikes from a specific school
                bikeHist = db.BicycleStudent
                    .Include(bs => bs.Bicycle)
                    .Include(bs => bs.Bicycle.BicycleStatu)
                    .Include(bs => bs.Student)
                    .Where(bs => bs.Student.SchoolID == SelectedSchool).ToList();

                //Bikes just gets all the bicycle objects so that we dont have to work with alot of navigration properties.
                //var Bikes = bikeHist.Select(bhu => bhu.Bicycle).ToList();

                //collecting all unique bikes in a list from that particular school to get list of all jobs done from that particular school/bikes
                var bikeHistUnique = bikeHist.GroupBy(bh => bh.BicycleID).Select(g => g.First()).ToList();

                //Getting all the bikes from BikeHistUnique  so that we can only work with the bicycle object instead of al the navigation properities.
                var Bikes = bikeHistUnique.Select(bhu => bhu.Bicycle).ToList();

                //var Jobs = db.CheckIn.ToList();

                List<RepairJob> RepJobs = new List<RepairJob>();
                List<MaintenanceJob> MtcJobs = new List<MaintenanceJob>();

                switch (selectedJobType)
                {
                    case 1:
                        //Get all the repair jobs from the database so that we can loop through them with the bicycleID
                        var RepairJobs = db.RepairJob.Include(mj => mj.CheckIn).ToList();

                        //Loop through each unique bike from the school that has been specified
                        foreach (var bike in Bikes)
                        {
                            //var repjob = db.RepairJob.Include(c => c.CheckIn).Where(jt => jt.CheckIn.JobTypeID == selectedJobType)/*.Where(bid => bid.BicycleID == bike.BicycleID)*/.FirstOrDefault();

                            // Loop through the repair jobs and extract the objects where the jobs have the specified bicycleID
                            foreach (var repJob in RepairJobs)
                            {
                                if (repJob.BicycleID == bike.BicycleID)
                                {
                                    RepJobs.Add(repJob);
                                }
                            }
                        }
                        break;
                    case 2:
                        //Get all the maintenance jobs from the database so that we can loop through them with the bicycleID
                        var MaintJobs = db.MaintenanceJob.Include(mj => mj.CheckIn).ToList();

                        //Loop through each unique bike from the school that has been specified
                        foreach (var bike in Bikes)
                        {
                            //var mtcJob = db.MaintenanceJob.Include(c => c.CheckIn).Where(jt => jt.CheckIn.JobTypeID == selectedJobType).Where(bid => bid.BicycleID == bike.BicycleID).FirstOrDefault();

                            // Loop through the maintenance jobs and extract the objects where the jobs have the specified bicycleID
                            foreach (var maintJob in MaintJobs)
                            {
                                if (maintJob.BicycleID == bike.BicycleID)
                                {
                                    MtcJobs.Add(maintJob);
                                }
                            }
                            //var mtcJob = db.MaintenanceJob.Include(c => c.CheckIn).Where( jt => jt.BicycleID == bike.BicycleID).FirstOrDefault();
                            //MtcJobs.Add(mtcJob);
                        }
                        break;
                    default:
                        break;
                }

                return Ok(getBikeHistRepObj(RepJobs, MtcJobs));
            //}
            //catch (Exception err)
            //{
            //    return NotFound();
            //    //throw;
            //}
        }

        public dynamic /*IHttpActionResult*/ getBikeHistRepObj(List<RepairJob> repaired, List<MaintenanceJob> maintained)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //try
            //{
                dynamic toReturn = new ExpandoObject();
                toReturn.TableData = null;

                List<dynamic> BikeCodes = new List<dynamic>();
                BikeHistoryReportVM specificBikeJobs = new BikeHistoryReportVM();

                //Table Data
                //Group by and sort products by BicycleID of Job so the Key in this case will be the BicycleCode.

                if (repaired.Count != 0)
                {
                    var rjd = repaired.GroupBy(z => z.BicycleID);
                    List<dynamic> SpecificBikeJobZ = new List<dynamic>();

                    foreach (var bike in rjd)
                    {
                        dynamic SpecificBikeZ = new ExpandoObject();

                        //SpecificBikeZ.BicycleCode = bike.Key;
                        SpecificBikeZ.BicycleCode = (int)db.BicycleStudent
                            .Include(b => b.Bicycle)
                            .Where(b_ => b_.BicycleID == bike.Key)
                            .Select(b_code => b_code.Bicycle.BicyleCode)
                            .FirstOrDefault();

                        SpecificBikeZ.TotalJobs = bike.Count();

                        SpecificBikeZ.BicycleStatus = db.Bicycle
                            .Include(aa => aa.BicycleStatu)
                            .Where(rr => rr.BicycleID == bike.Key)
                            .Select(tt => tt.BicycleStatu.BicycleStatus)
                            .FirstOrDefault();

                        SpecificBikeZ.DateAdded = db.Bicycle
                            .Where(da => da.BicycleID == bike.Key)
                            .Select(ddaa => ddaa.DateAdded)
                            .FirstOrDefault().Value.ToString("dd-MM-yyyy HH:mm:ss");

                        List<dynamic> JobList = new List<dynamic>();

                        foreach (var specBk in bike)
                        {
                            specificBikeJobs = new BikeHistoryReportVM();

                            specificBikeJobs.JOBcode = specBk.CheckIn.CheckInID;
                            specificBikeJobs.specificJobID = specBk.RepairJobID;

                            specificBikeJobs.StudentName = db.BicycleStudent
                                .Include(a => a.Student)
                                .Where(ww => ww.BicycleID == specBk.BicycleID)
                                .Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname)
                                .FirstOrDefault();

                            if (/*(DateTime)*/specBk.CheckIn.CheckInDate.HasValue == false )
                            {
                                specificBikeJobs.DateCompleted = "Pending";
                            }
                            else
                            {
                                specificBikeJobs.DateCompleted = /*(DateTime)*/specBk.CheckIn.CheckInDate.Value.ToString("dd-MM-yyyy");
                                //specificBikeJobs.DateCompleted = /*(DateTime)*/specBk.CheckIn.CheckInDate.Value.ToString("dd-MM-yyyy HH:mm:ss");
                        }

                        JobList.Add(specificBikeJobs);
                        }
                        SpecificBikeZ.Jobs = JobList;

                        SpecificBikeJobZ.Add(SpecificBikeZ);
                        BikeCodes = SpecificBikeJobZ;
                    }
                }
                else
                {
                    var mjd = maintained.GroupBy(z => z.BicycleID);
                    List<dynamic> SpecificBikeJobZ = new List<dynamic>();

                    foreach (var bike in mjd)
                    {
                        dynamic SpecificBikeZ = new ExpandoObject();

                        //SpecificBikeZ.BicycleCode = bike.Key;
                        SpecificBikeZ.BicycleCode = (int)db.BicycleStudent
                            .Include(b => b.Bicycle)
                            .Where(b_ => b_.BicycleID == bike.Key)
                            .Select(b_code => b_code.Bicycle.BicyleCode)
                            .FirstOrDefault();

                        SpecificBikeZ.TotalJobs = bike.Count();

                        SpecificBikeZ.BicycleStatus = db.Bicycle
                            .Include(aa => aa.BicycleStatu)
                            .Where(rr => rr.BicycleID == bike.Key)
                            .Select(tt => tt.BicycleStatu.BicycleStatus)
                            .FirstOrDefault();

                        SpecificBikeZ.DateAdded = db.Bicycle
                            .Where(da => da.BicycleID == bike.Key)
                            .Select(ddaa => ddaa.DateAdded)
                            .FirstOrDefault().Value.ToString("dd-MM-yyyy HH:mm:ss");

                        List<dynamic> JobList = new List<dynamic>();
                        //The purpose of this ForEach is to generate a list of the jobs for A specific Bike
                        foreach (var specBk in bike)
                        {
                            specificBikeJobs = new BikeHistoryReportVM();

                            specificBikeJobs.JOBcode = specBk.CheckIn.CheckInID;
                            specificBikeJobs.specificJobID = specBk.MaintenanceJobID;

                            specificBikeJobs.StudentName = db.BicycleStudent
                                .Include(a => a.Student)
                                .Where(ww => ww.BicycleID == specBk.BicycleID)
                                .Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname)
                                .FirstOrDefault();

                        if (/*(DateTime)*/specBk.CheckIn.CheckInDate.HasValue == false)
                        {
                            specificBikeJobs.DateCompleted = "Pending";
                        }
                        else
                        {
                            specificBikeJobs.DateCompleted = /*(DateTime)*/specBk.CheckIn.CheckInDate.Value.ToString("dd-MM-yyyy HH:mm:ss");
                        }

                        //Above is the Job information for the bicycle

                        JobList.Add(specificBikeJobs);
                        }
                        SpecificBikeZ.Jobs = JobList;

                        SpecificBikeJobZ.Add(SpecificBikeZ);
                        BikeCodes = SpecificBikeJobZ;
                    }
                }
                toReturn.TableData = BikeCodes;

                return /*Ok*/toReturn;
        }

        //Additional API Endpoints needed for generating a Bicycle History Report.

        //GET ---> SCHOOL
        [System.Web.Http.Route("api/Reporting/GetAllSchools")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllSchools()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getSchoolsReturnList(db.School.ToList());
        }

        private List<dynamic> getSchoolsReturnList(List<School> forReport)
        {
            List<dynamic> dynamicSchools = new List<dynamic>();
            foreach (School sxul in forReport)
            {
                dynamic dynamicSxul = new ExpandoObject();

                dynamicSxul.SchoolID = sxul.SchoolID;
                dynamicSxul.VillageID = sxul.VillageID;
                dynamicSxul.SchoolName = sxul.SchoolName;
                dynamicSxul.SchoolTelephone = sxul.SchoolTelephone;
                dynamicSxul.SchoolEmail = sxul.SchoolEmail;

                dynamicSchools.Add(dynamicSxul);
            }
            return dynamicSchools;
        }

        //GET ---> JOB TYPE
        [System.Web.Http.Route("api/Reporting/GetAllJobTypes")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllJobTypes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getJTypesReturnList(db.JobType.ToList());
        }

        private List<dynamic> getJTypesReturnList(List<JobType> forReport)
        {
            List<dynamic> dynamicJobTypes = new List<dynamic>();
            foreach (JobType jt in forReport)
            {
                dynamic dynamicJT = new ExpandoObject();

                dynamicJT.JobTypeID = jt.JobTypeID;
                dynamicJT.JobType = jt.JobType1;

                dynamicJobTypes.Add(dynamicJT);
            }
            return dynamicJobTypes;
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- LOG REPORT --- --- --- --- ---
        [Route("api/Reporting/GetLogReport")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetLogReport(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<Log> logs = new List<Log>();

                /*logs*/
                logs = db.Log.Include(ChkI => ChkI.Antenna).Include(bk => bk.Tag).Where(a => a.DateTime >= StartDate && a.DateTime <= EndDate).ToList();

                return Ok(getLogObjs(logs));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }

        }

        public dynamic /*IHttpActionResult*/ getLogObjs(List<Log> logsInRange)
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.TableData = null;

                List<dynamic> ALL_Logs = new List<dynamic>();
                LogReportHistoryVM logEntry = new LogReportHistoryVM();

                //Table Data
                //Group by and sort products by BicycleID of Job so the Key in this case will be the BicycleCode.

                if (logsInRange.Count != 0)
                {
                    var entry = logsInRange.GroupBy(z => z.AntennaID);
                    List<dynamic> the_logsList = new List<dynamic>();

                    foreach (var antenna in entry)
                    {
                        dynamic Specificentry = new ExpandoObject();

                        Specificentry.AntennaPort = db.Antenna.Where(rr=>rr.AntennaID == antenna.Key).Select(rr => rr.AntennaAddress);
                        Specificentry.TotalTags = antenna.Count();
                        Specificentry.AntennaStatus = db.Antenna.Include(aa => aa.AntennaStatu).Where(rr => rr.AntennaID == antenna.Key).Select(tt => tt.AntennaStatu.AntennaStatus).FirstOrDefault();
                        Specificentry.Coordinates = db.Antenna.Where(da => da.AntennaID == antenna.Key).Select(CooD => CooD.CoOrdinates).FirstOrDefault();

                        List<dynamic> SpecificTagsInAntenna = new List<dynamic>();

                        foreach (var tagz in antenna)
                        {
                            //
                            logEntry = new LogReportHistoryVM();

                            logEntry.TagSerialNumber = tagz.Tag.TagSerialNumber;

                            if (/*(int)*/db.Bicycle.Include(tg => tg.Tag).Where(aa => aa.TagID == tagz.TagID).Select(bc => bc.BicyleCode).FirstOrDefault().HasValue == false)
                            {
                                logEntry.BicycleCode = "No bicycle right now";
                            }
                            else
                            {
                                logEntry.BicycleCode = db.Bicycle.Include(tg => tg.Tag).Where(aa => aa.TagID == tagz.TagID).Select(bc => bc.BicyleCode).FirstOrDefault().ToString();
                            }

                            logEntry.StudentName = db.BicycleStudent
                                                                    .Include(a => a.Student)
                                                                    .Include(b => b.Bicycle)
                                                                    /*.Where(ww => ww.BicycleID == ww.Bicycle.BicycleID)*/
                                                                    .Where(aa => aa.Bicycle.TagID == tagz.TagID)
                                                                    .Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname)
                                                                    .FirstOrDefault();
                            
                            logEntry.entryDate = /*(DateTime)*/tagz.DateTime.Value.ToString("dd-MM-yyyy");
                            logEntry.entryTime = /*(DateTime)*/tagz.DateTime.Value.ToString("HH:mm:ss");
                            
                            logEntry.School = db.Student.Include(a => a.School)
                                                                    .Include(b => b.BicycleStudents)
                                                                    .Where(ww => ww.SchoolID == ww.School.SchoolID)
                                                                    .Select(tt => tt.School.SchoolName)
                                                                    .FirstOrDefault();

                            SpecificTagsInAntenna.Add(logEntry);
                        }
                        Specificentry.Tags = SpecificTagsInAntenna;

                        the_logsList.Add(Specificentry);
                        ALL_Logs.Add(Specificentry);
                    }
                }
                toReturn.TableData = ALL_Logs;

                return /*Ok*/(toReturn);
            }
            catch (Exception err)
            {
                return NotFound();
                //throw;
            }
        }

//-------------------------------------------------------------------------------------------------------------------------------------------------------

        // --- --- --- --- --- PARTS USED REPORT --- --- --- --- ---
        [Route("api/Reporting/GetPartsUsedReport")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetPartsUsedReport(DateTime StartDate, DateTime EndDate, int SelectedMechanic)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<RepairJob> RepairjobsDue = new List<RepairJob>();
                List<MaintenanceJob> MaintenjobsDue = new List<MaintenanceJob>();

                /*repairs*/
                RepairjobsDue = db.RepairJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(nn => nn.CheckIn.CheckInDate != null)
                    .ToList();

                /*maintenances*/
                MaintenjobsDue = db.MaintenanceJob
                    .Include(ChkI => ChkI.CheckIn)
                    .Include(bk => bk.Bicycle)
                    .Include(y => y.CheckIn.MechanicSchool)
                    .Where(y => y.CheckIn.MechanicSchool.UserID == SelectedMechanic)
                    .Where(a => a.DateScheduled >= StartDate && a.DateScheduled <= EndDate)
                    .Where(nn => nn.CheckIn.CheckInDate != null)
                    .ToList();

                return Ok(getPartsUsedObjs(RepairjobsDue, MaintenjobsDue));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        public dynamic /*IHttpActionResult*/ getPartsUsedObjs(List<RepairJob> reps, List<MaintenanceJob> mnts)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.TableData = null;

                //TableData
                List<dynamic> ALLJOBS = new List<dynamic>();

                var rJs = reps;
                foreach (var item in rJs)
                {
                    PartsUsedVM RepairJobs = new PartsUsedVM();

                    RepairJobs.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    RepairJobs.BicycleCode = (int)db.Bicycle.Include(tg => tg.RepairJobs).Where(aa => aa.BicycleID == item.BicycleID).Select(bc => bc.BicyleCode).FirstOrDefault();
                    RepairJobs.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    RepairJobs.JobCode = item.CheckIn.CheckInID;
                    RepairJobs.DateCompleted = /*(DateTime)*/item.CheckIn.CheckInDate.Value.ToString("dd-MM-yyyy");
                    RepairJobs.TimeCompleted = /*(DateTime)*/item.CheckIn.CheckInDate.Value.ToString("HH:mm:ss");

                    ALLJOBS.Add(RepairJobs);
                }
                var mJs = mnts;
                foreach (var item in mJs)

                {
                    PartsUsedVM MaintenanceJobs = new PartsUsedVM();

                    MaintenanceJobs.JobType = db.CheckIn.Include(jt => jt.JobType).Where(jd => jd.CheckInID == item.CheckInID).Select(jst => jst.JobType.JobType1).FirstOrDefault();
                    MaintenanceJobs.BicycleCode = (int)db.Bicycle.Include(tg => tg.MaintenanceJobs).Where(aa => aa.BicycleID == item.BicycleID).Select(bc => bc.BicyleCode).FirstOrDefault();
                    MaintenanceJobs.StudentName = db.BicycleStudent.Include(a => a.Student).Where(ww => ww.BicycleID == item.BicycleID).Select(tt => tt.Student.StudentName + " " + tt.Student.StudentSurname).FirstOrDefault();
                    MaintenanceJobs.JobCode = item.CheckIn.CheckInID;
                    MaintenanceJobs.DateCompleted = /*(DateTime)*/item.CheckIn.CheckInDate.Value.ToString("dd-MM-yyyy");
                    MaintenanceJobs.TimeCompleted = /*(DateTime)*/item.CheckIn.CheckInDate.Value.ToString("HH:mm:ss");

                    ALLJOBS.Add(MaintenanceJobs);
                }
                toReturn.TableData = ALLJOBS;

                return /*Ok*/(toReturn);
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        //THAT LITTLE SMALL TABLE AT THE BOTTOM
        [Route("api/Reporting/GetPartsUsedLittleTable")]
        [HttpGet]
        public /*dynamic*/ IHttpActionResult GetPartsUsedLittleTable(DateTime StartDate, DateTime EndDate)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                List<JobFault> jobfaultpartsneeded = new List<JobFault>();

                jobfaultpartsneeded = db.JobFault.Include(jf => jf.RepairJob)/*.Include(JFT=>JFT.JobFaultTasks)*/./*Where(ud => ud.PartsUsed == true).*/Where(pf => pf.RepairJob.DateScheduled >= StartDate && pf.RepairJob.DateScheduled <= EndDate).ToList();

                return Ok(getPartsUsedCalc(jobfaultpartsneeded));
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }

        public dynamic /*IHttpActionResult*/ getPartsUsedCalc(List<JobFault> needed)
        {
            try
            {
                dynamic toReturn = new ExpandoObject();
                toReturn.LilTableData = null;

                //TableData
                List<dynamic> NeededPartsUsed = new List<dynamic>();
                PartUsedCalcVM usedPsrc = new PartUsedCalcVM();

                List<dynamic> uniqu = new List<dynamic>();
                foreach (var guyz in needed)
                {
                    var doGroupPartsUsed = db.JobFault
                                                    .Include(aa => aa.PartFault)
                                                    .Where(ab => ab.PartFaultID == guyz.PartFaultID)
                                                    .GroupBy(zz => zz.PartFault.FaultType.PartNeeded);

                    foreach (var item in doGroupPartsUsed)
                    {
                        usedPsrc = new PartUsedCalcVM();

                        //Checking if this item is in the list already so as to group a lot easier... we know... we know
                        if (!uniqu.Contains(item.Key))
                        {
                            uniqu.Add(item.Key);

                            usedPsrc.PartsUtilized = item.Key;
                            usedPsrc.PartsUsedCount = item.Count();

                            NeededPartsUsed.Add(usedPsrc);
                        }
                    }
                }
                toReturn.LilTableData = NeededPartsUsed;

                return /*Ok*/(toReturn);
            }
            catch (Exception)
            {
                return NotFound();
                //throw;
            }
        }
    }
}
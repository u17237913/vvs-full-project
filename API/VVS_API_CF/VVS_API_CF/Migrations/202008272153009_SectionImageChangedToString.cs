﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SectionImageChangedToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Section", "SectionImage", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Section", "SectionImage", c => c.Binary());
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Route } from 'src/app/Classes/Route/route';
import { RouteAntenna } from 'src/app/Classes/RouteAntenna/route-antenna';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class RouteService {


  RouteForm: any;

  constructor(public http: HttpClient, public audit : AuditTrailService) {}

  GetRoutes(): Observable<Route[]> {
    //this.audit.addAuditTrail(4, "Route", null, null).subscribe(() => console.error);
    return this.http.get<Route[]>(environment.apiURL + '/Routes/GetRoutes');
  }

  AddRoute(newRoute: Route) {
    this.audit.addAuditTrail(1, "Route", newRoute, null).subscribe(() => console.error);
    return this.http.post(
      environment.apiURL + '/Routes/PostRoute',
      newRoute
      );
  }

  UpdateRoute(id: any, route: Route, olddata: any) {
    this.audit.addAuditTrail(2, "Route", route, olddata).subscribe(() => console.error);
    return this.http.put(
      environment.apiURL + '/Routes/PutRoute?id=' + id,
      route
    );
  }

  DeleteRoute(id: number, olddata: any) {
    this.audit.addAuditTrail(3, "Route", null, olddata).subscribe(() => console.error);
    return this.http.delete(
      environment.apiURL + '/Routes/School?id=' + id
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { MaintenanceList } from 'src/app/Classes/MaintenanceList/maintenance-list';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { User } from 'src/app/Services/user.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-maintenance-list',
  templateUrl: './maintenance-list.component.html',
  styleUrls: ['./maintenance-list.component.css']
})
export class MaintenanceListComponent implements OnInit {

  MaintenanceList: MaintenanceList[];
  public values = [];
  ListForm: MaintenanceList;
  ID: any;
  complete: boolean = false;

  constructor(private ListService: MyServiceService, public router: Router, public route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.PopulateTable();
  }

  PopulateTable() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;

    })

    console.log("hello");
    this.ListService.GetMaintenanceLists(this.ID).subscribe(data => data.forEach(element => {
      if (element.MaintenanceJobID == this.ID) {
        console.log(element.MaintenanceJobID)
        this.MaintenanceList = data as MaintenanceList[];
        this.completionCheck();
      }
    }));

  }

  UpdateMaintenanceJobStatus(JobID: number) {
    console.log(JobID);
    this.ListService.UpdateMaintenanceJobStatus(JobID).subscribe(() => {});
  }

  goBack() {
    this.location.back();
  }

  UpdateStatus(listID: number, listJob: MaintenanceList) {
    if (listJob.MaintenanceListStatusID == 1) {
      // this.isChecked = true;
      listJob.MaintenanceListStatusID = 3;
      this.ListService.UpdateMaintenanceLists(listID, listJob).subscribe(() => {});
    } 

    console.log(listJob);
    // this.PopulateTable();
    this.completionCheck();
  }

  async CheckBox() {
    await this.PopulateTable();
    // debugger;
    this.MaintenanceList.forEach((element) => {
      var CheckBox = document.getElementById(
        'Check' + element.MaintenanceListID.toString()
      ) as HTMLInputElement;
      if (element.MaintenanceListStatusID == 3) {
        CheckBox.checked = true;
      } else {
        CheckBox.checked = false;
      }
    });
  }

  completionCheck() {
    var count = 0;
    var check = 0;
    this.MaintenanceList.forEach(element => {
       count = count + 1;
       if (element.MaintenanceListStatusID == 3) {
         check = check + 1;
       }
    });
    if (count == check) {
      this.complete = true;
    }
    else{
      this.complete = false;
    }
  }

}

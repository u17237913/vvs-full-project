import { Section } from 'src/app/Classes/Section/section';
import { PartType } from 'src/app/Classes/PartType/part-type';

export class Part {

    constructor (){}

    PartID : number
    //PartTypeID : number
    PartDescription : string
    PartName : string
    SectionName : string
    Section : Section
    PartType : PartType
    SectionID : number
    PartTypeID : number
    PartImage : string
}

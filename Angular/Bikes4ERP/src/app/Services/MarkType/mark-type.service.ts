import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';
import { AuditTrailService } from 'src/app/Services/AuditTrail/audit-trail.service';

@Injectable({
  providedIn: 'root'
})
export class MarkTypeService {

  url : string;
  header : any;
  option : any;

  constructor(private http : HttpClient, public audit : AuditTrailService) { 
    this.url = environment.apiURL + '/MarkTypes/';
    const headerSettings: {[name: string]: string | string[]; } = {}; 
    
    let headers = new HttpHeaders({'Content-Type': 'application/json'});
    this.option = {headers : headers};
}

//MARK TYPE ---------------------------------------//

    //GET ---> Mark Type ---------------------------------------------------------------------
    getMarkTypes():Observable<MarkType[]> {
      //this.audit.addAuditTrail(4, "MarkType", null, null).subscribe(() => console.error);
      return this.http.get<MarkType[]>(this.url + 'GetAllMarkTypes/'); 
    }

    //ADD ---> Mark Type ---------------------------------------------------------------------
    addMarkType(newMarkType : MarkType){
      this.audit.addAuditTrail(1, "MarkType", newMarkType, null).subscribe(() => console.error);
      return this.http.post(this.url + 'AddMarkType/' , newMarkType); 
    }

    //UPDATE ---> Mark Type ---------------------------------------------------------------------
    UpdateMarkType(updatedMarkType : MarkType, olddata: any){
      this.audit.addAuditTrail(2, "MarkType", updatedMarkType, olddata).subscribe(() => console.error);
      return this.http.post(this.url + 'UpdateMarkType/' , updatedMarkType); 
  }

    //DELETE ---> Mark Type ---------------------------------------------------------------------
    DeleteMarkType(id : number, olddata: any){ 
      this.audit.addAuditTrail(3, "MarkType", null, olddata).subscribe(() => console.error);
      return this.http.delete(this.url + 'DeleteMarkType/?id=' + id, this.option); 
  }

}
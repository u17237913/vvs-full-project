import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicyclePartComponent } from './bicycle-part.component';

describe('BicyclePartComponent', () => {
  let component: BicyclePartComponent;
  let fixture: ComponentFixture<BicyclePartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicyclePartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicyclePartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MyServiceService } from 'src/app/Services/my-service.service';
import { take } from 'rxjs/operators';
import { FaultType } from 'src/app/Classes/FaultType/fault-type';
import { Part } from 'src/app/Classes/Part/part';
import { PartFault } from 'src/app/Classes/PartFault/part-fault';
import { FaultTypeService } from 'src/app/Services/FaultType/fault-type.service';
import { FileToUpload } from 'src/app/Classes/FileToUpload/file-to-upload';
import { DomSanitizer } from '@angular/platform-browser';
import { FileUploadServiceService } from 'src/app/Services/FileUpload/file-upload-service.service';

@Component({
  selector: 'app-view-fault-type',
  templateUrl: './view-fault-type.component.html',
  styleUrls: ['./view-fault-type.component.css'],
})
export class ViewFaultTypeComponent implements OnInit {
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    public route: ActivatedRoute,
    public faultTypeService: MyServiceService,
    public service2: FaultTypeService,
    private sanitizer: DomSanitizer,
    private uploadService: FileUploadServiceService
  ) {}

  PartFaultForm: any;
  data = false;
  message: string;
  ID: any;
  FaultType: FaultType[] = [];
  Parts: Part[] = [];
  oldData: any;

    // imageUrl: string;
    fileToUpload: File = null;
    thefile: FileToUpload = new FileToUpload();
    imageUrl: string = '../../../assets/images/img-placeholder.jpg';

  //declare the max file size
  public MAX_SIZE: number = 10048576; //BE CAREFUL, extra zero included!

  //declare a file variable, this is for the initial upload
  theFile: any;

  //If a picture from the API is available, then set this to true so it can
  //be used in the transformer function
  isPictureAvailable = false;
  Base64Picture: string;

  //variable for error messages - note
  messages: string[] = [];

  ngOnInit(): void {
    this.PartFaultForm = new FormGroup({
      FaultType1: new FormControl(),
      PartNeeded: new FormControl(),
      FaultTypeID: new FormControl(),
      // FaultImage: new FormControl(),
    });

    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('ID'));
      this.ID = id;
    });

    this.service2.GetFaultTypes().subscribe((data) =>
      data.forEach((element) => {
        //console.log(element.PartNeeded);
        if (element.FaultTypeID == this.ID) {
          this.PartFaultForm = this.formbuilder.group({
            FaultTypeID: [element.FaultTypeID, [Validators.required]],
            FaultType1: [element.FaultType1, [Validators.required]],
            PartNeeded: [element.PartNeeded, [Validators.required]],
          });
          this.oldData = element;
        }
      })
    );

    //PICKUP FROM HERE ---> READ image to display (WORKING)
    this.uploadService.getFaultImage(this.ID).subscribe((data) => {
      this.thefile = data;
      this.Base64Picture = data.fileAsBase64;
      if (this.Base64Picture) {
        this.isPictureAvailable = true;
      }
      this.message = 'Image has been successfully loaded boiii !';
    }); //GetImage

    //GET THE REST OF THE LISTS
    this.getFaults();
    this.getParts();

    //var UploadButton = <HTMLInputElement> document.getElementById("hideME");
    var UploadArea = document.getElementById('UploadArea');
    //var UploadBtn = document.getElementById('UploadBtn');
    var ObjArea = document.getElementById('ObjArea');
    var CreateObj = document.getElementById('CreateObj');

    UploadArea.hidden = false;
    //UploadBtn.hidden = true;
    ObjArea.hidden = false;
    CreateObj.hidden = false;
  }//ngOnInit

  getParts() {
    this.faultTypeService.GetParts()
      .pipe(take(1))
      .subscribe((Parts) => {
        this.Parts = Parts;
        // console.log(this.UserTypes);
      });
  }

  getFaults() {
    this.faultTypeService.GetFaultTypes()
      .pipe(take(1))
      .subscribe((Faults) => {
        this.FaultType = Faults;
        // console.log(this.UserTypes);
      });
  }

  //this event is called from the html file on the file upload element
  onFileChange(event) {
    this.theFile = null;

    //check that there is a file and the file is not 0 bits
    if (event.target.files && event.target.files.length > 0) {
      //Disallow the uploading of files larger than 10MB.
      if (event.target.files[0].size < this.MAX_SIZE) {
        //set theFile property
        this.theFile = event.target.files[0];

        //Show image preview
        var reader = new FileReader();

        reader.readAsDataURL(this.theFile);
        reader.onload = () => {
          this.imageUrl = reader.result as string;
        };
      } else {
        //Display error message <-!-!-> watchout, this allows for multiple files...
        this.messages.push(
          'File: ' + event.target.files[0].name + 'is too large to upload.'
        );
      }
    }
  } //OnFileChange

  //This function will convert base 64 string to an image and return it
  transformer() {
    if (this.isPictureAvailable) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(
        'data:image;base64,' + this.Base64Picture
      );
    }
  }

  //this function is now called when the submit button is clicked
  uploadFile(): void {
    this.readAndUploadFile(this.theFile, this.ID);
  }

  //this function converts to base 64 and makes the service call, which sends stuff to the API bla bla bla
  private readAndUploadFile(theFile: any, id: number) {
    //file to upload model
    let file = new FileToUpload();

    //Set File Information
    file.fileName = theFile.name;

    //Use FileReader() object to get file to upload <-!-!-> NOTE: FileReader only works with newer browsers
    let reader = new FileReader();

    //Setup onload event for reader
    reader.onload = () => {
      //Store base64 encoded representation of file
      file.fileAsBase64 = reader.result.toString();

      //POST to server
      this.uploadService.updateFaultImage(file, id).subscribe((respondation) => {
        this.messages.push('Upload complete');
      });
    };

    //Read the file
    reader.readAsDataURL(theFile);

    //Nav back to the list
    //this.router.navigate(['/faulttype']);
  }

  onFormSubmit(PartFaultForm) {
    //console.log();
    const _faulttype = PartFaultForm.value;
    this.UpdateFaultType(this.ID, _faulttype);
  }
  UpdateFaultType(ID: any, PartFaultForm: PartFault) {
    //console.log(PartFaultForm)
    this.faultTypeService.UpdatePartFault(ID, PartFaultForm, this.oldData).subscribe(() => {
      if (this.theFile != null) {
        this.uploadFile();
      } else {}
      this.message = 'Section has been successfully updated boiii !';
      this.PartFaultForm.reset;
    }); //UpdatePartFault
    //console.log(ID, PartFaultForm);

    this.GoBack();
  } //UpdateFaultType

  GoBack() {
    this.router.navigate(['/faulttype']);
  }

  DeleteFaultType(faulttypeID: any) {
    faulttypeID = this.ID;
    this.faultTypeService.DeletePartFault(
      faulttypeID,
      this.oldData
    ).subscribe(() => {
      this.data = true;
    });
    this.GoBack();
  } //DeleteFaultType
}//OnInit
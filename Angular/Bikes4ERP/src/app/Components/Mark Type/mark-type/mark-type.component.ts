import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MarkTypeService } from 'src/app/Services/MarkType/mark-type.service';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';

@Component({
  selector: 'app-mark-type',
  templateUrl: './mark-type.component.html',
  styleUrls: ['./mark-type.component.css'],
})
export class MarkTypeComponent implements OnInit {
  MarkTypeArray: MarkType[] = [];
  public values = [];
  LogForm : MarkType;
  public TempArray : MarkType[] = [];
  public SearchArray: MarkType[] =[];
  isArrayStored : boolean = false;
  data = false;
  mySubscription: any;

  constructor(
    private router: Router,
    private MarkTypeService: MarkTypeService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
// debugger
    this.MarkTypeService.getMarkTypes().subscribe(data => data.forEach(element => { 
      var marktype = new MarkType();
      marktype.MarkTypeID = element.MarkTypeID;
      marktype.MarkType1 = element.MarkType1;

        this.MarkTypeArray.push(marktype);
      })
    );
  } //ngOnInit

  EditMarkType(ids: number) {
    this.router.navigate(['/viewmarktype', { id: ids }],{ skipLocationChange: true });
  } //EditMarkType

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy

  SearchLogs(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.MarkTypeArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.MarkTypeArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.MarkTypeArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

      /*
      this.LogList.forEach((log) => {
        var SearchString: string = event.target.value;
        if (
        log.TagSerialNumber.toLowerCase().startsWith(
            SearchString.toLowerCase()
          )
        ) {
          this.SearchArray.push(log);
        }
      }); // For each
*/

      this.MarkTypeArray = this.SearchArray;
    } else {
      this.MarkTypeArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
} //OnInit
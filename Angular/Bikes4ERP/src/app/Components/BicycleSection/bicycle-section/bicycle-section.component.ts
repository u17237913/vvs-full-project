import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Section } from 'src/app/Classes/Section/section';
import { BicyclePartServiceService } from 'src/app/Services/BicyclePart/bicycle-part-service.service';
import {
  HttpClient,
  HttpHeaders,
  HttpClientModule,
} from '@angular/common/http';

@Component({
  selector: 'app-bicycle-section',
  templateUrl: './bicycle-section.component.html',
  styleUrls: ['./bicycle-section.component.css'],
})
export class BicycleSectionComponent implements OnInit {
  SectionArray: Section[] = [];
  data = false;
  mySubscription: any;
  //search
  public TempArray: Section[] = [];
  public SearchArray: Section[] = [];
  isArrayStored: boolean = false;

  constructor(
    private router: Router,
    private BicyclePartServiceService: BicyclePartServiceService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit(): void {
    this.BicyclePartServiceService.getSections().subscribe((data) =>
      data.forEach((element) => {
        var section = new Section();
        section.SectionID = element.SectionID;
        section.SectionName = element.SectionName;
        section.SectionDescription = element.SectionDescription;
        //section.SectionImage = element.SectionImage;

        this.SectionArray.push(section);
      })
    );
  } //ngOnInit

  EditSection(ids: number) {
    this.router.navigate(['/viewbicyclesection', { id: ids }],{ skipLocationChange: true });
  } //EditSection

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    } //Endif
  } //ngOnDestroy
  SearchUsers(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.SectionArray.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.SectionArray = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      const filterValue = event.target.value;
      this.SearchArray = this.SectionArray.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));

/*
      this.SectionArray.forEach((user) => {
        var SearchString: string = event.target.value;
        if (
          user.SectionName.toLowerCase().startsWith(SearchString.toLowerCase())
        ) {
          this.SearchArray.push(user);
        }
      }); // For each
*/

      this.SectionArray = this.SearchArray;
    } else {
      this.SectionArray = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
} //OnInit

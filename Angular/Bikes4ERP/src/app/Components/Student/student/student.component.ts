import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from 'src/app/Services/Student/student.service';
import { Student } from 'src/app/Classes/Student/student';
import { Village } from 'src/app/Classes/Village/village';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css'],
})
export class StudentComponent implements OnInit {
  StudentList: Student[] = [];
  VillageList: Village[] = [];
  public values = [];
  StudentForm: Student;
  //SearchType: string = 'Name';

  public TempArray: Student[] = [];
  public SearchArray: Student[] = [];
  isArrayStored: boolean = false;

  constructor(public route: Router, private studentService: StudentService) {}

  ngOnInit(): void {
    this.GetStudents();
    var Check = document.getElementById('Name') as HTMLInputElement;
    Check.checked = true;
    // subscribe
  } // ngOnInit
  GetStudents() {
    this.studentService.GetStudents().subscribe((data) => {
      this.StudentList = data as Student[];
      console.log(this.StudentList, 'list');
      console.log(data, 'data');
    });
  }

  CreateStudent() {
    this.route.navigate(['/createstudent']);
  }

  ViewStudent(ids: number) {
    this.route.navigate(['viewstudent', { id: ids }],{ skipLocationChange: true });
    console.log(ids);
  }

  SearchOptions() {
    var myModal = new Bootstrap.Modal(
      document.getElementById('SearchOptionsModal')
    );
    myModal.show();
  }

  // var valid =
  // (keycode > 47 && keycode < 58)   || // number keys
  // keycode == 32 || keycode == 13   || // spacebar & return key(s) (if you want to allow carriage returns)
  // (keycode > 64 && keycode < 91)   || // letter keys
  // (keycode > 95 && keycode < 112)  || // numpad keys
  // (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
  // (keycode > 218 && keycode < 223);   // [\]' (in order)

  SearchStudents(event: any) {
    if (this.isArrayStored == false) {
      this.TempArray = this.StudentList.slice();
      this.isArrayStored = true;
    }

    //debugger;
    if (event.target.value != '') {
      this.SearchArray = [];
      this.StudentList = this.TempArray;
      //this.TempArray = [];
      //var destinationArray = Array.from(sourceArray);

      // this.StudentList.forEach((student) => {
      //   var SearchString: string = event.target.value;
      //   if (
      //     student.StudentName.toLowerCase().startsWith(
      //       SearchString.toLowerCase()
      //     )
      //   ) {
      //     this.SearchArray.push(student);
      //   }
      // }); // For each

      const filterValue = event.target.value;
      this.SearchArray = this.StudentList.filter(x => Object.keys(x).some(k => x[k] != null && 
        x[k].toString().toLowerCase()
        .includes(filterValue.toLowerCase())));
      this.StudentList = this.SearchArray;
    } else {
      this.StudentList = this.TempArray.slice();
      this.SearchArray = [];
    }
  }
} // export
﻿namespace VVS_API_CF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedThoseTwoTables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.JobFaultTask", "FaultTaskID", "dbo.FaultTask");
            DropForeignKey("dbo.FaultTask", "PartID", "dbo.Part");
            DropForeignKey("dbo.JobFaultTask", "JobFaultID", "dbo.JobFault");
            DropIndex("dbo.JobFaultTask", new[] { "JobFaultID" });
            DropIndex("dbo.JobFaultTask", new[] { "FaultTaskID" });
            DropIndex("dbo.FaultTask", new[] { "PartID" });
            DropTable("dbo.JobFaultTask");
            DropTable("dbo.FaultTask");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.FaultTask",
                c => new
                    {
                        FaultTaskID = c.Int(nullable: false, identity: true),
                        FaultTaskDescription = c.String(),
                        PartID = c.Int(nullable: false),
                        FaultTask1 = c.String(),
                    })
                .PrimaryKey(t => t.FaultTaskID);
            
            CreateTable(
                "dbo.JobFaultTask",
                c => new
                    {
                        JobFaultTaskID = c.Int(nullable: false, identity: true),
                        JobFaultID = c.Int(),
                        FaultTaskID = c.Int(),
                        PartUsed = c.Boolean(),
                    })
                .PrimaryKey(t => t.JobFaultTaskID);
            
            CreateIndex("dbo.FaultTask", "PartID");
            CreateIndex("dbo.JobFaultTask", "FaultTaskID");
            CreateIndex("dbo.JobFaultTask", "JobFaultID");
            AddForeignKey("dbo.JobFaultTask", "JobFaultID", "dbo.JobFault", "JobFaultID");
            AddForeignKey("dbo.FaultTask", "PartID", "dbo.Part", "PartID", cascadeDelete: true);
            AddForeignKey("dbo.JobFaultTask", "FaultTaskID", "dbo.FaultTask", "FaultTaskID");
        }
    }
}

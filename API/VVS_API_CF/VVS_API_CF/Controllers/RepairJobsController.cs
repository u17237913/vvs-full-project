﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RepairJobsController : ApiController
    {

        private vvsContext db = new vvsContext();
        //declare a global userID

       

        // GET: api/RepairJobs
        [System.Web.Http.Route("api/RepairJobs/GetRepairJobs")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetRepairJobs(string id, int userID)
        {
            
          
            int year = Convert.ToInt32(id.Substring(0, 4));
            int month = Convert.ToInt32(id.Substring(5, 2));
            int day = Convert.ToInt32(id.Substring(8, 2));

            DateTime dateTime = new DateTime(year, month, day);

            db.Configuration.ProxyCreationEnabled = false;

            
            List<RepairJob> repairJobs = db.RepairJob
                .Include(s => s.RepairJobStatu)
                .Include(z => z.CheckIn)
                .Include(y => y.CheckIn.MechanicSchool)
                .Where(y => y.CheckIn.MechanicSchool.UserID == userID)
                .Include(x => x.Bicycle) 
                .Where(x => x.DateScheduled == dateTime)
                
                .ToList();

            return getRepairJobLists(repairJobs);
        }
        private List<dynamic> getRepairJobLists(List<RepairJob> forClient)
        {
            
            List<dynamic> dynamicMTs = new List<dynamic>();
            
            foreach (RepairJob mt in forClient)
            {
                dynamic dynamicMT = new ExpandoObject();
                dynamicMT.RepairJobID = mt.RepairJobID;
                dynamicMT.BicycleCode = mt.Bicycle.BicyleCode;
                dynamicMT.RepairJobStatusID = mt.RepairJobStatusID;
                dynamicMT.CheckInID = mt.CheckInID;
                dynamicMT.DateReported = mt.DateReported.ToString().Substring(0,10);
                dynamicMT.Assigned = mt.Assigned;
                dynamicMT.DateScheduled = mt.DateScheduled.ToString().Substring(0,10);
                dynamicMT.RepairJobStatus = mt.RepairJobStatu.RepairJobStatus;
                //dynamicMT.CheckInDate = mt.CheckIn.CheckInDate;
                dynamicMT.BicycleCode = mt.Bicycle.BicyleCode;



                dynamicMTs.Add(dynamicMT);
            }
            return dynamicMTs;
        }


        // GET: api/RepairJobs/5
        [ResponseType(typeof(RepairJob))]
        public async Task<IHttpActionResult> GetRepairJob(int id)
        {
            RepairJob repairJob = await db.RepairJob.FindAsync(id);
            if (repairJob == null)
            {
                return NotFound();
            }

            return Ok(repairJob);
        }

        // PUT: api/RepairJobs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRepairJob(int id, RepairJob repairJob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != repairJob.RepairJobID)
            {
                return BadRequest();
            }

            db.Entry(repairJob).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RepairJobExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RepairJobs
        [ResponseType(typeof(RepairJob))]
        public async Task<IHttpActionResult> PostRepairJob(RepairJob repairJob)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            RepairJob schdRepairJob = this.ScheduleCheckIn(repairJob);

            db.RepairJob.Add(schdRepairJob);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = repairJob.RepairJobID }, repairJob);
        }



        [ResponseType(typeof(RepairJob))]
        [System.Web.Http.Route("api/RepairJobs/AddJobFaults")]
        [System.Web.Mvc.HttpPost]
        public async Task<IHttpActionResult> AddJobFaults(List<JobFault> jobFaults)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.JobFault.AddRange(jobFaults);
            await db.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/RepairJobs/5
        [ResponseType(typeof(RepairJob))]
        public async Task<IHttpActionResult> DeleteRepairJob(int id)
        {
            RepairJob repairJob = await db.RepairJob.FindAsync(id);
            if (repairJob == null)
            {
                return NotFound();
            }

            db.RepairJob.Remove(repairJob);
            await db.SaveChangesAsync();

            return Ok(repairJob);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RepairJobExists(int id)
        {
            return db.RepairJob.Count(e => e.RepairJobID == id) > 0;
        }

        private RepairJob ScheduleCheckIn(RepairJob repairJob)
        {
            var student = db.BicycleStudent.Where(x => x.BicycleID == repairJob.BicycleID && x.DateUnassigned == null).FirstOrDefault();
            var school = db.Student.Where(y => y.StudentID == student.StudentID).Select(x => x.SchoolID).FirstOrDefault();
            var mechanicSchool = db.MechanicSchool.Where(x => x.SchoolID == school).FirstOrDefault();
            var mechanicWorkday = db.WorkDay.Where(x => x.WorkDayID == mechanicSchool.WorkDayID).Select(y => y.WorkDay1).FirstOrDefault();

            DateTime date = repairJob.DateReported ?? DateTime.Now.Date;

            for (int i = 0; i < 7; i++)
            {
                DateTime testDate = date.AddDays(i);
                if (testDate.DayOfWeek.ToString() == mechanicWorkday)
                {
                    repairJob.DateScheduled = testDate;
                    break;
                }
            }

            //CheckIn dynamicCheckIn = new CheckIn();
            //dynamicCheckIn.JobTypeID = 1;
            //dynamicCheckIn.CheckInStatusID = 1;
            //dynamicCheckIn.CheckInDate = null;
            //dynamicCheckIn.MechanicSchoolID = mechanicSchool.MechanicSchoolID;

            //db.CheckIn.Add(dynamicCheckIn);
            //db.SaveChanges();

            try
            {
                var studentNumberToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == repairJob.BicycleID).Select(ee => ee.Student.PhoneNumber).FirstOrDefault();
                var studentNameToSend = db.BicycleStudent.Include(ee => ee.Bicycle).Include(ee => ee.Student).Where(ee => ee.BicycleID == repairJob.BicycleID).Select(ee => ee.Student.StudentName + " " + ee.Student.StudentSurname).FirstOrDefault();
                var studbikecode = db.Bicycle.Where(bc => bc.BicycleID == repairJob.BicycleID).Select(bc => bc.BicyleCode).FirstOrDefault();

                // Find your Account Sid and Token at twilio.com/console
                // DANGER! This is insecure. See http://twil.io/secure
                const string accountSid = "AC0d9e20e4cc743b4494969bfcd9613f85";
                const string authToken = "47a721b61b23a3ace55346b7feef8715";

                string manipNum, StudCell;

                if (studentNumberToSend.Substring(0, 1) == "0")
                {
                    //removing the 0 in 0XXXXXXXXX to make it +27XXXXXXXXX
                    manipNum = studentNumberToSend.Remove(0, 1);
                    StudCell = "+27" + manipNum;
                }
                else if ((studentNumberToSend.Substring(0, 3) == "+27"))
                {
                    manipNum = studentNumberToSend;
                    StudCell = manipNum;
                }
                else
                {
                    manipNum = studentNumberToSend;
                    StudCell = "+27" + manipNum;
                }

                TwilioClient.Init(accountSid, authToken);

                dynamic textval = new ExpandoObject();
                textval.txt = "Hello " + studentNameToSend +
                                " You have reported a repair job scheduled for your bicycle " + studbikecode +
                                " scheduled for " + repairJob.DateScheduled +
                                ".\nPlease bring the bicycle on this day to be maintained\n -#Bikes4ERP";

                var message = MessageResource.Create(
                    from: new Twilio.Types.PhoneNumber("+12056229145"),
                    body: textval.txt,
                    to: new Twilio.Types.PhoneNumber(StudCell)
                );

                Console.WriteLine(message.Sid);
            }
            catch (Exception err)
            {
                Console.WriteLine(err);
            }

            return repairJob;
        }


        //checking if Bike is checked in
        [System.Web.Http.Route("api/RepairJobs/IsCheckedIn")]
        [System.Web.Http.HttpGet]
        public string IsCheckedIn(int jobID)
        {

            string returnVal = "";
            RepairJob myRepairJob = new RepairJob();

            foreach (RepairJob repairJob in db.RepairJob)
            {
                if(repairJob.RepairJobID == jobID)
                {
                    myRepairJob = repairJob;
                }

            }

            CheckIn mycheckin = new CheckIn();
            foreach(CheckIn checkIn in db.CheckIn)
            {
               if(checkIn.CheckInID == myRepairJob.CheckInID)
                {
                    mycheckin = checkIn;
                }
            }

            if(mycheckin.CheckInDate == null)
            {
                returnVal = "NotCheckedIn";
            }
            else
            {
                returnVal = "CheckedIn";
            }

           
            return returnVal;
            


        }

        //checking if Bike is checked in (maintenance)
        [System.Web.Http.Route("api/RepairJobs/IsCheckedInMaintenance")]
        [System.Web.Http.HttpGet]
        public string IsCheckedInMaintenance(int jobID)
        {

            string returnVal = "";
            RepairJob myRepairJob = new RepairJob();

            foreach (RepairJob repairJob in db.RepairJob)
            {
                if (repairJob.RepairJobID == jobID)
                {
                    myRepairJob = repairJob;
                }

            }

            CheckIn mycheckin = new CheckIn();
            foreach (CheckIn checkIn in db.CheckIn)
            {
                if (checkIn.CheckInID == myRepairJob.CheckInID)
                {
                    mycheckin = checkIn;
                }
            }

            if (mycheckin.CheckInDate == null)
            {
                returnVal = "NotCheckedIn";
            }
            else
            {
                returnVal = "CheckedIn";
            }


            return returnVal;



        }


    }
}
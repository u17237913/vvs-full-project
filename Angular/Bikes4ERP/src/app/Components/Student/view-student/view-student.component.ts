import { Component, OnInit } from '@angular/core';
import { ParamMap, Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
} from '@angular/forms';
import { StudentService } from 'src/app/Services/Student/student.service';
import { Student } from 'src/app/Classes/Student/student';
import { LocationViewModel } from 'src/app/Classes/School/location-view-model';
import { School } from 'src/app/Classes/School/school';

// import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
// import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { StuGuaViewModel } from 'src/app/Classes/StuGuaViewModel/stu-gua-view-model';
import { Guardian } from 'src/app/Classes/Guardian/guardian';
import { id } from 'date-fns/locale';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';

@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css'],
})
export class ViewStudentComponent implements OnInit {
  constructor(
    public formBuilder: FormBuilder,
    public studentService: StudentService,
    public route: ActivatedRoute,
    public router: Router
  ) {}

  StudentForm: any;
  GuardianForm: any;
  StudentList: Student;
  data = false;
  StudentID: number;
  message: string;
  LocationList: LocationViewModel[] = [];
  SchoolList: School[] = [];
  // StuGuaVM : StuGuaViewModel [];
  oldData: any;

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.StudentID = id;
    });

    this.GetLocations();
    this.GetSchools();

    this.StudentForm = new FormGroup({
      StudentName: new FormControl(),
      StudentSurname: new FormControl(),
      PhoneNumber: new FormControl(),
      StudentDoB: new FormControl(),
      VillageID: new FormControl(),
      SchoolID: new FormControl(),
    });

    this.GuardianForm = new FormGroup({
      GuardianID: new FormControl(),
      GuardianName: new FormControl(),
      GuardianEmail: new FormControl(),
      GuardianPhone: new FormControl(),
    });

    this.studentService.GetStudents().subscribe(
      (data) =>
        data.forEach((element) => {
          if (element.StudentID == this.StudentID) {
            this.StudentForm = this.formBuilder.group({
              StudentID: [element.StudentID, [Validators.required]],
              StudentName: [element.StudentName, [Validators.required]],
              StudentSurname: [element.StudentSurname, [Validators.required]],
              GuardianID: [element.GuardianID, [Validators.required]],
              VillageID: [element.VillageID, [Validators.required]],
              StudentDoB: [element.StudentDoB, [Validators.required]],
              PhoneNumber: [element.PhoneNumber, [Validators.required]],
              SchoolID: [element.SchoolID, [Validators.required]],

              // GuardianID : [element.Guardians.GuardianID, [Validators.required]]
            }); // FormBuilder
            this.oldData = element;

            this.GuardianForm = this.formBuilder.group({
              GuardianID: [element.Guardian.GuardianID, [Validators.required]],
              GuardianName: [
                element.Guardian.GuardianName,
                [Validators.required],
              ],
              GuardianEmail: [
                element.Guardian.GuardianEmail,
                [Validators.required],
              ],
              GuardianPhone: [
                element.Guardian.GuardianPhone,
                [Validators.required],
              ],
            });
          }
        }) //ForEach
    );
  }

  BackView() {
    this.router.navigate(['/viewstudent']);
  }

  RouteStudent() {
    this.router.navigate(['/student']);
  }

  onFormSubmit() {
    console.log('test');
    const student: Student = this.StudentForm.value;
    const guardian: Guardian = this.GuardianForm.value;
    var stuGuaViewModel = new StuGuaViewModel();
    stuGuaViewModel.Guardians = guardian;
    stuGuaViewModel.Students = student;
    this.UpdateStudent(stuGuaViewModel);
    // this.UpdateStudent(this.StudentID, student)
  }

  UpdateStudent(stuGuaViewModel: StuGuaViewModel) {
    this.studentService.UpdateStudent(stuGuaViewModel, this.oldData).subscribe(() => {
      this.data = true;
      this.message = 'School has been updated';
      this.StudentForm.reset;
      var myModal = new Bootstrap.Modal(
        document.getElementById('StudentUpdatedModal')
      );
      myModal.show();
    });
  }
  DeleteStudent(StudentID: any) {
    StudentID = this.StudentID;
    this.studentService.DeleteStudent(StudentID, this.oldData).subscribe(() => {
      this.data = true;
      var myModal = new Bootstrap.Modal(
        document.getElementById('StudentDeletedModal')
      );
      myModal.show();
    });
  }
  GetLocations() {
    this.studentService.GetLocations().subscribe((data) => {
      this.LocationList = data;
      console.log(this.LocationList);
    });
  }

  GetSchools() {
    this.studentService.GetSchools().subscribe((data) => {
      this.SchoolList = data;
      console.log(this.SchoolList);
    });
  }
}

export class MaintenanceTask {

    constructor() { }
    MaintenanceTypeID: number
    ID: number
    PartID: number
    MaintenanceTask: string
    MaintenanceTaskDescription: string
    MaintenanceType: string
    Part: string
}

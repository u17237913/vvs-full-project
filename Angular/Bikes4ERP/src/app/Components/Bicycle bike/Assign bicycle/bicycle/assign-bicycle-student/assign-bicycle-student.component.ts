import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { take } from 'rxjs/operators';
import { BicycleBrand } from 'src/app/Classes/BicycleBrand/bicycle-brand';
import { BicycleService } from 'src/app/Services/Bicycle/bicycle.service';
import { Bicycle } from 'src/app/Classes/Bicycle/bicycle';
import { Student } from 'src/app/Classes/Student/student';
import { StudentService } from 'src/app/Services/Student/student.service';
import * as Bootstrap from 'node_modules/bootstrap/dist/js/bootstrap.min.js';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Query } from '@syncfusion/ej2-data';
import { EmitType } from '@syncfusion/ej2-base';
import { FilteringEventArgs } from '@syncfusion/ej2-dropdowns';

@Component({
  selector: 'app-assign-bicycle-student',
  templateUrl: './assign-bicycle-student.component.html',
  styleUrls: ['./assign-bicycle-student.component.css'],
})
export class AssignBicycleStudentComponent implements OnInit {


  //public data: { [key: string]: Object; }[] = [
  public CurrentBicycleCode: number;
  //public StudentList: Student[] = [];
  public StudentList: { [key: string]: Student; }[] = [];
  public StudentID: number;
  // public options: string[] = this.StudentList [] ;

  public ModalHeading: string;
  public ModalBody: string;

  constructor(
    public formBuilder: FormBuilder,
    public bicycleService: BicycleService,
    public route: ActivatedRoute,
    public router: Router,
    public studentService: StudentService
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.CurrentBicycleCode = id;
    });

    this.GetStudentList();
    

   
  } // ngOnInit

      // maps the appropriate column to fields property
      public fields: Object = { text: 'StudentFullName' , value: 'StudentID' };
      //bind the Query instance to query property
      public query: Query = new Query().from('StudentList').select(['StudentFullName']);
      // set the height of the popup element
      public height: string = '220px';
      // set the height of the popup element
      public width: string = '500px';
      // set the placeholder to DropDownList input element
      public watermark: string = 'Select a Student';
      // set the placeholder to filter search box input element
      public filterPlaceholder: string = 'Search Student';
      // filtering event handler to filter a Country
      public onFiltering: EmitType<FilteringEventArgs> = (e: FilteringEventArgs) => {
          let query: Query = new Query();
          //frame the query based on search string with filter type.
          query = (e.text !== '') ? query.where('StudentFullName', 'startswith', e.text, true) : query;
          //pass the filter data source, filter query to updateData method.
          e.updateData(this.StudentList, query);
      }


  GetStudentList() {
    this.studentService.GetStudentList().subscribe(
      (sl) => {
        if (sl.length > 0) {
          this.StudentList = sl;
          console.clear();
          console.log(this.StudentList);
        } else {
          var myModal = new Bootstrap.Modal(
            document.getElementById('NoStudentsModal')
          );
          myModal.show();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }


  ConfirmationModal() {
    var myModal = new Bootstrap.Modal(document.getElementById('CreateModal'));
    myModal.show();
  }

  ShowModal() {
    // var modal = document.getElementById('UnassignBicycle');
    // modal.style.removeProperty('z-index');
    var myModal = new Bootstrap.Modal(document.getElementById('UpdateModal'));
    myModal.show();
  }

  GoToBicycleAssignmentPage() {
    this.router.navigate(['assignbicycle']);
  }

  AssignBicyle() {
    console.log(this.CurrentBicycleCode);
    console.log(this.StudentID);
    this.bicycleService
      .AssignBicycle(this.CurrentBicycleCode, this.StudentID)
      .subscribe(
        (res) => {
          this.ShowModal();
        },
        (error) => {},
        () => {}
      );
  }
}

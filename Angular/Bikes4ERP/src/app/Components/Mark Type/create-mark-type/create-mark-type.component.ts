import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { MarkTypeService } from 'src/app/Services/MarkType/mark-type.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { MarkType } from 'src/app/Classes/MarkType/mark-type';

@Component({
  selector: 'app-create-mark-type',
  templateUrl: './create-mark-type.component.html',
  styleUrls: ['./create-mark-type.component.css'],
})
export class CreateMarkTypeComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private MarkTypeService: MarkTypeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
//MarkTypeForm : any;
  ID: number;
  data = false;
  message: string;

  //Initialize FormGroup ting
  MarkTypeForm = new FormGroup({
    MarkType1: new FormControl(''),
  });

  ngOnInit(): void {
    //For the Create object
    this.MarkTypeForm = this.formBuilder.group({
      MarkType1: ['', [Validators.required]],
    });
  } //ngOnInit

  onFormSubmit(MarkTypeForm) {
    // debugger;
    const marktype = MarkTypeForm.value;
    this.CreateMarkType(marktype);
  } //OnFormSubmit

  CreateMarkType(marktype: MarkType) {
    //inject the services
    // debugger;
    this.MarkTypeService.addMarkType(marktype).subscribe(() => {
      this.data = true;

      this.message = 'MarkType has been added bro! good job...';
      this.MarkTypeForm.reset;
      console.log(this.MarkTypeForm.value + 'we adding');

      this.router.navigate(['/marktype']);
    }); //addMarkType
  } //CreateMarkType

  GoToList() {
    this.router.navigate(['/marktype']);
  } //GoToList
} //OnInit
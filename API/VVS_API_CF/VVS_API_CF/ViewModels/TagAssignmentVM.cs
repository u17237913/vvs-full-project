﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class TagAssignmentVM
    {
        public string TagSerialNumber { get; set; }
        public int BicycleCode { get; set; }
        public string TagStatus { get; set; }
    }
}
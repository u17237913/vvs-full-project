﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class BicycleBrandsController : ApiController
    {
        private vvsContext db = new vvsContext();

        //----------------------------------------ALL Bicycle Brands API END-POINTS BELOW----------------------------------------------

        //GET ---> Bicycle Brands
        [System.Web.Http.Route("api/BicycleBrands/GetAllBicycleBrands")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetAllBicycleBrands()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getBicycleBrandsReturnList(db.BicycleBrand.ToList());
        }

        private List<dynamic> getBicycleBrandsReturnList(List<BicycleBrand> forBicycle)
        {
            List<dynamic> dynamicBicycleBrands = new List<dynamic>();
            foreach (BicycleBrand bicyclebrand in forBicycle)
            {
                dynamic dynamicBicycleBrand = new ExpandoObject();

                dynamicBicycleBrand.BicycleBrandID = bicyclebrand.BicycleBrandID;
                dynamicBicycleBrand.BicycleBrandName = bicyclebrand.BicycleBrandName;

                dynamicBicycleBrands.Add(dynamicBicycleBrand);
            }
            return dynamicBicycleBrands;
        }

        //ADD ---> Bicycle Brand
        [System.Web.Http.Route("api/BicycleBrands/AddBicycleBrand")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddBicycleBrand([FromBody] BicycleBrand bicyclebrand)
        {
            db.Configuration.ProxyCreationEnabled = false;
            db.BicycleBrand.Add(bicyclebrand);
            db.SaveChanges();

            return getBicycleBrandsReturnList(db.BicycleBrand.ToList());
        }

        //UPDATE ---> Bicycle Brand
        [System.Web.Http.Route("api/BicycleBrands/UpdateBicycleBrand")]
        [System.Web.Mvc.HttpPost]
        public bool UpdateBicycleBrand([FromBody] BicycleBrand _bicyclebrand)
        {
            using (vvsContext vvsSys = new vvsContext())
            {
                BicycleBrand updatedBicycleBrand = (from mt in vvsSys.BicycleBrand
                                            where mt.BicycleBrandID == _bicyclebrand.BicycleBrandID
                                            select mt)
                                            .FirstOrDefault();

                updatedBicycleBrand.BicycleBrandID = _bicyclebrand.BicycleBrandID;
                updatedBicycleBrand.BicycleBrandName = _bicyclebrand.BicycleBrandName;

                vvsSys.SaveChanges();
            }
            return true;
        }

        //DELETE ---> BicyclePart
        [System.Web.Http.Route("api/BicycleBrands/DeleteBicycleBrand")]
        [System.Web.Mvc.HttpDelete]
        public void DeleteBicycleBrand(int id)
        {
            BicycleBrand BicycleBrandToDelete = (db.BicycleBrand
                                        .Where(o => o.BicycleBrandID == id))
                                        .FirstOrDefault();

            db.BicycleBrand.Remove(BicycleBrandToDelete);
            db.SaveChanges();
        }



        // --- SCAFFOLDED APPROACH BELOW IF NEEDED...



/*        // GET: api/BicycleBrands
        public IQueryable<BicycleBrand> GetBicycleBrand()
        {
            return db.BicycleBrand;
        }

        // GET: api/BicycleBrands/5
        [ResponseType(typeof(BicycleBrand))]
        public async Task<IHttpActionResult> GetBicycleBrand(int id)
        {
            BicycleBrand bicycleBrand = await db.BicycleBrand.FindAsync(id);
            if (bicycleBrand == null)
            {
                return NotFound();
            }

            return Ok(bicycleBrand);
        }

        // PUT: api/BicycleBrands/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBicycleBrand(int id, BicycleBrand bicycleBrand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bicycleBrand.BicycleBrandID)
            {
                return BadRequest();
            }

            db.Entry(bicycleBrand).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BicycleBrandExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BicycleBrands
        [ResponseType(typeof(BicycleBrand))]
        public async Task<IHttpActionResult> PostBicycleBrand(BicycleBrand bicycleBrand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BicycleBrand.Add(bicycleBrand);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = bicycleBrand.BicycleBrandID }, bicycleBrand);
        }

        // DELETE: api/BicycleBrands/5
        [ResponseType(typeof(BicycleBrand))]
        public async Task<IHttpActionResult> DeleteBicycleBrand(int id)
        {
            BicycleBrand bicycleBrand = await db.BicycleBrand.FindAsync(id);
            if (bicycleBrand == null)
            {
                return NotFound();
            }

            db.BicycleBrand.Remove(bicycleBrand);
            await db.SaveChangesAsync();

            return Ok(bicycleBrand);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BicycleBrandExists(int id)
        {
            return db.BicycleBrand.Count(e => e.BicycleBrandID == id) > 0;
        }
*/
    }
}
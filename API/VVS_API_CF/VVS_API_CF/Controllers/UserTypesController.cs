﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using VVS_API_CF.Models;

namespace VVS_API_CF.Controllers
{
    public class UserTypesController : ApiController
    {
        private vvsContext db = new vvsContext();

        // GET: api/UserTypes
        [System.Web.Http.Route("api/UserTypes/GetUserTypes")]
        [System.Web.Mvc.HttpGet]
        public List<dynamic> GetUserType()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return getUserTypeList(db.UserType.ToList());
        }
        private List<dynamic> getUserTypeList(List<UserType> forClient)
        {
            List<dynamic> dynamicUserTypes = new List<dynamic>();
            foreach (UserType userType in forClient)
            {
                dynamic dynamicType = new ExpandoObject();
                dynamicType.ID = userType.UserTypeID;
                dynamicType.UserType = userType.UserType1;
                dynamicType.UserPermissions = userType.UserPermissions;

                dynamicUserTypes.Add(dynamicType);
            }
            return dynamicUserTypes;
        }

        // GET: api/UserTypes/5
        [System.Web.Http.Route("api/UserTypes/GetUserType")]
        [System.Web.Mvc.HttpGet]
        [ResponseType(typeof(UserType))]
        public async Task<IHttpActionResult> GetUserType(int id)
        {
            UserType userType = await db.UserType.FindAsync(id);
            if (userType == null)
            {
                return NotFound();
            }

            return Ok(userType);
        }

        // PUT: api/UserTypes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUserType(int id, UserType userType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != userType.UserTypeID)
            {
                return BadRequest();
            }

            db.Entry(userType).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UserTypes
        //[System.Web.Http.Route("api/UserTypes/PostUserType")]
        [System.Web.Mvc.HttpPost]
        [ResponseType(typeof(UserType))]
        public async Task<IHttpActionResult> PostUserType(UserType userType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UserType.Add(userType);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { ID = userType.UserTypeID }, userType);
        }

        // DELETE: api/UserTypes/5
        [ResponseType(typeof(UserType))]
        public async Task<IHttpActionResult> DeleteUserType(int id)
        {
            UserType userType = await db.UserType.FindAsync(id);
            if (userType == null)
            {
                return NotFound();
            }

            db.UserType.Remove(userType);
            await db.SaveChangesAsync();

            return Ok(userType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserTypeExists(int id)
        {
            return db.UserType.Count(e => e.UserTypeID == id) > 0;
        }



        
    }
}
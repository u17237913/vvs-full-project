﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VVS_API_CF.ViewModels
{
    public class StudentReportVM
    {
        public StudentReportVM() { }

        public int StudID;
        public string StudentName;
        public int BicycleCode;
        public int Age;
        public int Attendance;
        public double AverageMark;
    }
}